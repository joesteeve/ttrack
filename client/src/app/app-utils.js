// Angular stuff
angular.module('ttrack.utils', [
  'ui.bootstrap'
])
  .factory(
    'ttrackUtils', function () {
      return {
        pad: function (number) {
          if (number < 10) {
            return '0' + number;
          }
          return number;
        },
        jq: function (myid) {
          return '#' + myid.replace(/(:|\.)/g,'\\$1');
        }
      };
    })
  .controller(
    "confirmationDialogYesNoController",
    ["$scope", "$modalInstance", "primaryMessage", "secondaryMessage",
     function($scope, $modalInstance, primaryMessage, secondaryMessage) {
       $scope.primaryMessage = primaryMessage;
       $scope.secondaryMessage = secondaryMessage;
       $scope.yes = function() {
         $modalInstance.close("yes");
       };
       $scope.no = function() {
         $modalInstance.dismiss("no");
       };
     }])
  .controller(
    "messageDialogController",
    ["$scope", "$modalInstance", "primaryMessage", "secondaryMessage",
     function($scope, $modalInstance, primaryMessage, secondaryMessage) {
       $scope.primaryMessage = primaryMessage;
       $scope.secondaryMessage = secondaryMessage;
     }])

  .factory(
    'confirmationDialogYesNo',
    ['$modal', function($modal) {
      return {
        show : function(primaryMessage, secondaryMessage) {
          var modalInstance = $modal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>{{primaryMessage}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <p>{{secondaryMessage}}</p>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="yes()">' +
              '    Yes' +
              '  </button>' +
              '  <button class="btn btn-default" data-ng-click="no()">' +
              '    No' +
              '  </button>' +
              '</div>',
            controller : "confirmationDialogYesNoController",
            resolve : {
              'primaryMessage' : function () {
                return primaryMessage;
              },
              'secondaryMessage' : function () {
                return secondaryMessage;
              }
            }
          });
          return modalInstance.result;
        }
      };
    }])
  .factory(
    'messageDialog',
    ['$modal',function($modal) {
      return {
        show : function(primaryMessage, secondaryMessage) {
          var modalInstance = $modal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>{{primaryMessage}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <p>{{secondaryMessage}}</p>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="$dismiss()">' +
              '    Ok' +
              '  </button>' +
              '</div>',
            controller : "messageDialogController",
            resolve : {
              'primaryMessage' : function () {
                return primaryMessage;
              },
              'secondaryMessage' : function () {
                return secondaryMessage;
              }
            }
          });
          return modalInstance.result;
        }
      };
    }])
  .directive(
    'triStateCheckbox',
    function () {
      // Thirdparty triStateCheckbox directive from SO
      // http://stackoverflow.com/a/14820851
      return {
        replace: true,
        restrict: 'E',
        scope: { checkboxes: '=' },
        template: '<input type="checkbox" ng-model="master" ng-change="masterChange()">',
        controller: ["$scope", "$element", function($scope, $element) {
          $scope.masterChange = function() {
            if ($scope.master) {
              angular.forEach($scope.checkboxes, function(cb, index) {
                cb.isSelected = true;
              });
            } else {
              angular.forEach($scope.checkboxes, function(cb, index) {
                cb.isSelected = false;
              });
            }
          };
          $scope.$watch('checkboxes', function() {
            var allSet = true, allClear = true;
            angular.forEach($scope.checkboxes, function(cb, index) {
              if (cb.isSelected) {
                allClear = false;
              } else {
                allSet = false;
              }
            });
            if (allSet) {
              $scope.master = true;
              $element.prop('indeterminate', false);
            }
            else if (allClear) {
              $scope.master = false;
              $element.prop('indeterminate', false);
            }
            else {
              $scope.master = false;
              $element.prop('indeterminate', true);
            }
          }, true);
        }]
      };
    })
  .filter(
    'matchProject',
    function () {
      return function (project_list, search_term) {
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in project_list) {
          var project_name = project_list[index].name.toLowerCase();
          var project_id = project_list[index].project_id.toLowerCase();
          if (project_name.indexOf(term) != -1 ||
              project_id.indexOf(term) != -1) {
            result.push(project_list[index]);
          }
        }
        return result;
      };
    })
  .filter(
    'matchClient',
    function () {
      return function (client_list, search_term) {
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in client_list) {
          var client_name = client_list[index].name.toLowerCase();
          var client_id = client_list[index].client_id.toLowerCase();
          if (client_name.indexOf(term) != -1 ||
              client_id.indexOf(term) != -1) {
            result.push(client_list[index]);
          }
        }
        return result;
      };
    })
  .filter(
    'matchUser',
    function () {
      return function (user_list, search_term) {
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in user_list) {
          var user_name = user_list[index].real_name.toLowerCase();
          var userid = user_list[index].userid.toLowerCase();
          if (user_name.indexOf(term) != -1 ||
              userid.indexOf(term) != -1) {
            result.push(user_list[index]);
          }
        }
        return result;
      };
    })
  .controller(
    "apiErrorDialogController",
    ["$scope", "$modalInstance", "response",
     function($scope, $modalInstance, response) {
       $scope.response = response;
     }])
  .factory(
    'apiErrorDialog',
    ['$modal', function($modal) {
      return {
        show : function(response) {
          var modalInstance = $modal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>API error : {{response.status}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <ul>' +
              '    <li data-ng-repeat="error in response.data.errors">' +
              '      \'{{error.location}}\', \'{{error.name}}\', ' +
              '      \'{{error.description}}\' ' +
              '    </li>' +
              '  </ul>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="$dismiss()">' +
              '    Ok' +
              '  </button>' +
              '</div>',
            controller : "apiErrorDialogController",
            resolve : {
              'response' : function () {
                return response;
              }
            }
          });
        }
      };
    }]);
