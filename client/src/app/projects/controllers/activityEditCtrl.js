angular.module('ttrack.projects.controllers.activities.edit', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.projects.directives'
]).controller(
  'activityEditCtrl',
  ['$scope', 'Activity', '$stateParams', '$state', 'apiErrorDialog',
   function ($scope, Activity, $stateParams, $state, errorDlg) {
     $scope.project_id = $stateParams.projectId;
     var act_name = $stateParams.activityName;

     Activity.get(
       {projectId: $scope.project_id,
        activityName: act_name},
       function(data) {
         $scope.activity = data.result;
         $scope.activity.new_name = act_name;
       },
       function(err_res) {
         console.log("Invalid activity_name : " + act_name);
         $state.go("manage.activities.list", { projectId: $scope.project_id });
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.activityEditForm.$dirty && $scope.activityEditForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.activityEditForm.$valid) {
         return;
       }
       // $scope.activity will contain both name and new_name so we
       // construct a new activity object and pass it for PUT
       var activity = {};
       activity.name = $scope.activity.new_name;
       Activity.save(
         {
           projectId: $scope.project_id,
           activityName: act_name
         },
         activity,
         function (data) {
           $state.go("manage.activities.show", {
             projectId: $scope.project_id,
             activityName: activity.name
           });
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }
  ]);
