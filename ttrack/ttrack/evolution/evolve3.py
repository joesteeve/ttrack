# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

def evolve(root):
    """This evolve converts all the client IDs to lower case

    This will try to resolve conflicts while trying to rename an
    upper-case client_id to a lower-case. If a lower case of a client
    id conflicts with an existing client id, number is suffixed to get
    a unique client_id as there is no point in deleting a client ID as
    this will cause all the timeentries affected
    """
    log.info("Evolving to step 3: convert all client IDs to lowercase")
    clients = root['clients']

    to_change = []
    for k in clients.keys():
        if k.islower() is False:
            to_change.append(k)

    for client_id in to_change:
        if client_id in clients:
            log.info("Lower case converstion of client ID (%s) conflicts",
                     client_id)
            log.info("Trying to resolve conflict of client ID (%s)", client_id)
            i = 1
            new_id = client_id
            lcase_id = client_id.strip().lower()
            while new_id in clients:
                new_id = "%s%d" % (lcase_id, i)
                i = i + 1
            log.info("Client ID (%s) resolved to '%s'", client_id, new_id)
        else:
            new_id = client_id.strip().lower()
        clients.rename_client_id(client_id, new_id)

    if to_change:
        log.info("Migrated %d record(s) to 'step 3' standard", len(to_change))
    else:
        log.info("All records comply to 'step 3' standard")
    log.info("ZODB successfully evolved to step 3")
