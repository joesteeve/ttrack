# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
import datetime

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

import ttrack.testing
from .api_data import TE_DATA


class ActionAPITests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet/action/split'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_invalid_date_format(self):
        new_time_entry = {
            'start_date': '2014/1/1',
            'end_date': '2014/1/1',
            'start_time' : '10:00',
            'end_time' : '10:30',
            'userid' : 'john'
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_invalid_time_format(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '10:00am',
            'end_time' : '10:30am',
            'userid' : 'john'
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_invalid_date(self):
        new_time_entry = {
            'start_date': '2014-2-29', # 2014 is not a leap year
            'end_date': '2014-2-29',
            'start_time' : '10:00',
            'end_time' : '10:30',
            'userid' : 'john'
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_incorrect_start_and_end_time(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '12:00',
            'end_time' : '10:30',
            'userid' : 'john'
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_incorrect_start_and_end_date(self):
        new_time_entry = {
            'start_date': '2014-1-2',
            'end_date': '2014-1-1',
            'start_time' : '9:00',
            'end_time' : '10:30',
            'userid' : 'john'
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_default_authenticated_userid(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '10:00',
            'end_time' : '10:30',
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)

    def test_bad_userid(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '10:00',
            'end_time' : '10:30',
            'userid': 'user5',
        }
        self.app.post_json(
            self.api_url, new_time_entry, status=HTTPBadRequest.code)

    def test_good_entry_no_split(self):
        new_time_entry = {
            'start_date': '2014-1-2',
            'end_date': '2014-1-2',
            'start_time' : '11:30',
            'end_time' : '12:00',
            'userid': 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'good')

    def test_error_entry_conflict(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '11:30',
            'end_time' : '12:00',
            'userid': 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'error')
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '10:30')
        self.assertEqual(res['end_time'], '11:30')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-02')

    def test_error_no_new_additions_possible(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-2',
            'start_time' : '09:00',
            'end_time' : '12:30',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'error')
        self.assertEqual(len(resp.json['result']), 2)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '09:00')
        self.assertEqual(res['end_time'], '10:00')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-02')
        res = resp.json['result'][1]
        self.assertEqual(res['start_time'], '10:00')
        self.assertEqual(res['end_time'], '12:30')
        self.assertEqual(res['start_date'], '2014-01-02')
        self.assertEqual(res['end_date'], '2014-01-02')

    def test_entry_with_split_1(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-1',
            'start_time' : '09:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'split')
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '10:00')
        self.assertEqual(res['end_time'], '10:30')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-01')

    def test_entry_with_split_2(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-2',
            'start_time' : '09:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'split')
        self.assertEqual(len(resp.json['result']), 2)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '10:00')
        self.assertEqual(res['end_time'], '10:30')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-01')
        res = resp.json['result'][1]
        self.assertEqual(res['start_time'], '11:30')
        self.assertEqual(res['end_time'], '12:00')
        self.assertEqual(res['start_date'], '2014-01-02')
        self.assertEqual(res['end_date'], '2014-01-02')

    def test_entry_with_split_3(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '09:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'split')
        self.assertEqual(len(resp.json['result']), 3)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '10:00')
        self.assertEqual(res['end_time'], '10:30')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-01')
        res = resp.json['result'][1]
        self.assertEqual(res['start_time'], '11:30')
        self.assertEqual(res['end_time'], '12:00')
        self.assertEqual(res['start_date'], '2014-01-02')
        self.assertEqual(res['end_date'], '2014-01-02')
        res = resp.json['result'][2]
        self.assertEqual(res['start_time'], '18:30')
        self.assertEqual(res['end_time'], '12:30')
        self.assertEqual(res['start_date'], '2014-01-03')
        self.assertEqual(res['end_date'], '2014-01-04')

    def test_entry_with_split_4(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)
        self.assertEqual(resp.json['status'], 'split')
        self.assertEqual(len(resp.json['result']), 4)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '08:30')
        self.assertEqual(res['end_time'], '09:00')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-01')
        res = resp.json['result'][1]
        self.assertEqual(res['start_time'], '10:00')
        self.assertEqual(res['end_time'], '10:30')
        self.assertEqual(res['start_date'], '2014-01-01')
        self.assertEqual(res['end_date'], '2014-01-01')
        res = resp.json['result'][2]
        self.assertEqual(res['start_time'], '11:30')
        self.assertEqual(res['end_time'], '12:00')
        self.assertEqual(res['start_date'], '2014-01-02')
        self.assertEqual(res['end_date'], '2014-01-02')
        res = resp.json['result'][3]
        self.assertEqual(res['start_time'], '18:30')
        self.assertEqual(res['end_time'], '12:30')
        self.assertEqual(res['start_date'], '2014-01-03')
        self.assertEqual(res['end_date'], '2014-01-04')


class ActionAPINonAdminPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet/action/split'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_allow_post_for_self(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)

    def test_deny_post_for_other_userid(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
            'userid' : 'admin',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPForbidden.code)

class ActionAPIAdminPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet/action/split'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_allow_post_for_other(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)

    def test_allow_post_for_self(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPOk.code)

class TimeSheetAPIUnAuthenticatedPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet/action/split'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_deny_post(self):
        new_time_entry = {
            'start_date': '2014-1-1',
            'end_date': '2014-1-4',
            'start_time' : '08:30',
            'end_time' : '12:30',
            'userid' : 'john',
        }
        resp = self.app.post_json(
            self.api_url, new_time_entry, status=HTTPForbidden.code)
