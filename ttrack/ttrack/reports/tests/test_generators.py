# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
from mock import (
    Mock,
    MagicMock
)
import datetime

from ttrack.reports.generators import (
    generate_project_report,
)


class GenerateProjectReport(unittest.TestCase):

    def test_start_yes_end_yes_report(self):
        pts = Mock()
        start_d = datetime.date(2014,01,01)
        end_d = datetime.date(2014,01,02)
        start_dt = datetime.datetime.combine(start_d, datetime.time(0, 0))
        end_dt = datetime.datetime.combine(end_d, datetime.time(23, 59, 59))

        generate_project_report(pts, None, start_d, end_d)
        pts.get_statistics_for_range.assert_called_with(start_dt, end_dt)

    def test_start_None_end_yes_report(self):
        pts = Mock()
        end_d = datetime.date(2014,01,02)
        end_dt = datetime.datetime.combine(end_d, datetime.time(23, 59, 59))

        generate_project_report(pts, None, None, end_d)
        pts.get_statistics_for_range.assert_called_with(None, end_dt)

    def test_start_yes_end_None_report(self):
        pts = Mock()
        start_d = datetime.date(2014,01,01)
        start_dt = datetime.datetime.combine(start_d, datetime.time(0, 0))

        generate_project_report(pts, None, start_d, None)
        pts.get_statistics_for_range.assert_called_with(start_dt, None)

    def test_start_None_end_None_report(self):
        pts = Mock()
        generate_project_report(pts, None, None, None)
        pts.get_statistics_for_time_key.assert_called_with(None)

    def test_fail_on_bad_range(self):
        pts = Mock()
        start_d = datetime.date(2014,01,02)
        end_d = datetime.date(2014,01,01)

        self.assertRaises(
            TypeError, generate_project_report, (pts, None, start_d, end_d))
