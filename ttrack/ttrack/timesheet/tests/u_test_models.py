# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
#from unittest import mock
import datetime

from ttrack.timesheet import (
    UITimeEntry,
    TimeLog,
    TimeSheet,
    )
from ttrack.timesheet.models import get_overlapping_entries, fit_time_entry

from ttrack.users import User

from ttrack import errors


class TestTimeLog(unittest.TestCase):

    def setUp(self):
        start_time = datetime.datetime(2014, 1, 1, 8, 0, 0)
        end_time = datetime.datetime(2014, 1, 1, 9, 30, 0)
        self.timeentry1 = UITimeEntry(start_time, end_time)

    def tearDown(self):
        pass

    def test_add_timelog(self):
        tl = TimeLog()
        tl.add(self.timeentry1)
        self.assertIsNotNone(tl.get_ute(self.timeentry1.ute_id))

    def test_fail_when_adding_duplicate_log_instance(self):
        tl = TimeLog()
        tl.add(self.timeentry1)
        with self.assertRaises(errors.BadParameter):
            tl.add(self.timeentry1)

    def test_add_duplicate_clone(self):
        tl = TimeLog()
        tl.add(self.timeentry1)
        self.assertIsNotNone(tl.get_ute(self.timeentry1.ute_id))
        ute_clone = UITimeEntry.clone(self.timeentry1)
        tl.add(ute_clone)
        self.assertIsNotNone(tl.get_ute(ute_clone.ute_id))

    def test_remove_timelog(self):
        tl = TimeLog()
        tl.add(self.timeentry1)
        self.assertIsNotNone(tl.get_ute(self.timeentry1.ute_id))
        tl.remove(self.timeentry1)
        self.assertIsNone(tl.get_ute(self.timeentry1.ute_id))

    def test_fetch_a_range(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 1, 10, 0, 0)),
                    ((2014, 1, 2, 9, 0, 0), (2014, 1, 2, 10, 0, 0)),
                    ((2014, 1, 3, 9, 0, 0), (2014, 1, 3, 10, 0, 0)),
                    ((2014, 1, 4, 9, 0, 0), (2014, 1, 4, 10, 0, 0)))
        da_ute = []
        tl = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            tl.add(te)
            da_ute.append(te)

        entries = tl.get_ute_entries(datetime.datetime(2014, 1, 2),
                                     datetime.datetime(2014, 1, 4, 23, 59, 59))

        ts_ute = []
        for logid in entries:
            te = tl.get_ute(logid.ute_id)
            ts_ute.append(te)

        self.assertEqual(len(ts_ute), 3)
        self.assertEqual(da_ute[1].ute_id, ts_ute[0].ute_id)
        self.assertEqual(da_ute[2].ute_id, ts_ute[1].ute_id)
        self.assertEqual(da_ute[3].ute_id, ts_ute[2].ute_id)

    def test_fetch_multi_day_range(self):
        data_set = (((2014, 1, 1, 8, 0, 0), (2014, 1, 3, 9, 0, 0)),
                    ((2014, 1, 2, 9, 0, 0), (2014, 1, 4, 10, 0, 0)),
                    ((2014, 1, 3, 10, 0, 0), (2014, 1, 3, 11, 0, 0)))

        da_ute = []
        tl = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            tl.add(te)
            da_ute.append(te)


        ite_entries = tl.get_ite_entries(
            datetime.datetime(2014, 1, 3),
            datetime.datetime(2014, 1, 4, 23, 59, 59))

        ts_ute_id = []
        for ite in ite_entries:
            if ite.ute.ute_id in ts_ute_id:
                continue
            ts_ute_id.append(ite.ute.ute_id)

        self.assertEqual(len(ts_ute_id), 3)
        self.assertIn(da_ute[0].ute_id, ts_ute_id)
        self.assertIn(da_ute[1].ute_id, ts_ute_id)
        self.assertIn(da_ute[2].ute_id, ts_ute_id)

    def test_fetch_multi_day_range(self):
        data_set = (((2014, 1, 1, 8, 0, 0), (2014, 1, 3, 9, 0, 0)),
                    ((2014, 1, 2, 9, 0, 0), (2014, 1, 4, 10, 0, 0)),
                    ((2014, 1, 3, 10, 0, 0), (2014, 1, 3, 11, 0, 0)))

        da_ute = []
        tl = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            tl.add(te)
            da_ute.append(te)

        ite_entries = tl.get_ite_entries(None, None)
        ts_ute_id = []
        for ite in ite_entries:
            if ite.ute.ute_id in ts_ute_id:
                continue
            ts_ute_id.append(ite.ute.ute_id)

        self.assertEqual(len(ts_ute_id), 3)
        self.assertIn(da_ute[0].ute_id, ts_ute_id)
        self.assertIn(da_ute[1].ute_id, ts_ute_id)
        self.assertIn(da_ute[2].ute_id, ts_ute_id)

    def test_time_totals(self):
        data_set = (((2014, 1, 1, 8, 0, 0), (2014, 1, 1, 9, 0, 0)),   # 1 hr
                    ((2014, 1, 2, 9, 0, 0), (2014, 1, 2, 10, 0, 0)),  # 1 hr
                    ((2014, 2, 3, 10, 0, 0), (2014, 2, 3, 11, 0, 0))) # 1 hr
        tl = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            tl.add(te)
        self.assertEqual(tl.get_total_time_mins(), (3 * 60))
        self.assertEqual(tl.get_total_time_mins(key="year-2014"), (3 * 60))
        self.assertEqual(tl.get_total_time_mins(key="month-2014-01"), (2 * 60))
        self.assertEqual(tl.get_total_time_mins(key="month-2014-02"), (1 * 60))
        self.assertEqual(tl.get_total_time_mins(key="week-2014-01"), (2 * 60))
        self.assertEqual(tl.get_total_time_mins(key="week-2014-06"), (1 * 60))
        from decimal import Decimal as D
        self.assertEqual(tl.get_total_time_hours(), D(3))
        self.assertEqual(tl.get_total_time_hours(key="year-2014"), D(3))
        self.assertEqual(tl.get_total_time_hours(key="month-2014-01"), D(2))
        self.assertEqual(tl.get_total_time_hours(key="month-2014-02"), D(1))
        self.assertEqual(tl.get_total_time_hours(key="week-2014-01"), D(2))
        self.assertEqual(tl.get_total_time_hours(key="week-2014-06"), D(1))


class TestOverlappingEntries(unittest.TestCase):

    def setUp(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 3, 10, 0, 0)),
                    ((2014, 1, 1, 10, 30, 0), (2014, 1, 1, 11, 30, 0)),
                    ((2014, 1, 2, 12, 0, 0), (2014, 1, 3, 18, 30, 0)),
                    ((2014, 1, 3, 8, 30, 0), (2014, 1, 3, 10, 0, 0)))
        self.data_ute = []
        self.timelog = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            self.timelog.add(te)
            self.data_ute.append(te)

    def test_conflict_start_contained(self):
        s_dtime = datetime.datetime(2014, 1, 1, 9, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 9, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertEqual(ent[0], self.data_ute[0])

    def test_conflict_match_old_exactly(self):
        s_dtime = datetime.datetime(2014, 1, 1, 10, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 11, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 2)
        status, ent_list = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertIn(ent_list[0], self.data_ute)
        self.assertIn(ent_list[1], self.data_ute)
        self.assertIn(ent_list[0].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[1].ute_id))
        self.assertIn(ent_list[1].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[1].ute_id))

    def test_conflict_contained(self):
        s_dtime = datetime.datetime(2014, 1, 1, 10, 45, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 11, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 2)
        status, ent_list = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertIn(ent_list[0], self.data_ute)
        self.assertIn(ent_list[1], self.data_ute)
        self.assertIn(ent_list[0].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[1].ute_id))
        self.assertIn(ent_list[1].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[1].ute_id))

    def test_conflict_contained_and_pre(self):
        s_dtime = datetime.datetime(2014, 1, 1, 9, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 10, 45, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 1)
        status, ent_list = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertEqual(ent_list[0], self.data_ute[0])

    def test_conflict_contained_and_post(self):
        s_dtime = datetime.datetime(2014, 1, 2, 14, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 10, 45, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 1)
        status, ent_list = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertEqual(ent_list[0], self.data_ute[2])

    def test_conflict_end_contained(self):
        s_dtime = datetime.datetime(2014, 1, 3, 9, 40, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 10, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list[0][1]), 3)
        status, ent_list = overlap_list[0]
        self.assertEqual(status, 'conflict')
        self.assertIn(ent_list[0], self.data_ute)
        self.assertIn(ent_list[1], self.data_ute)
        self.assertIn(ent_list[2], self.data_ute)
        self.assertIn(ent_list[0].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[2].ute_id,
                                           self.data_ute[3].ute_id))
        self.assertIn(ent_list[1].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[2].ute_id,
                                           self.data_ute[3].ute_id))
        self.assertIn(ent_list[2].ute_id, (self.data_ute[0].ute_id,
                                           self.data_ute[2].ute_id,
                                           self.data_ute[3].ute_id))

    def test_pre(self):
        s_dtime = datetime.datetime(2014, 1, 1, 8, 15, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 10, 15, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'pre')
        self.assertEqual(ent, self.data_ute[0])

    def test_pre_and_contains(self):
        s_dtime = datetime.datetime(2014, 1, 1, 8, 15, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 12, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)
        status, ent = overlap_list[0]
        self.assertIn(status, ('pre', 'contains'))
        if status == 'pre':
            self.assertEqual(ent, self.data_ute[0])
        else:
            self.assertEqual(ent, self.data_ute[1])
        status, ent = overlap_list[0]
        self.assertIn(status, ('pre', 'contains'))
        if status == 'pre':
            self.assertEqual(ent, self.data_ute[0])
        else:
            self.assertEqual(ent, self.data_ute[1])


    def test_post(self):
        s_dtime = datetime.datetime(2014, 1, 3, 14, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 19, 15, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'post')
        self.assertEqual(ent, self.data_ute[2])

    def test_post_and_contains(self):
        s_dtime = datetime.datetime(2014, 1, 3, 8, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 19, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 3)
        status, ent = overlap_list[0]
        self.assertIn(status, ('post', 'contains'))
        if status == 'contains':
            self.assertEqual(ent, self.data_ute[3])
        else:
            self.assertIn(ent, (self.data_ute[0], self.data_ute[2]))
        status, ent = overlap_list[0]
        self.assertIn(status, ('post', 'contains'))
        if status == 'contains':
            self.assertEqual(ent, self.data_ute[3])
        else:
            self.assertIn(ent, (self.data_ute[0], self.data_ute[2]))

        status, ent = overlap_list[1]
        self.assertIn(status, ('post', 'contains'))
        if status == 'contains':
            self.assertEqual(ent, self.data_ute[3])
        else:
            self.assertIn(ent, (self.data_ute[0], self.data_ute[2]))


    def test_all_contains(self):
        s_dtime = datetime.datetime(2014, 1, 1, 8, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 19, 15, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 4)

    def test_multiple_contains(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 1, 10, 30, 0)),
                    ((2014, 1, 1, 10, 30, 0), (2014, 1, 2, 11, 30, 0)),
                    ((2014, 1, 2, 12, 0, 0), (2014, 1, 3, 18, 30, 0)),
                    ((2014, 1, 3, 8, 30, 0), (2014, 1, 3, 10, 0, 0)))
        data_ute = []
        timelog = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            timelog.add(te)
            data_ute.append(te)

        s_dtime = datetime.datetime(2014, 1, 1, 8, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 11, 45, 0)
        overlap_list = get_overlapping_entries(timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)

    def test_multiple_contains_match_old_exactly(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 1, 10, 30, 0)),
                    ((2014, 1, 1, 10, 30, 0), (2014, 1, 2, 11, 30, 0)),
                    ((2014, 1, 2, 12, 0, 0), (2014, 1, 3, 18, 30, 0)),
                    ((2014, 1, 3, 8, 30, 0), (2014, 1, 3, 10, 0, 0)))
        data_ute = []
        timelog = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            timelog.add(te)
            data_ute.append(te)

        s_dtime = datetime.datetime(2014, 1, 1, 9, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 11, 30, 0)
        overlap_list = get_overlapping_entries(timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)


    def test_pre_non_overlapping(self):
        s_dtime = datetime.datetime(2014, 1, 1, 7, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 1, 9, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 0)

    def test_post_non_overlapping(self):
        s_dtime = datetime.datetime(2014, 1, 3, 18, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 19, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 0)

    def test_inter_non_overlapping(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 1, 10, 30, 0)),
                    ((2014, 1, 1, 10, 30, 0), (2014, 1, 2, 11, 30, 0)),
                    ((2014, 1, 2, 12, 0, 0), (2014, 1, 3, 18, 30, 0)),
                    ((2014, 1, 3, 8, 30, 0), (2014, 1, 3, 10, 0, 0)))
        data_ute = []
        timelog = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            timelog.add(te)
            data_ute.append(te)

        s_dtime = datetime.datetime(2014, 1, 2, 11, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 12, 0, 0)
        overlap_list = get_overlapping_entries(timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 0)


class TestFitTimeEntries(unittest.TestCase):

    def setUp(self):
        data_set = (((2014, 1, 1, 9, 0, 0), (2014, 1, 1, 10, 0, 0)),
                    ((2014, 1, 1, 10, 30, 0), (2014, 1, 2, 11, 30, 0)),
                    ((2014, 1, 2, 12, 0, 0), (2014, 1, 3, 10, 0, 0)),
                    ((2014, 1, 3, 8, 30, 0), (2014, 1, 3, 18, 30, 0)))

        self.data_ute = []
        self.timelog = TimeLog()
        for d in data_set:
            te = UITimeEntry(datetime.datetime(*d[0]),
                             datetime.datetime(*d[1]))
            self.timelog.add(te)
            self.data_ute.append(te)

    def test_contains_fit_new_entry_in_gap(self):
        s_dtime = datetime.datetime(2014, 1, 1, 10, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 12, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'contains')
        self.assertEqual(ent, self.data_ute[1])

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 2)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime, s_dtime)
        self.assertEqual(new_ute.end_datetime,
                         datetime.datetime(2014, 1, 1, 10, 30, 0))

        new_ute = split_list[1]
        self.assertEqual(new_ute.start_datetime,
                         datetime.datetime(2014, 1, 2, 11, 30, 0))
        self.assertEqual(new_ute.end_datetime, e_dtime)


    def test_contains_fit_new_pre_entry(self):
        s_dtime = datetime.datetime(2014, 1, 1, 10, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 10, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'pre')
        self.assertEqual(ent, self.data_ute[1])

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 1)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime, s_dtime)
        self.assertEqual(new_ute.end_datetime,
                         datetime.datetime(2014, 1, 1, 10, 30, 0))

    def test_contains_fit_new_post_entry(self):
        s_dtime = datetime.datetime(2014, 1, 3, 10, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 19, 0, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 1)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'post')
        self.assertEqual(ent, self.data_ute[3])

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 1)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime,
                         datetime.datetime(2014, 1, 3, 18, 30, 0))
        self.assertEqual(new_ute.end_datetime, e_dtime)


    def test_contains_fit_new_pre_and_post_entries(self):
        s_dtime = datetime.datetime(2014, 1, 1, 18, 0, 0)
        e_dtime = datetime.datetime(2014, 1, 2, 12, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)
        status, ent = overlap_list[0]
        self.assertIn(status, ('post', 'pre'))
        if status == 'post':
            self.assertEqual(ent, self.data_ute[1])
        else:
            self.assertEqual(ent, self.data_ute[2])
        status, ent = overlap_list[1]
        self.assertIn(status, ('post', 'pre'))
        if status == 'post':
            self.assertEqual(ent, self.data_ute[1])
        else:
            self.assertEqual(ent, self.data_ute[2])

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 1)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime,
                         datetime.datetime(2014, 1, 2, 11, 30, 0))
        self.assertEqual(new_ute.end_datetime,
                         datetime.datetime(2014, 1, 2, 12, 0, 0))

    def test_pre_fit_new_pre_entry(self):
        s_dtime = datetime.datetime(2014, 1, 2, 11, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 3, 9, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'pre')
        self.assertIn(ent, (self.data_ute[2], self.data_ute[3]))

        status, ent = overlap_list[0]
        self.assertEqual(status, 'pre')
        self.assertIn(ent, (self.data_ute[2], self.data_ute[3]))

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 1)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime,
                         datetime.datetime(2014, 1, 2, 11, 30, 0))
        self.assertEqual(new_ute.end_datetime,
                         datetime.datetime(2014, 1, 2, 12, 0, 0))


    def test_post_fit_new_post_entry(self):
        s_dtime = datetime.datetime(2014, 1, 3, 9, 30, 0)
        e_dtime = datetime.datetime(2014, 1, 4, 9, 30, 0)
        overlap_list = get_overlapping_entries(self.timelog, s_dtime, e_dtime)
        self.assertEqual(len(overlap_list), 2)
        status, ent = overlap_list[0]
        self.assertEqual(status, 'post')
        self.assertIn(ent, (self.data_ute[2], self.data_ute[3]))

        status, ent = overlap_list[0]
        self.assertEqual(status, 'post')
        self.assertIn(ent, (self.data_ute[2], self.data_ute[3]))

        ute = UITimeEntry(s_dtime, e_dtime)
        split_list = fit_time_entry(ute, overlap_list)
        self.assertEqual(len(split_list), 1)
        new_ute = split_list[0]
        self.assertEqual(new_ute.start_datetime,
                         datetime.datetime(2014, 1, 3, 18, 30, 0))
        self.assertEqual(new_ute.end_datetime,
                         datetime.datetime(2014, 1, 4, 9, 30, 0))
