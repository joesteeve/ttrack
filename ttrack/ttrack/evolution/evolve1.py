# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from ttrack.auth import PERMISSIONS
from ttrack.users.models import User

def evolve(root):
    """This evolve removes the concept of a system generated role. There
    are two system generated roles by default 'admin' and 'user'. We
    need to do the following:

    1. Remove the role information from user whose userid is 'admin',
    now 'admin' user is treated in a special way

    2. Delete the 'user' role, which will cause all the users in that
    role to be mapped to None

    3. Modify the 'admin' role to include all the defined permissions
    on it

    4. Delete the 'system' attribute from all the roles

    5. Set the ACL on the root object explicitly even though it is
    done implicitly by the IObjectModifiedEvent handler of IRole
    """
    log.info("Evolving to step 1: remove the concept of system role")
    users = root['users']
    roles = root['roles']

    # Step 1
    admin = users.get('admin', None)
    if admin is None:
        # 'admin' user has been deleted so re-create it
        email_id = "admin%d@admin.org"
        i = 1
        # Find unique available 'admin' email ID
        while users.find_with_email_id(email_id % i) is not None:
            i += 1
        di = {
            'userid' : 'admin',
            'real_name' : 'Administrator',
            'password': 'admin',
            'email_id': email_id % i
        }
        log.info("'admin' user not found, re-creating it")
        u = User.from_dict(di)
        users.add(u)
    else:
        admin.role = None
    log.info("'admin' user will now have full rights, irrespective of previous "
             "privileges assigned to it.")

    # Step 2: Deleting 'user' role will set User.role to None for the
    # Users belonging to that role by the handler
    user_role = roles.get('user', None)
    if user_role is not None and user_role.system is True:
        # Ensure that it is indeed an old style 'user' role
        roles.remove('user')

    # Step 3: Give all defined permissions to the 'admin' role but
    # from now on this 'admin' role will be less powerful
    # (theoretically) than the system 'admin' user
    admin_role = roles.get('admin', None)
    if admin_role is not None and admin_role.system is True:
        # Ensure that it is indeed an old style 'admin' role
        for p in PERMISSIONS.keys():
            admin_role.add_permission(p)

    # Step 4: Remove the system attribute from all the roles
    for r in roles.values():
        if hasattr(r, 'system'):
            del r.system

    # Step 5: This step should be performed automatically by the event
    # handlers (handle_role_modification) but we do it here again for
    # explicity sake
    root.set_acl(roles.get_acl())
    log.info("ZODB successfully evolved to step 1")
