# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from persistent import Persistent
from persistent.mapping import PersistentMapping
from BTrees.OOBTree import OOBTree

from ttrack import errors

from .timeentry import (
    UITimeEntry,
    InternalTimeEntry,
)

from .timelog import (
    TimeLog,
    intersecting_timelog,
)

from .utils import (
    get_or_create_timelog,
    cleanup_timeentry,
)


class ClientTimeSheet(Persistent):
    """Per client time-sheet

    This time sheet object maintains the following relationships:
    a) intersection(C, U)
    b) intersection(C, T)

    Where C => Client
          U => User
          T => Tag
    """

    def __init__(self):
        super(ClientTimeSheet, self).__init__()
        self._all = TimeLog()
        self._users = PersistentMapping()
        self._tags = PersistentMapping()

    def add(self, timeentry):
        self._all.add(timeentry)
        # add to client.user timelog
        tl = get_or_create_timelog(self._users, timeentry.user)
        tl.add(timeentry)
        # add to project.tag timelog
        for tag in timeentry.tags:
            tl = get_or_create_timelog(self._tags, tag)
            tl.add(timeentry)

    def remove(self, timeentry):
        self._all.remove(timeentry)
        cleanup_timeentry(self._users, timeentry.user, timeentry)
        for tag in timeentry.tags:
            cleanup_timeentry(self._tags, tag, timeentry)

    def remove_tag(self, tag):
        if tag in self._tags:
            del(self._tags[tag])

    def get_all(self):
        return self._all

    def get_user(self, user):
        return self._users.get(user, TimeLog())

    def get_tag(self, tag):
        return self._tags.get(tag, TimeLog())

    def get_statistics_for_time_key(self, time_key):
        u_stats = {}
        for user, timelog in self._users.items():
            u_stats[user.userid] = timelog.get_total_time_mins(key=time_key)
        t_stats = {}
        for tag, timelog in self._tags.items():
            t_stats[tag.name] = timelog.get_total_time_mins(key=time_key)
        return {
            'users' : u_stats,
            'tags' : t_stats,
            'total' : self._all.get_total_time_mins(key=time_key),
        }

    def get_statistics_for_range(self, start_datetime, end_datetime):
        users = {}
        tags = {}
        total = 0

        for entry in self._all.get_ite_entries(start_datetime, end_datetime):
            mins = entry.effective_time_mins()
            # per-user time-spent
            if entry.user.userid not in users:
                users[entry.user.userid] = 0
            users[entry.user.userid] += mins
            # per-tag time-spent
            for tag in entry.tags:
                if tag.name not in tags:
                    tags[tag.name] = 0
                tags[tag.name] += mins
            # total time-spent
            total += mins

        return {
            'users' : users,
            'tags' : tags,
            'total' : total,
        }
