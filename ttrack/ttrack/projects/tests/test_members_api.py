# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    HTTPMethodNotAllowed,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.projects.models import (
    ProjectContainer,
    Project,
    Member,
    )

from ttrack.users.models import User, UserContainer, find_group
from ttrack.clients.models import (
    ClientContainer,
    Client,
    )

USER_DATA = (
    {"userid" : "admin",
     "real_name" : "Administrator",
     "email_id" : "admin@admin.org",
     "password" : "pass"},
    {"userid" : "john",
     "real_name" : "John Doe",
     "email_id" : "john@doe.org",
     "password" : "john"},
    {"userid" : "smith",
     "real_name" : "Jane Smith",
     "email_id" : "jane@smith.org",
     "password" : "jane"}
)

CLIENT_DATA = (
    {
        'client_id' : 'c1',
        'name': 'Client 1',
        'address': 'c1 Address',
        },
    {
        'client_id' : 'c2',
        'name' : 'Client 2',
        'address' : 'c2 Address',
        },
    {
        'client_id' : 'c3',
        'name' : 'Client 3',
        'address' : 'c3 Address',
        },
    )

PROJECT_DATA = (
    {
        'project_id' : 'p1',
        'client' : 'c1',
        'name': 'Project One',
        'members': ['admin', 'john', 'smith']
    },
    {
        'project_id' : 'p2',
        'client' : 'c2',
        'name' : 'Project Two',
        'members' : ['admin']
    },
    {
        'project_id' : 'p3',
        'client' : 'c3',
        'name' : 'Project Three',
        'userid' : 'john',
        'members' : ['smith']
    },
)


class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "projects.view"),
               (Allow, Authenticated, 'view'),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(udata, cdata, pdata):
    root = TestRoot()
    root['users'] = UserContainer()
    root['clients'] = ClientContainer()
    root['projects'] = ProjectContainer()

    for u in udata:
        user = User.from_dict(u)
        root['users'].add(user)

    for c in cdata:
        client = Client.from_dict(c)
        root['clients'].add(client)

    for p in pdata:
        pd = {}
        pd.update(p)
        pd['client'] = root['clients'].get(p['client'])
        project = Project.from_dict(pd)
        for userid in pd['members']:
            mem = Member(root['users'].get(userid))
            mem.role = project.roles.get('member')
            project.add_member(mem)
        if 'userid' in p:
            mem = Member(root['users'].get(p['userid']))
            mem.role = project.roles.get('admin')
            project.add_member(mem)
        root['projects'].add(project)

    def root_factory(request):
        return root
    return root_factory

class MemberAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/members/p1'
        self.api_url_p2 = '/api/v1/members/p2'
        self.api_url_p3 = '/api/v1/members/p3'
        self.api_url_not_exist = '/api/v1/members/p4' # non existent
        self.p1_data = PROJECT_DATA[0]['members']
        self.p2_data = PROJECT_DATA[1]['members']
        self.p3_data = PROJECT_DATA[2]['members']

        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all_fail_for_non_existent_project(self):
        self.app.get(self.api_url_not_exist, status=HTTPNotFound.code)

    def test_get_all_members_for_project(self):
        resp = self.app.get(self.api_url_p1, status=HTTPOk.code)
        members = resp.json['result']
        self.assertEqual(len(members), len(self.p1_data))
        resp = self.app.get(self.api_url_p2, status=HTTPOk.code)
        members = resp.json['result']
        self.assertEqual(len(members), len(self.p2_data))
        resp = self.app.get(self.api_url_p3, status=HTTPOk.code)
        members = resp.json['result']
        self.assertEqual(len(members), len(self.p3_data) + 1)#for admin user)

    def test_add_new_member_fail_for_non_existent_project(self):
        data = {'userid' : 'john'}
        self.app.post_json(self.api_url_not_exist, data,
                           status=HTTPBadRequest.code)

    def test_add_new_member_fail_on_non_existent_role_id(self):
        data = {
            'userid': 'john',
            'project_role_id' : 'member2'
        }
        self.app.post_json(self.api_url_p2, data, status=HTTPBadRequest.code)

    def test_add_new_member_fail_on_existing_member(self):
        data = {
            'userid': 'john',
            'project_role_id' : 'member'
        }
        self.app.post_json(self.api_url_p1, data, status=HTTPBadRequest.code)

    def test_add_new_member_fail_for_invalid_user(self):
        data = {'userid' : 'jane'}
        self.app.post_json(self.api_url_p2, data, status=HTTPBadRequest.code)

    def test_add_new_member_fail_for_duplicate_user(self):
        data = {'userid' : 'smith'}
        self.app.post_json(self.api_url_p3, data, status=HTTPBadRequest.code)

    def test_add_new_member_fail_on_bad_schema(self):
        data = {}
        self.app.post_json(self.api_url_p3, data, status=HTTPBadRequest.code)

    def test_add_new_member_missing_role_id(self):
        data = {'userid' : 'smith'}
        resp = self.app.post_json(self.api_url_p2, data, status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], data['userid'])
        self.assertEqual(user['role_id'], '')

    def test_add_new_member(self):
        data = {
            'userid': 'john',
            'project_role_id' : 'member'
        }
        resp = self.app.post_json(self.api_url_p2, data, status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], data['userid'])
        self.assertEqual(user['role_id'], data['project_role_id'])

    def test_get_fail_on_non_member(self):
        self.app.get(self.api_url_p2 + "/smith.json", status=HTTPNotFound.code)

    def test_get_fail_on_invalid_member(self):
        self.app.get(self.api_url_p1 + "/jane.json", status=HTTPNotFound.code)

    def test_get_fail_on_non_existent_project(self):
        self.app.get(self.api_url_not_exist + "/smith.json",
                     status=HTTPNotFound.code)

    def test_get_member(self):
        resp = self.app.get(self.api_url_p1 + "/john.json", status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'john')

    def test_put_fail_on_non_member(self):
        data = {'userid' : 'john'}
        self.app.put_json(self.api_url_p2 + "/john.json", data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_readonly_userid(self):
        data = {'userid' : 'john'}
        self.app.put_json(self.api_url_p3 + "/smith.json", data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_invalid_role_id(self):
        data = {
            'userid' : 'john',
            'project_role_id' : 'member2',
        }
        self.app.put_json(self.api_url_p1 + "/john.json", data,
                          status=HTTPBadRequest.code)

    def test_put(self):
        data = {
            'userid' : 'john',
            'project_role_id' : 'manager',
        }
        resp = self.app.put_json(self.api_url_p1 + "/john.json", data,
                                 status=HTTPOk.code)
        member = resp.json['result']
        self.assertEqual(member['role_id'], 'manager')

    def test_put_remove_role_id(self):
        data = { 'userid' : 'john' }
        resp = self.app.get(self.api_url_p1 + '/john.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['role_id'], 'member')
        self.app.put_json(self.api_url_p1 + "/john.json", data,
                          status=HTTPOk.code)
        resp = self.app.get(self.api_url_p1 + '/john.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['role_id'], '')

    def test_put_remove_empty_role_id(self):
        data = {
            'userid' : 'john',
            'project_role_id' : '',

        }
        resp = self.app.get(self.api_url_p1 + '/john.json', status=HTTPOk.code)
        old_user = resp.json['result']
        self.assertEqual(old_user['role_id'], 'member')
        self.app.put_json(self.api_url_p1 + "/john.json", data,
                          status=HTTPOk.code)
        new_resp = self.app.get(self.api_url_p1 + '/john.json',
                                status=HTTPOk.code)
        new_user = new_resp.json['result']
        self.assertEqual(new_user['role_id'], '')

    def test_delete_fail_on_non_member(self):
        self.app.delete(self.api_url_p2 + "/smith.json",
                        status=HTTPNotFound.code)

    def test_delete_fail_on_invalid_user(self):
        self.app.delete(self.api_url_p2 + "/jane.json",
                        status=HTTPNotFound.code)

    def test_delete(self):
        self.app.delete(self.api_url_p1 + "/smith.json",
                        status=HTTPOk.code)
        self.app.get(self.api_url_p1 + "/smith.json", status=HTTPNotFound.code)


class MemeberAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/members/p1'
        self.api_url_p2 = '/api/v1/members/p2'
        self.api_url_p3 = '/api/v1/members/p3'
        self.api_url_not_exist = '/api/v1/members/p4' # non existent
        self.p1_data = PROJECT_DATA[0]['members']
        self.p2_data = PROJECT_DATA[1]['members']
        self.p3_data = PROJECT_DATA[2]['members']

        self.config = testing.setUp()
        self.config.include("cornice")
        request = testing.DummyRequest()
        root_factory = build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA)
        request.root = root_factory(None)
        self.config.testing_securitypolicy(
            userid='john', groupids=find_group('john', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(root_factory)
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_all(self):
        self.app.get(self.api_url_p1, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url_p1 + "/smith.json", status=HTTPOk.code)

    def test_deny_post(self):
        data = {'userid' : 'john'}
        self.app.post_json(self.api_url_p2, data, status=HTTPForbidden.code)

    def test_deny_put(self):
        data = {'userid' : 'smith'}
        self.app.put_json(self.api_url_p1 + "/smith.json", data,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url_p1 + "/smith.json",
                        status=HTTPForbidden.code)


class MemberAPIProjectAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/members/p1'
        self.api_url_p2 = '/api/v1/members/p2'
        self.api_url_p3 = '/api/v1/members/p3'
        self.api_url_not_exist = '/api/v1/members/p4' # non existent
        self.p1_data = PROJECT_DATA[0]['members']
        self.p2_data = PROJECT_DATA[1]['members']
        self.p3_data = PROJECT_DATA[2]['members']

        self.config = testing.setUp()
        self.config.include("cornice")
        request = testing.DummyRequest()
        root_factory = build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA)
        request.root = root_factory(None)
        self.config.testing_securitypolicy(
            userid='john', groupids=find_group('john', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(root_factory)
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_all(self):
        self.app.get(self.api_url_p3, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url_p3 + "/smith.json", status=HTTPOk.code)

    def test_allow_post(self):
        data = {'userid' : 'admin'}
        self.app.post_json(self.api_url_p3, data, status=HTTPOk.code)

    def test_allow_put(self):
        data = {'userid' : 'smith'}
        self.app.put_json(self.api_url_p3 + "/smith.json", data,
                          status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url_p3 + "/smith.json",
                        status=HTTPOk.code)


class MemberAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/members/p1'
        self.api_url_p2 = '/api/v1/members/p2'
        self.api_url_p3 = '/api/v1/members/p3'
        self.api_url_not_exist = '/api/v1/members/p4' # non existent
        self.p1_data = PROJECT_DATA[0]['members']
        self.p2_data = PROJECT_DATA[1]['members']
        self.p3_data = PROJECT_DATA[2]['members']

        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_all(self):
        self.app.get(self.api_url_p1, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url_p3 + "/smith.json", status=HTTPForbidden.code)

    def test_deny_post(self):
        data = {'userid' : 'john'}
        self.app.post_json(self.api_url_p2, data, status=HTTPForbidden.code)

    def test_deny_put(self):
        data = {'userid' : 'john'}
        self.app.put_json(self.api_url_p2 + "/john.json", data,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url_p1 + "/smith.json",
                        status=HTTPForbidden.code)
