# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

_SDT = None

def _compute_start_datetime():
    start_date = datetime.date.today()
    start_date -= datetime.timedelta(days=50)
    return datetime.datetime.combine(start_date, datetime.time(0, 0))

def DT(day, hour, mins):
    global _SDT
    if _SDT is None:
        _SDT = _compute_start_datetime()
    offset = datetime.timedelta(days=day, hours=hour, minutes=mins)
    return _SDT + offset


CLIENTS = (
    {
        'client_id' : 'doe_chem',
        'name': 'Doe Chemicals',
        'address': '50 degrees SSW, Antartica',
    },

    {
        'client_id' : 'mary_dance',
        'name': 'Mary Dance School',
        'address': "On top of Joshua's house",
    },
)

PROJECTS = (
    {
        'project_id' : 'cfactory',
        'client' : 'doe_chem',
        'name': 'Factory setup analysis',
        'activities': [
            'Initial feasibility analysis visit',
            'Detailed audit of immovable assets',
            'Soil strength analyss',
            'Environmental analysis'
        ],
        'members' : ['john', 'mari'],
        'userid' : 'admin',
    },

    {
        'project_id' : 'coovum_cleanup',
        'client' : 'doe_chem',
        'name': 'Coovum Cleanup Requirement Analysis',
        'activities': [
            'Survey of Water flow path',
            'Survey of water depth',
            'Opinion poll from inhabitants on coast',
            'Collection of past dredging efforts',
            'Analysis of actual cost without bribes',
        ],
        'members' : ['mari',],
        'userid' : 'john',
    },

    {
        'project_id' : 'md_studio_interiors',
        'client' : 'mary_dance',
        'name': 'Acoustic design',
        'activities': [
            'Get rid of all students',
            'Pull down walls',
            'Put up new walls',
            'Plaster interior with sound-proof material',
            'Deploy echo cancellation edges',
            'analyse appropriate furniture',
            'Bring back the students',
        ],
        'members' : ['admin', 'mari'],
        'userid' : 'john',
    },
)

ROLES = (
    {
        'role_id' : 'admin',
        'name' : 'Administrator',
        'permissions' : ['Manage users', 'Manage clients', 'Manage projects',
                         'Manage tags', 'Manage roles'],
    },
    {
        'role_id' : 'user',
        'name' : 'Normal User',
        'permissions' : ['Manage tags',],
    },
    {
        'role_id' : 'manager',
        'name' : 'Project Manager',
        'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                         'Manage roles',],
    },
)


USERS = (
    {
        'userid' : 'john',
        'real_name' : 'John doe',
        'password' : 'john',
        'email_id' : 'john@user.com',
        'role_id' : 'user',
    },

    {
        'userid' : 'admin',
        'real_name' : 'Jane Kirkland',
        'password' : 'jane',
        'email_id' : 'jane@user.com',
        'role_id' : '',
    },

    {
        'userid' : 'mari',
        'real_name' : 'Marimuthu',
        'password' : 'mmuthu',
        'email_id' : 'mmuthu@user.com',
        'role_id' : 'manager',
    },
)

TIMEENTRIES = (
    {
        'start_datetime': DT(0, 8, 0),
        'end_datetime': DT(0, 20, 0),
        'project_id' : 'mary_dance',
        'activity' : 'get rid of all students',
        'userid' : 'mari',
        'tags' : ('wrestling', )

    },

    {
        'start_datetime': DT(1, 10, 30),
        'end_datetime': DT(4, 20, 0),
        'project_id' : 'mary_dance',
        'activity' : 'pull down walls',
        'userid' : 'mari',
        'tags' : ('breaking', )
    },
)
