# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

# cornice view error handlers

def body_userid_exists(request):
    users = request.root['users']
    userid = request.json.get("userid", None)
    if userid and userid not in users:
        request.errors.add(
            'body', 'userid', "User='%s' does not exist" % userid)
        request.errors.status = HTTPBadRequest.code

def qs_userid_exists(request):
    users = request.root['users']
    userid = request.params.get("userid", None)
    if userid and userid not in users:
        request.errors.add(
            'querystring', 'userid', "User='%s' does not exist" % userid)
        request.errors.status = HTTPBadRequest.code

def url_userid_exists(request):
    users = request.root['users']
    userid = request.matchdict["userid"]
    if userid not in users:
        request.errors.add(
            'url', 'userid', "User='%s' does not exist" % userid)
        request.errors.status = HTTPNotFound.code

def strict_url_userid_exists(request):
    users = request.root['users']
    userid = request.matchdict["userid"]
    if userid not in users:
        request.errors.add(
            'url', 'userid', "User='%s' does not exist" % userid)
        request.errors.status = HTTPBadRequest.code

# pyramid view error handlers

def pv_url_userid_exists(request):
    users = request.root['users']
    userid = request.matchdict["userid"]
    if userid not in users:
        raise HTTPNotFound("User='%s' does not exist" % userid)

def pv_qs_userid_exists(request):
    users = request.root['users']
    userid = request.params.get("userid", None)
    if userid and userid not in users:
        raise HTTPBadRequest("User='%s' does not exist" % userid)
