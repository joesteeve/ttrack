# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

# cornice view error handlers

def body_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.json.get("project_id", None)
    if project_id and project_id not in projects:
        request.errors.add(
            'body', 'project_id', "Project='%s' does not exist" % project_id)
        request.errors.status = HTTPBadRequest.code

def qs_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.params.get("project_id", None)
    if project_id and project_id not in projects:
        request.errors.add('querystring', 'project_id',
                           "Project='%s' does not exist" % project_id)
        request.errors.status = HTTPBadRequest.code

def url_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.matchdict["project_id"]
    if project_id not in projects:
        request.errors.add(
            'url', 'project_id', "Project='%s' does not exist" % project_id)
        request.errors.status = HTTPNotFound.code

def strict_url_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.matchdict["project_id"]
    if project_id not in projects:
        request.errors.add(
            'url', 'project_id', "Project='%s' does not exist" % project_id)
        request.errors.status = HTTPBadRequest.code

# pyramid view error handlers

def pv_url_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.matchdict["project_id"]
    if project_id not in projects:
        raise HTTPNotFound("Project='%s' does not exist" % project_id)

def pv_qs_project_id_exists(request):
    projects = request.root['projects']
    project_id = request.params.get("project_id", None)
    if project_id and project_id not in projects:
        raise HTTPBadRequest("Project='%s' does not exist" % project_id)
