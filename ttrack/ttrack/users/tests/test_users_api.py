# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.users import api
from ttrack.users import models
from ttrack.roles import Role, RoleContainer

ROLE_DATA = (
    {
        'role_id' : 'manager',
        'name' : 'Project Manager',
        'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                         'Manage roles',],
    },
)

USER_DATA = (
    {"userid" : "admin",
     "real_name" : "Administrator",
     "email_id" : "admin@admin.org",
     "password" : "pass",
     "role_id" : ""},

    {"userid" : "john",
     "real_name" : "John Doe",
     "email_id" : "john@doe.org",
     "password" : "john",
     "role_id" : "user"}
    )

def user_in_list(data, user_list):
    for user in user_list:
        if data['userid'] == user['userid']:
            return True
    return False

class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "users.view"),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(role_data, user_data):
    root = TestRoot()
    root['roles'] = RoleContainer()
    for r in role_data:
        role = Role.from_dict(r)
        root['roles'].add(role)

    root['users'] = models.UserContainer()
    for u in user_data:
        user = models.User.from_dict(u)
        if u['role_id']:
            user.role = root['roles'].get(u['role_id'])
        root['users'].add(user)

    def root_factory(request):
        return root
    return root_factory


class UserAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/users'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all_users(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        users = resp.json['result']
        self.assertEqual(len(users), 2)
        for d in USER_DATA:
            self.assertTrue(user_in_list(d, users))

    def test_get_user_with_existing_email_id(self):
        resp = self.app.get(self.api_url + '?email_id=admin@admin.org',
                            status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'admin')

    def test_get_user_with_non_existant_email_id(self):
        resp = self.app.get(self.api_url + '?email_id=bad@email.org',
                            status=HTTPNotFound.code)
        self.assertTrue(resp.json['status'], True)
        self.assertEqual(resp.json['errors'][0]['name'], 'email_id')

    def test_get_user_collection_with_bad_param(self):
        resp = self.app.get(self.api_url + '?blah=bad@email.org',
                            status=HTTPBadRequest.code)
        self.assertEqual(resp.json['status'], 'error')

    def test_get_user(self):
        resp = self.app.get(self.api_url + '/admin.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'admin')

    def test_get_user_case_insensitive(self):
        resp = self.app.get(self.api_url + '/AdmIn.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'admin')

    def test_get_non_existant_user(self):
        resp = self.app.get(self.api_url + '/invalid.json',
                            status=HTTPNotFound.code)

    def test_add_new_user(self):
        user = {
            'userid' : 'mary',
            'real_name' : 'Mary Jane',
            'email_id' : 'mary@maryjane.com',
            'password' : 'mary',
            'role_id' : 'manager',
            }
        resp = self.app.post_json(self.api_url, user, status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'mary')

        resp = self.app.get(self.api_url + '/mary.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'mary')

    def test_add_new_user_case_insensitive_userid(self):
        user = {
            'userid' : 'MaRy',
            'real_name' : 'Mary Jane',
            'email_id' : 'mary@maryjane.com',
            'password' : 'mary',
            'role_id' : 'manager',
            }
        resp = self.app.post_json(self.api_url, user, status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'mary')

        resp = self.app.get(self.api_url + '/mary.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['userid'], 'mary')

    def test_add_user_with_non_unique_userid(self):
        user = {
            'userid' : 'admin',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'duplicate@admin.com',
            'password' : 'admin',
            }
        resp = self.app.post_json(self.api_url, user,
                                  status=HTTPBadRequest.code)

    def test_add_user_fail_with_case_insensitive_non_unique_userid(self):
        user = {
            'userid' : 'ADMIN',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'duplicate@admin.com',
            'password' : 'admin',
            }
        resp = self.app.post_json(self.api_url, user,
                                  status=HTTPBadRequest.code)

    def test_add_user_with_non_unique_email_id(self):
        user = {
            'userid' : 'mary',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'admin@admin.org',
            'password' : 'admin',
            }
        resp = self.app.post_json(self.api_url, user,
                                  status=HTTPBadRequest.code)

    def test_add_user_with_bad_schema(self):
        user = {
            'userid' : 'mary',
            'real_name' : 'Duplicate Admin',
            'password' : 'admin',
            }
        resp = self.app.post_json(self.api_url, user,
                                  status=HTTPBadRequest.code)

    def test_add_user_with_new_role(self):
        data = {
            'userid' : 'mary',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'mary@mary.org',
            'password' : 'admin',
            'role_id' : 'manager',
            }
        resp = self.app.post_json(
            self.api_url, data, status=HTTPOk.code)
        resp = self.app.get(
            self.api_url + '/mary.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(data['role_id'], user['role_id'])

    def test_add_user_with_empty_role(self):
        data = {
            'userid' : 'mary',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'mary@mary.org',
            'password' : 'admin',
            'role_id' : '',
            }
        resp = self.app.post_json(
            self.api_url, data, status=HTTPOk.code)
        resp = self.app.get(
            self.api_url + '/mary.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['role_id'], data['role_id'])

    def test_add_user_with_bad_role(self):
        data = {
            'userid' : 'mary',
            'real_name' : 'Duplicate Admin',
            'email_id' : 'mary@mary.org',
            'password' : 'admin',
            'role_id' : 'manager2',
            }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_update_user_no_password(self):
        user = {
            'userid' : 'admin',
            'real_name' : 'New Admin',
            'email_id' : 'new@new.org'
            }
        resp = self.app.put_json(self.api_url + '/admin.json', user,
                                 status=HTTPOk.code)
        upd_user = resp.json['result']
        self.assertEqual(user['real_name'], upd_user['real_name'])
        self.assertEqual(user['email_id'], upd_user['email_id'])

        resp = self.app.get(self.api_url + '/admin.json',
                            status=HTTPOk.code)
        upd_user = resp.json['result']
        self.assertEqual(user['real_name'], upd_user['real_name'])
        self.assertEqual(user['email_id'], upd_user['email_id'])

    def test_update_user_with_password(self):
        user = {
            'userid' : 'admin',
            'real_name' : 'New Admin',
            'email_id' : 'new@new.org',
            'password' : 'nopass',
            }
        resp = self.app.put_json(self.api_url + '/admin.json', user,
                                 status=HTTPOk.code)
        upd_user = resp.json['result']
        self.assertEqual(user['real_name'], upd_user['real_name'])
        self.assertEqual(user['email_id'], upd_user['email_id'])
        self.assertTrue('password' not in upd_user)

        resp = self.app.get(self.api_url + '/admin.json',
                            status=HTTPOk.code)
        upd_user = resp.json['result']
        self.assertEqual(user['real_name'], upd_user['real_name'])
        self.assertEqual(user['email_id'], upd_user['email_id'])
        self.assertTrue('password' not in upd_user)

    def test_update_user_with_invalid_url(self):
        user = {
            'userid' : 'admin',
            'real_name' : 'New Admin',
            'email_id' : 'new@new.org',
            'password' : 'nopass',
            }
        resp = self.app.put_json(self.api_url + '/invalid.json', user,
                                 status=HTTPBadRequest.code)

    def test_update_admin_user_role_with_no_change(self):
        data = {
            'userid' : 'admin',
            'role_id': 'manager'
        }
        self.app.put_json(
            self.api_url + "/admin.json", data, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/admin.json', status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual(user['role_id'], '')

    def test_update_user_with_no_userid(self):
        user = {
            'real_name' : 'New Admin',
            }
        resp = self.app.put_json(self.api_url + '/admin.json', user,
                                 status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['real_name'], user['real_name'])

    def test_update_user_case_insensitive_userid(self):
        user = {
            'userid' : 'AdmiN',
            'real_name' : 'New Admin',
            }
        resp = self.app.put_json(self.api_url + '/ADMIN.json', user,
                                 status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['real_name'], user['real_name'])

    def test_update_user_with_non_unique_email_id(self):
        user = {
            'userid' : 'john',
            'email_id' : 'admin@admin.org',
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPBadRequest.code)

    def test_update_user_partially_no_change(self):
        user = {
            'userid' : 'john',
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)

    def test_update_user_partially_real_name(self):
        user = {
            'userid' : 'john',
            'real_name' : 'Joker',
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/john.json',
                            status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual('Joker', user['real_name'])

    def test_update_user_partially_email_id(self):
        user = {
            'userid' : 'john',
            'email_id' : 'new.mail@mail.org',
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/john.json',
                            status=HTTPOk.code)
        user = resp.json['result']
        self.assertEqual('new.mail@mail.org', user['email_id'])

    def test_update_user_partially_password(self):
        user = {
            'userid' : 'john',
            'password' : 'newpass',
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)

    def test_update_user_partially_state(self):
        user = {
            'userid' : 'john',
            'active' : False,
            }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)

    def test_delete(self):
        self.app.delete(
            self.api_url + '/john.json', status=HTTPOk.code)
        self.app.get(
            self.api_url + '/john.json', status=HTTPNotFound.code)

    def test_delete_case_insensitive_userid(self):
        self.app.delete(
            self.api_url + '/JoHn.json', status=HTTPOk.code)
        self.app.get(
            self.api_url + '/john.json', status=HTTPNotFound.code)

    def test_delete_non_existant_user(self):
        self.app.delete(
            self.api_url + '/invalid.json', status=HTTPBadRequest.code)

    def test_delete_admin_user(self):
        self.app.delete(
            self.api_url + "/admin.json", status=HTTPBadRequest.code)

    def test_delete_self_user(self):
        self.app.delete(
            self.api_url + '/admin.json', status=HTTPBadRequest.code)


class UserAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/users'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_deny_add_user(self):
        user = {
            "userid" : "john1",
            "real_name" : "Doe John",
            "email_id" : "john1@doe.org",
         }
        self.app.post_json(self.api_url, user, status=HTTPForbidden.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/john.json', status=HTTPOk.code)

    def test_update_user_deny_all(self):
        user = {
            "userid" : "john",
            "real_name" : "Doe John",
            "email_id" : "john@doe.org",
         }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/john.json', status=HTTPForbidden.code)


class UserAPIAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/users'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin',])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_update_user_allow_self_modify_other(self):
        user = {
            "userid" : "john",
            "real_name" : "Doe John",
            "email_id" : "john@doe.org",
         }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)

    def test_update_user_allow_role_modification(self):
        user = {
            "userid" : "john",
            "real_name" : "Doe John",
            "email_id" : "john@doe.org",
            "role_id" : "manager",
         }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPOk.code)

    def test_delete_user_allow_other(self):
        self.app.delete(
            self.api_url + '/john.json', status=HTTPOk.code)


class UserAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/users'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_add_user(self):
        user = {
            "userid" : "john1",
            "real_name" : "Doe John",
            "email_id" : "john1@doe.org",
         }
        self.app.post_json(self.api_url, user, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url + '/john.json', status=HTTPForbidden.code)

    def test_update_user_deny_all(self):
        user = {
            "userid" : "john",
            "real_name" : "Doe John",
            "email_id" : "john@doe.org",
         }
        resp = self.app.put_json(
            self.api_url + '/john.json', user, status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/john.json', status=HTTPForbidden.code)
