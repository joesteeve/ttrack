angular.module('ttrack.projects.controllers.activities.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.projects.directives'
]).controller(
  'activityAddCtrl',
  ['$scope', 'ActivityCollection', '$state', 'apiErrorDialog', '$stateParams',
   function ($scope, ActivityCollection, $state, errorDlg, $stateParams) {
     // Setting the project_id in scope has double usefulness. It
     // will be used in templates to contruct other urls and in the
     // directives that are used in the template
     $scope.project_id = $stateParams.projectId;

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.activityAddForm.$valid) {
         return;
       }
       ActivityCollection.save(
         {projectId: $scope.project_id},
         $scope.activity,
         function (data) {
           $state.go("manage.activities.list", {projectId: $scope.project_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return $scope.activityAddForm.$dirty && $scope.activityAddForm.$valid;
     };
   }
  ]);
