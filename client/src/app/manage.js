angular.module('ttrack.manage', [
  'ui.router',
  'ttrack.users.manage',
  'ttrack.roles.manage',
  'ttrack.clients.manage',
  'ttrack.projects.manage',
  'ttrack.tags.manage',
  'ttrack.providers'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('manage', {
          parent: 'two-part-page',
          url: '/manage',
          views: {
            "sidebar": {
              controller: 'managePageCtrl',
              templateUrl: 'managePageLinks.tpl.html'
            },
            "": {
              controller: 'managePageCtrl',
              // Switching state from template is an hack. Even though
              // the 'managePageCtrl' specified in 'sidebar' view
              // should switch the state automatically but I found
              // that this doesn't happen consistently so this
              // template gets rendered at times (which should not
              // happen). So, I forcefully call the switchState
              // function from the template code.
              template: "<div data-ui-view>Main manage {{ switchState() }}</div>"
            }
          }
        });

    }])

  .controller(
    "managePageCtrl",
    ['$scope', '$state', 'userInfo', function($scope, $state, userInfo) {

      $scope.links = [];

      if (userInfo.hasUserManagePermission()) {
        $scope.links.push({
          name: "Users",
          bref: 'manage.users', // base abstract state
          sref: "manage.users.list" // default state
        });
      }

      if (userInfo.hasRoleManagePermission()) {
        $scope.links.push({
          name: "Roles",
          bref: 'manage.roles',
          sref: "manage.roles.list"
        });
      }

      if (userInfo.hasClientManagePermission()) {
        $scope.links.push({
          name: "Clients",
          bref: 'manage.clients',
          sref: "manage.clients.list"
        });
      }

      if (userInfo.hasProjectManagePermission()) {
        $scope.links.push({
          name: "Projects",
          bref: "manage.projects",
          sref: "manage.projects.list"
        });
       }

      if (userInfo.hasTagManagePermission()) {
        $scope.links.push({
          name: "Tags",
          bref: "manage.tags",
          sref: "manage.tags.list"
        });
      }

      // We don't specify the 'manage' state as abstract but rather
      // redirect the user when he lands on the '/manage' link to
      // '/manage/users' from here. This is done so that he/she may be
      // redirected to appropriate pages based on permissions
      console.log("state : " + $state + " is manage? : " + $state.is("manage"));
      $scope.switchState = function () {
        if ($state.is("manage")) {
          if (userInfo.hasUserManagePermission()) {
            $state.go("manage.users.list");
          } else if (userInfo.hasRoleManagePermission()) {
            $state.go("manage.roles.list");
          } else if (userInfo.hasClientManagePermission()) {
            $state.go("manage.clients.list");
          } else if (userInfo.hasProjectManagePermission()) {
            $state.go("manage.projects.list");
          } else {
            $state.go("manage.tags.list");
          }
        }
      };
      $scope.switchState();

    }]);
