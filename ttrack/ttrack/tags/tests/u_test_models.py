# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
import datetime
from mock import (
    Mock,
    MagicMock,
)

import zope.component.testing as zc_testing
from ttrack.tags import (
    Tag,
    TagContainer,
    )
from ttrack import errors


class TestTagContainer(unittest.TestCase):

    def setUp(self):
        # cleanup the registry in order to stop the propagation of
        # zope events
        zc_testing.setUp()
        self.tag_container = TagContainer()
        self.tag_container.add(Tag("Hold"))

    def tearDown(self):
        zc_testing.tearDown()

    def test_add(self):
        tag = Tag("hello")
        self.tag_container.add(tag)
        self.assertIn("hello", self.tag_container)

    def test_add_throw_on_duplicate(self):
        tag = Tag("hold")
        self.assertRaises(KeyError, self.tag_container.add, tag)

    def test_add_does_case_conversion(self):
        tag = Tag("HellO")
        self.tag_container.add(tag)
        self.assertIn("hello", self.tag_container)

    def test_remove(self):
        self.tag_container.remove("hold")
        self.assertNotIn("hold", self.tag_container)

    def test_remove_throw_on_nonexistant(self):
        self.assertRaises(
            KeyError, self.tag_container.remove, "hello")

    def test_remove_does_case_conversion(self):
        self.tag_container.remove("hOlD")
        self.assertNotIn("hold", self.tag_container)

    def test_get_does_case_conversion(self):
        tag = self.tag_container.get("hOlD", None)
        self.assertNotEqual(tag, None)

    def test_exists_does_case_conversion(self):
        self.assertTrue(self.tag_container.exists("hOlD"))
