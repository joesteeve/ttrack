# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

TE_DATA = (
    {
        'start_datetime' : datetime.datetime(2014, 1, 1, 9, 0, 0),
        'end_datetime' : datetime.datetime(2014, 1, 1, 10, 0, 0),
        'userid' : 'john',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 1, 10, 30, 0),
        'end_datetime' : datetime.datetime(2014, 1, 2, 11, 30, 0),
        'userid' : 'john',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 2, 12, 0, 0),
        'end_datetime' : datetime.datetime(2014, 1, 3, 10, 0, 0),
        'userid' : 'john',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 3, 8, 30, 0),
        'end_datetime' : datetime.datetime(2014, 1, 3, 18, 30, 0),
        'userid' : 'john',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 1, 9, 0, 0),
        'end_datetime' : datetime.datetime(2014, 1, 2, 10, 0, 0),
        'userid' : 'admin',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 2, 10, 0, 0),
        'end_datetime' : datetime.datetime(2014, 1, 2, 12, 30, 0),
        'userid' : 'admin',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 2, 14, 0, 0),
        'end_datetime' : datetime.datetime(2014, 1, 4, 10, 0, 0),
        'userid' : 'admin',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },

    {
        'start_datetime' : datetime.datetime(2014, 1, 3, 8, 30, 0),
        'end_datetime' : datetime.datetime(2014, 1, 3, 18, 30, 0),
        'userid' : 'admin',
        'project_id' : 'cfactory',
        'activity' : 'Environmental analysis',
        'tags' : ('research', )
    },
)
