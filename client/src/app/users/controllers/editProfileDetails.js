angular.module('ttrack.users.controllers.profile.edit', [
  'ui.router',
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'editProfileDetails',
  ['$scope', 'UserProfile', 'apiErrorDialog',
   function ($scope, UserProfile, errorDlg) {
     $scope.alert = { show: false,
                      type: '',
                      msg: ''};

     $scope.closeAlert = function () {
       $scope.alert.show = false;
       $scope.alert.type = '';
       $scope.alert.msg = '';
     };

     function update () {
       UserProfile.get(
         function(data) {
           $scope.user = data.result;
         },
         function(err_res) {
           console.log("Unable to get user profile");
         });
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.editProfileForm.$dirty && $scope.editProfileForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.editProfileForm.$valid) {
         return;
       }
       UserProfile.save(
         $scope.user,
         function (data) {
           $scope.alert.show = true;
           $scope.alert.type = 'success';
           $scope.alert.msg = 'User profile saved sucessfully.';
           $scope.editProfileForm.$setPristine(true);
           update();
         },
         function (err_res) {
           $scope.alert.show = true;
           $scope.alert.type = 'danger';
           $scope.alert.msg = "Unable to save user profile";
           errorDlg.show(err_res);
         });
     };
     update();
   }
  ]);
