# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import os

from pyramid.view import (
    view_config,
    forbidden_view_config,
    )
from pyramid.security import (
    Authenticated,
    NO_PERMISSION_REQUIRED,
    authenticated_userid,
    remember,
    forget,
    )
from pyramid.httpexceptions import (
    HTTPForbidden,
    HTTPFound,
    )
from pyramid.renderers import render_to_response

import users

PERMISSIONS = {
    'Manage users' : ('users.manage',),
    'Manage clients' : ('clients.manage',),
    'Manage projects' : ('projects.manage',"activities.manage",
                         "activities.add", "members.manage"),
    'Manage tags' : ('tags.manage',),
    'Manage roles' : ('roles.manage',),
    'Manage all timelogs in all projects' : ('timesheet.manage',
                                             'timesheet.manage.others',
                                             'timesheet.view'),
}

AUTH_PERMISSIONS = ("view", "users.view", "clients.view", "projects.view",
                    "tags.view", "tags.add", "roles.view", "reports.view")

class AuthViews(object):
    def __init__(self, request):
        self.request = request

    @forbidden_view_config()
    def forbidden_view(self):
        # do not allow a user to login if they are already logged in
        if authenticated_userid(self.request):
            return HTTPForbidden()

        loc = self.request.route_url(
            'login', _query=(('next', self.request.path),))
        return HTTPFound(location=loc)

    @view_config(
        route_name='login',
        permission=NO_PERMISSION_REQUIRED,
        )
    def login_view(self):
        nxt = self.request.params.get('next') or self.request.route_url('home')
        login = ''
        did_fail = False
        if 'submit' in self.request.POST:
            login = self.request.POST.get('login', '')
            passwd = self.request.POST.get('passwd', '')
            user = users.find_user(self.request.root, login)
            if user and user.check_password(passwd):
                headers = remember(self.request, login)
                return HTTPFound(location=nxt, headers=headers)
            did_fail = True
        settings = self.request.registry.settings
        pub_dir = settings['ttrack.publish_folder']
        data = {
            'login': login,
            'next': nxt,
            'failed_attempt': did_fail,
            }
        return render_to_response(os.path.join(pub_dir, "login.pt"),
                                  data, request=self.request)

    @view_config(
        route_name='logout',
        )
    def logout_view(self):
        headers = forget(self.request)
        loc = self.request.route_url('login')
        return HTTPFound(location=loc, headers=headers)
