# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import (
    authenticated_userid,
    has_permission
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )
import colander

from ttrack.roles import validators as role_validators
from .models import User
from . import validators

class AddUserSchema(colander.MappingSchema):
    userid = colander.SchemaNode(
        colander.String(), location="body", type="str")
    real_name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    email_id = colander.SchemaNode(
        colander.String(), location="body", type="str",
        validator=colander.Email())
    password = colander.SchemaNode(
        colander.String(), location="body", type="str")
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class PutUserSchema(colander.MappingSchema):
    real_name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    email_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop,
        validator=colander.Email())
    password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    active = colander.SchemaNode(
        colander.Boolean(), location="body", type="bool", missing=colander.drop)
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class EditProfileSchema(colander.MappingSchema):
    real_name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    email_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop,
        validator=colander.Email())
    old_password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    new_password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class UserCollectionGetSchema(colander.MappingSchema):
    email_id = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)

    def cornice_unknowns_options(self):
        return {
            'querystring' : 'error',
            }

@cornice_resource(
    name="api.users",
    collection_path="/api/v1/users",
    path="/api/v1/users/{userid}.json",
    permission="users.manage",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.users = request.root['users']
        self.roles = request.root['roles']
        self.current_user = self.users[authenticated_userid(request)]

    # Helper methods
    def is_userid_unique(self, request):
        userid = request.json['userid']
        # we should check only when there is a 'userid'. missing
        # fields are handled by schema validation.
        if userid and not self.users.is_userid_unique(userid):
            self.request.errors.add('body', 'userid', 'User already exists')
            self.request.errors.status = HTTPBadRequest.code

    def is_email_unique(self, request):
        email = request.json.get('email_id', None)
        if email and not self.users.is_email_unique(email):
            self.request.errors.add('body', 'email_id', 'Email-id already used')
            self.request.errors.status = HTTPBadRequest.code

    # Views
    @cornice_view(schema=UserCollectionGetSchema,
                  permission="users.view")
    def collection_get(self):
        qs_has = self.request.params.has_key
        data = {
            "type" : None,
            "result" : None,
            }

        if qs_has('email_id'):
            email_id = self.request.params.get('email_id')
            user = self.users.find_with_email_id(email_id)
            if user:
                data['type'] = 'user'
                data['result'] = user.to_dict()
                return data
            else:
                self.request.errors.add(
                    'querystring', 'email_id', 'No user found')
                self.request.errors.status = HTTPNotFound.code
                return

        user_list = []
        for userid in self.users:
            user_list.append(self.users[userid].to_dict(exclude_projects=True))
        data['type'] = 'user_list'
        data['result'] = user_list
        return data

    @cornice_view(schema=AddUserSchema,
                  validators=('is_userid_unique', 'is_email_unique',
                              role_validators.body_role_id_exists))
    def collection_post(self):
        user = User.from_dict(self.request.json)
        role_id = self.request.json.get('role_id', None)
        if role_id:
            user.role = self.roles.get(role_id, None)
        self.users.add(user)
        data = {
            'type' : 'user',
            'result' : user.to_dict(exclude_projects=True)
            }
        return data

    @cornice_view(validators=(validators.url_userid_exists,),
                  permission="users.view")
    def get(self):
        userid = self.request.matchdict['userid']
        user = self.users.get(userid)
        data = {
            'type' : "user",
            'result' : user.to_dict(exclude_projects=True),
            }
        return data

    @cornice_view(schema=PutUserSchema,
                  validators=(validators.strict_url_userid_exists,
                              role_validators.body_role_id_exists))
    def put(self):
        userid = self.request.matchdict['userid']
        user = self.users.get(userid)
        updated_user = self.request.json

        # if the email-id has changed, we should ensure that the new
        # email-id is unique.
        if 'email_id' in updated_user:
            if updated_user['email_id'] != user.email_id:
                dup_user = self.users.find_with_email_id(
                    updated_user['email_id'])
                if dup_user and dup_user.userid != user.userid:
                    self.request.errors.add(
                        "body", "email_id = %s is already in use" % \
                        (user.email_id))
                    self.request.errors.status = HTTPBadRequest.code
                    return

        if "real_name" in updated_user:
            user.real_name = updated_user["real_name"]
        if "email_id" in updated_user:
            user.email_id = updated_user["email_id"]
        if "password" in updated_user:
            user.password = updated_user.pop("password")

        # Role cannot be assigned to 'admin' user
        if "role_id" in updated_user and not user.is_admin():
            user.role = self.roles.get(updated_user['role_id'], None)
        if "active" in updated_user:
            user.active = updated_user["active"]

        data = {
            'type' : 'user',
            'result' : user.to_dict(exclude_projects=True),
            }
        return data

    @cornice_view(validators=(validators.strict_url_userid_exists,))
    def delete(self):
        route_userid = self.request.matchdict['userid']

        if route_userid == "admin":
            self.request.errors.add(
                'body', 'userid', 'Cannot delete system admin')
            self.request.errors.status = HTTPBadRequest.code
            return

        # ensure cannot delete self
        if route_userid == self.current_user.userid:
            self.request.errors.add('body', 'userid', 'Cannot delete self')
            self.request.errors.status = HTTPBadRequest.code
            return
        self.users.remove(route_userid)


@cornice_resource(
    name="api.account",
    path="/api/v1/account",
    permission="users.view",
)
class AccountAPI(object):

    def __init__(self, request):
        self.request = request
        self.users = request.root['users']
        self.current_user = self.users[authenticated_userid(request)]

    @cornice_view(schema=EditProfileSchema)
    def put(self):
        user = self.current_user
        # Check context sensitive permission
        if not has_permission('account.modify', user, self.request):
            self.request.errors.add(
                'url', 'userid', "User='%s' has no permission to modify" %
                (user.userid,))
            self.request.errors.status = HTTPForbidden.code
            return

        updated_user = self.request.json

        # if the email-id has changed, we should ensure that the new
        # email-id is unique.
        if 'email_id' in updated_user:
            if updated_user['email_id'] != user.email_id:
                dup_user = self.users.find_with_email_id(
                    updated_user['email_id'])
                if dup_user and dup_user.userid != user.userid:
                    self.request.errors.add(
                        "body", "email_id = %s is already in use" % \
                        (user.email_id))
                    self.request.errors.status = HTTPBadRequest.code
                    return
                user.email_id = updated_user['email_id']

        if 'old_password' in updated_user:
            if 'new_password' not in updated_user:
                self.request.errors.add(
                    'body', 'new_password', "New password not supplied")
                self.request.errors.status = HTTPBadRequest.code
                return
            if user.password != updated_user['old_password']:
                self.request.errors.add(
                    'body', 'old_password', "Old password is incorrect")
                self.request.errors.status = HTTPBadRequest.code
                return

        if 'new_password' in updated_user:
            if 'old_password' not in updated_user:
                self.request.errors.add(
                    'body', 'old_password', "Old password not supplied")
                self.request.errors.status = HTTPBadRequest.code
                return
            user.password = updated_user['new_password']

        if "real_name" in updated_user:
            user.real_name = updated_user["real_name"]

        data = {
            'type' : 'user',
            'result' : user.to_dict(exclude_projects=True),
            }
        return data

    def get(self):
        data = {
            'type' : 'user',
            'result' : self.current_user.to_dict(),
        }
        return data
