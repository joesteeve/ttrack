# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from ttrack import errors

def _ensure_good_dates(start_date, end_date):
    if start_date and end_date:
        if start_date > end_date:
            raise TypeError("bad date range. start=%s, end=%s" % \
                            (start_date, end_date))

def _start_end_datetimes(start_date, end_date):
    start_dtime = None
    end_dtime = None
    if start_date is not None:
        start_dtime = datetime.datetime.combine(
            start_date, datetime.time(0, 0))
    if end_date is not None:
        end_dtime = datetime.datetime.combine(
            end_date, datetime.time(23, 59, 59))
    return (start_dtime, end_dtime)


def generate_project_report(project_timesheet, time_key, start_date, end_date):
    _ensure_good_dates(start_date, end_date)

    if start_date or end_date:
        start_dtime, end_dtime = _start_end_datetimes(start_date, end_date)
        return project_timesheet.get_statistics_for_range(
            start_dtime, end_dtime)
    else:
        return project_timesheet.get_statistics_for_time_key(time_key)


def generate_user_report(user_timesheet, time_key, start_date, end_date):
    _ensure_good_dates(start_date, end_date)

    if start_date or end_date:
        start_dtime, end_dtime = _start_end_datetimes(start_date, end_date)
        return user_timesheet.get_statistics_for_range(
            start_dtime, end_dtime)
    else:
        return user_timesheet.get_statistics_for_time_key(time_key)


def generate_client_report(client, timesheet, time_key, start_date, end_date):
    _ensure_good_dates(start_date, end_date)

    client_timesheet = timesheet.get_client_timesheet(client)
    report = None
    p_stat = {}
    if start_date or end_date:
        start_dtime, end_dtime = _start_end_datetimes(start_date, end_date)
        report = client_timesheet.get_statistics_for_range(
            start_dtime, end_dtime)
        for project in client.projects:
            ts = timesheet.get_project_timesheet(project)
            tl = ts.get_all()
            p_stat[project.project_id] = tl.get_total_time_for_range_mins(
                start_dtime, end_dtime)
    else:
        report = client_timesheet.get_statistics_for_time_key(time_key)
        for project in client.projects:
            ts = timesheet.get_project_timesheet(project)
            tl = ts.get_all()
            p_stat[project.project_id] = tl.get_total_time_mins(key=time_key)
    report['projects'] = p_stat
    return report

def generate_tag_report(tag, timesheet, time_key, start_date, end_date):
    _ensure_good_dates(start_date, end_date)

    tag_timesheet = timesheet.get_tag_timesheet(tag)
    report = None
    p_stat = {}
    if start_date or end_date:
        start_dtime, end_dtime = _start_end_datetimes(start_date, end_date)
        return tag_timesheet.get_statistics_for_range(
            start_dtime, end_dtime)
    else:
        return tag_timesheet.get_statistics_for_time_key(time_key)
