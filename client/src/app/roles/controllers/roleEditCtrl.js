angular.module('ttrack.roles.controllers.edit', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'roleEditCtrl',
  ['$scope', 'Role', '$stateParams', '$state', 'apiErrorDialog',
   function ($scope, Role, $stateParams, $state, errorDlg) {
     var role_id = $stateParams.roleId;

     $scope.permissions = [
       { name : 'Manage users', isSelected : false},
       { name : 'Manage clients', isSelected : false},
       { name : 'Manage projects', isSelected : false},
       { name : 'Manage tags', isSelected : false},
       { name : 'Manage roles', isSelected : false},
       { name : 'Manage all timelogs in all projects', isSelected : false}
     ];

     Role.get(
       {id: role_id},
       function(data) {
         $scope.role = data.result;
         for (var index in data.result.permissions) {
           var perm = data.result.permissions[index];
           for (var i in $scope.permissions) {
             var lperm = $scope.permissions[i];
             if (lperm.name == perm) {
               lperm.isSelected = true;
             }
           }
         }
       },
       function(err_res) {
         console.log("Invalid role_id : " + role_id);
         $state.go("manage.roles.list");
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       if ($scope.roleEditForm.$dirty && $scope.roleEditForm.$valid) {
         for (var index in $scope.permissions) {
           var perm = $scope.permissions[index];
           if (perm.isSelected) {
             return true;
           }
         }
       }
       return false;
     };

     $scope.save = function () {
       if (!$scope.roleEditForm.$valid) {
         return;
       }
       $scope.role.permissions = [];
       for (var index in $scope.permissions) {
         var perm = $scope.permissions[index];
         if (perm.isSelected) {
           $scope.role.permissions.push(perm.name);
         }
       }
       Role.save(
         {id: role_id},
         $scope.role,
         function (data) {
           $state.go("manage.roles.show", {roleId: role_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);
