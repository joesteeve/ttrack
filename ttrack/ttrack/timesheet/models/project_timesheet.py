# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from persistent import Persistent
from persistent.mapping import PersistentMapping

from ttrack import errors

from .timelog import (
    TimeLog,
)

from .utils import (
    get_or_create_timelog,
    cleanup_timeentry,
)


class ProjectTimeSheet(Persistent):
    """Per project time-sheet"""

    def __init__(self):
        super(ProjectTimeSheet, self).__init__()
        self.__all = TimeLog()
        self.__users = PersistentMapping()
        self.__activities = PersistentMapping()
        self.__tags = PersistentMapping()

    def add(self, timeentry):
        self.__all.add(timeentry)
        # add to project.user timelog
        tl = get_or_create_timelog(self.__users, timeentry.user)
        tl.add(timeentry)
        # add to project.activity timelog
        tl = get_or_create_timelog(self.__activities, timeentry.activity)
        tl.add(timeentry)
        # add to project.tag timelog
        for tag in timeentry.tags:
            tl = get_or_create_timelog(self.__tags, tag)
            tl.add(timeentry)

    def remove(self, timeentry):
        self.__all.remove(timeentry)
        cleanup_timeentry(self.__users, timeentry.user, timeentry)
        cleanup_timeentry(self.__activities, timeentry.activity, timeentry)
        for tag in timeentry.tags:
            cleanup_timeentry(self.__tags, tag, timeentry)

    def remove_tag(self, tag):
        if tag in self.__tags:
            del(self.__tags[tag])

    def get_all(self):
        return self.__all

    def get_user(self, user):
        return self.__users.get(user, TimeLog())

    def get_activity(self, activity):
        return self.__activities.get(activity, TimeLog())

    def get_tag(self, tag):
        return self.__tags.get(tag, TimeLog())

    def get_statistics_for_time_key(self, time_key):
        u_stats = {}
        for user, timelog in self.__users.items():
            u_stats[user.userid] = timelog.get_total_time_mins(key=time_key)
        a_stats = {}
        for activity, timelog in self.__activities.items():
            a_stats[activity.name] = timelog.get_total_time_mins(key=time_key)
        t_stats = {}
        for tag, timelog in self.__tags.items():
            t_stats[tag.name] = timelog.get_total_time_mins(key=time_key)
        return {
            'users' : u_stats,
            'activities' : a_stats,
            'tags' : t_stats,
            'total' : self.__all.get_total_time_mins(key=time_key),
        }

    def get_statistics_for_range(self, start_datetime, end_datetime):
        users = {}
        activities = {}
        tags = {}
        total = 0

        for entry in self.__all.get_ite_entries(start_datetime, end_datetime):
            mins = entry.effective_time_mins()
            # per-user time-spent
            if entry.user.userid not in users:
                users[entry.user.userid] = 0
            users[entry.user.userid] += mins
            # per-activity time-spent
            if entry.activity.name not in activities:
                activities[entry.activity.name] = 0
            activities[entry.activity.name] += mins
            # per-tag time-spent
            for tag in entry.tags:
                if tag.name not in tags:
                    tags[tag.name] = 0
                tags[tag.name] += mins
            # total time-spent
            total += mins

        return {
            'users' : users,
            'activities' : activities,
            'tags' : tags,
            'total' : total,
        }


class ActivityTimeSheet(Persistent):
    """Per activity time-sheet"""

    def __init__(self):
        super(ActivityTimeSheet, self).__init__()
        self.__all = TimeLog()
        self.__users = PersistentMapping()
        self.__tags = PersistentMapping()

    def add(self, timeentry):
        self.__all.add(timeentry)
        # add to activity.user timelog
        tl = get_or_create_timelog(self.__users, timeentry.user)
        tl.add(timeentry)
        # add to activity.tag timelog
        for tag in timeentry.tags:
            tl = get_or_create_timelog(self.__tags, tag)
            tl.add(timeentry)

    def remove(self, timeentry):
        self.__all.remove(timeentry)
        cleanup_timeentry(self.__users, timeentry.user, timeentry)
        for tag in timeentry.tags:
            cleanup_timeentry(self.__tags, tag, timeentry)

    def remove_tag(self, tag):
        if tag in self.__tags:
            del(self.__tags[tag])

    def get_all(self):
        return self.__all

    def get_user(self, user):
        return self.__users.get(user, TimeLog())

    def get_tag(self, tag):
        return self.__tags.get(tag, TimeLog())
