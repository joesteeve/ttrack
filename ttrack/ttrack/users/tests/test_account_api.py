# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.users import api
from ttrack.users import models
from ttrack.roles import Role, RoleContainer

ROLE_DATA = (
    {
        'role_id' : 'admin',
        'name' : 'Administrator',
        'permissions' : ['Manage users', 'Manage clients', 'Manage projects',
                         'Manage tags', 'Manage roles'],
        'system' : True,
    },
    {
        'role_id' : 'user',
        'name' : 'Normal User',
        'permissions' : [],
        'system' : True,
    },
    {
        'role_id' : 'manager',
        'name' : 'Project Manager',
        'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                         'Manage roles',],
        'system' : False
    },
)

USER_DATA = (
    {"userid" : "admin",
     "real_name" : "Administrator",
     "email_id" : "admin@admin.org",
     "password" : "pass",
     "role_id" : "admin",
     "active" : True,},

    {"userid" : "john",
     "real_name" : "John Doe",
     "email_id" : "john@doe.org",
     "password" : "john",
     "role_id" : "user",
     "active" : True,},

    {"userid" : "jane",
     "real_name" : "Jane Smith",
     "email_id" : "jane@smith.org",
     "password" : "jane",
     "role_id" : "manager",
     "active" : False},

    )

def user_in_list(data, user_list):
    for user in user_list:
        if data['userid'] == user['userid']:
            return True
    return False

class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, ("users.view", "account.modify")),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(role_data, user_data):
    root = TestRoot()
    root['roles'] = RoleContainer()
    for r in role_data:
        role = Role.from_dict(r)
        role.system = r['system']
        root['roles'].add(role)

    root['users'] = models.UserContainer()
    for u in user_data:
        user = models.User.from_dict(u)
        user.role = root['roles'].get(u['role_id'])
        root['users'].add(user)

    def root_factory(request):
        return root
    return root_factory

class AccountAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/account'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_fail_duplicate_email(self):
        data = {
            'email_id' : 'john@doe.org',
        }
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_fail_missing_old_password(self):
        data = {
            'new_password' : 'newpass',
        }
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_fail_missing_new_password(self):
        data = {
            'real_name' : 'Admin new',
            'old_password' : 'pass',
        }
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_fail_incorrect_old_password(self):
        data = {
            'real_name' : 'Admin new',
            'old_password' : 'oldpass',
            'new_password' : 'newpass',
        }
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_change_name(self):
        data = {
            'real_name' : 'Admin new',
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['real_name'], data['real_name'])

    def test_change_name_drop_userid(self):
        data = {
            'real_name' : 'Admin new',
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['real_name'], data['real_name'])

    def test_change_password(self):
        data = {
            'old_password' : 'pass',
            'new_password' : 'newpass',
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_change_email_id(self):
        data = {
            'email_id' : 'newid@admin.org'
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['email_id'], data['email_id'])

    def test_change_all_fields(self):
        data = {
            'old_password' : 'pass',
            'new_password' : 'newpass',
            'email_id' : 'newid@admin.org',
            'real_name' : 'new name',
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['email_id'], data['email_id'])
        self.assertEqual(res['real_name'], data['real_name'])
        self.app.put_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_no_change_on_non_allowed_fields_1(self):
        data = {
            'active' : False
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertEqual(res['active'], True)

    def test_no_change_on_non_allowed_fields_2(self):
        data = {
            'role_id' : ["manager",]
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'admin')
        self.assertNotEqual(res['role_id'], data['role_id'])

class AccountAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/account'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(ROLE_DATA, USER_DATA))
        self.config.scan("ttrack.users.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_update_self(self):
        data = {
            'real_name' : 'New name',
        }
        resp = self.app.put_json(self.api_url, data, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'john')
        self.assertEqual(res['real_name'], data['real_name'])

    def test_allow_get(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['userid'], 'john')
