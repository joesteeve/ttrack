# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
import datetime

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.timesheet.models import TimeSheet, UITimeEntry, TimeLog
