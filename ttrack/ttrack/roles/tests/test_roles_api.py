# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

import ttrack.testing
from ttrack.models import create_app_root
from ttrack.roles.models import Role, RoleContainer

ROLE_DATA = (
    {
        'role_id' : 'g:manager',
        'name' : 'Project Manager',
        'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                         'Manage roles',],
    },
    {
        'role_id' : 'g:lead',
        'name' : 'Team Leader',
        'permissions' : ['Manage tags', 'Manage projects'],
    },
    {
        'role_id' : 'g:developer',
        'name' : 'Developer',
        'permissions' : [],
    }
)

def build_root():
    root = create_app_root()
    for r in ROLE_DATA:
        role = Role.from_dict(r)
        root['roles'].add(role)
    return root

class RoleAPITests(unittest.TestCase):

    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/roles'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.scan('ttrack.roles.api')
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        self.root = build_root()

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_get_all_roles(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        roles = resp.json['result']
        self.assertEqual(len(roles), len(ROLE_DATA))

    def test_get_existing_role(self):
        resp = self.app.get(self.api_url + "/g:manager.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], 'g:manager')

    def test_get_existing_role_case_insensitive(self):
        resp = self.app.get(self.api_url + "/G:mAnagEr.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], 'g:manager')

    def test_get_non_existant_role(self):
        self.app.get(self.api_url + '/unknown.json', status=HTTPNotFound.code)

    def test_add_fail_on_duplicate_role_id(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_add_fail_on_case_insensitive_duplicate_role_id(self):
        data = {
            'role_id' : 'G:Manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_add_fail_on_missing_name(self):
        data = {
            'role_id' : 'g:tmanager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)


    def test_add_fail_on_missing_permissions(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_add_fail_on_empty_permissions(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : [],
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_add_fail_on_invalid_permission(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', 'My own permission', ],
        }
        self.app.post_json(self.api_url, data, status=HTTPBadRequest.code)

    def test_add_new(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', ],
        }
        resp = self.app.post_json(self.api_url, data, status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], data['role_id'])

        resp = self.app.get(self.api_url + '/g:tmanager.json',
                            status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], data['role_id'])

    def test_add_new_case_insensitive_role_id(self):
        data = {
            'role_id' : 'g:TManager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', ],
        }
        resp = self.app.post_json(self.api_url, data, status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], 'g:tmanager')

        resp = self.app.get(self.api_url + '/g:tmanager.json',
                            status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(role['role_id'], 'g:tmanager')

    def test_put_fail_on_role_id_change(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_missing_name(self):
        data = {
            'role_id' : 'g:manager',
            'permissions' : ['Manage users', ],
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_missing_permissions(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_empty_permissions(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : [],
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_invalid_permissions(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', 'Invalid permission',],
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_role_id_1(self):
        data = {
            'role_id' : 'G:Manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_role_id_2(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/G:Manager.json', data,
                          status=HTTPBadRequest.code)

    def test_put_change_name(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/g:manager.json', status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual(data['name'], role['name'])

    def test_put_change_permissions(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage projects', 'Manage tags', 'Manage users',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/g:manager.json', status=HTTPOk.code)
        role = resp.json['result']
        self.assertNotIn('Manage clients', role['permissions'])
        self.assertIn('Manage users', role['permissions'])
        self.assertEqual(len(data['permissions']), len(data['permissions']))

    def test_delete(self):
        self.app.delete(self.api_url + '/g:manager.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/g:manager.json',
                     status=HTTPNotFound.code)

    def test_delete_case_insensitive(self):
        self.app.delete(self.api_url + '/G:Manager.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/g:manager.json',
                     status=HTTPNotFound.code)

    def test_delete_non_existant_role(self):
        self.app.delete(
            self.api_url + '/g:other.json', status=HTTPNotFound.code)

class RoleAPINonAdminPermissionsTests(unittest.TestCase):

    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/roles'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(userid="user", groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan('ttrack.roles.api')
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        self.root = build_root()

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_all_get(self):
        self.app.get(self.api_url + '/g:manager.json', status=HTTPOk.code)

    def test_deny_add(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', ],
        }
        self.app.post_json(self.api_url, data, status=HTTPForbidden.code)

    def test_deny_update(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/g:manager.json',
                        status=HTTPForbidden.code)

class RoleAPIAdminPermissionsTests(unittest.TestCase):

    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/roles'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(userid="admin", groupids=['admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan('ttrack.roles.api')
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        self.root = build_root()

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_all_get(self):
        self.app.get(self.api_url + '/g:manager.json', status=HTTPOk.code)

    def test_allow_add(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', ],
        }
        self.app.post_json(self.api_url, data, status=HTTPOk.code)

    def test_allow_update(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url + '/g:manager.json', status=HTTPOk.code)


class RoleAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/roles'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan('ttrack.roles.api')
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        self.root = build_root()

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url + '/g:manager.json',
                     status=HTTPForbidden.code)

    def test_deny_add(self):
        data = {
            'role_id' : 'g:tmanager',
            'name' : 'Team Manager',
            'permissions' : ['Manage users', ],
        }
        self.app.post_json(self.api_url, data, status=HTTPForbidden.code)

    def test_deny_update(self):
        data = {
            'role_id' : 'g:manager',
            'name' : 'Team Manager',
            'permissions' : ['Manage clients', 'Manage projects', 'Manage tags',
                             'Manage roles']
        }
        self.app.put_json(self.api_url + '/g:manager.json', data,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/g:manager.json',
                        status=HTTPForbidden.code)
