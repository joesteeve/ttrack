angular.module('ttrack.tags.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'tagListCtrl',
  ['$scope', 'TagCollection', 'Tag', 'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, TagCollection, Tag, confirmDlg, errorDlg) {
     function update () {
       TagCollection.get(function (data) {
         $scope.tags = data.result;
       });
     }

     $scope.deleteTag = function(tag_name) {
       var result = confirmDlg.show(
         "Delete tag?",
         "All the timelog entries made for the tag '" + tag_name + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         Tag.remove(
           {name: tag_name},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }
  ]);
