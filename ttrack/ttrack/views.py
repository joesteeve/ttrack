# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import os

from pyramid.view import view_config
from pyramid.renderers import render_to_response

from .models import AppRoot

@view_config(
    context=AppRoot,
    route_name="home",
    permission='view',
    )
def home_view(request):
    settings = request.registry.settings
    pub_dir = settings['ttrack.publish_folder']
    data = {
        'rpt_csv_user_url': request.route_url('csv_user', userid=''),
        'rpt_csv_project_url': request.route_url('csv_project', project_id=''),
        'rpt_csv_client_url': request.route_url('csv_client', client_id=''),
        'rpt_csv_tag_url': request.route_url('csv_tag', tag_name=''),
        'logout_url': request.route_url('logout'),
        'home_url': request.route_url('home'),

        # API URLs
        'api_ui_session_url': request.route_url('api.session'),
        'api_user_url': request.route_url('collection_api.users'),
        'api_client_url': request.route_url('collection_api.clients'),
        'api_project_url': request.route_url('collection_api.projects'),
        'api_user_profile_url' : request.route_url('api.account'),
        'api_tags_url': request.route_url('collection_api.tags'),
        'api_role_url': request.route_url('collection_api.roles'),
        'api_timesheet_url' : request.route_url('collection_api.timesheet'),
        'api_timesheet_split_action_url' : request.route_url(
            'api.timesheet.split'),
        'api_activity_url': request.route_url(
            'collection_api.activities', project_id=''),
        'api_members_url': request.route_url(
            'collection_api.members', project_id=''),
        'api_url_project_reports' : request.route_url(
            'api.project_reports', project_id=''),
        'api_url_project_timesheet' : request.route_url(
            'api.project_timesheet', project_id=''),
        'api_url_user_reports' : request.route_url(
            'api.user_reports', userid=''),
        'api_url_user_timesheet' : request.route_url(
            'api.user_timesheet', userid=''),
        'api_url_client_reports' : request.route_url(
            'api.client_reports', client_id=''),
        'api_url_client_timesheet' : request.route_url(
            'api.client_timesheet', client_id=''),
        'api_url_tag_reports' : request.route_url(
            'api.tag_reports', tag_name=''),
        'api_url_tag_timesheet': request.route_url(
            'api.tag_timesheet', tag_name=''),

    }
    return render_to_response(os.path.join(pub_dir, "index.pt"),
                              data, request=request)
