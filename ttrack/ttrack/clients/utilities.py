# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from ttrack import errors

def is_client_id_valid(request, client_id):
    clients = request.root['clients']
    return client_id in clients

def get_client(request, client_id):
    clients = request.root['clients']
    if client_id not in clients:
        raise errors.TTrackError("Invalid client id (%s)" % client_id)
    return "[%s] %s" % (client_id, clients[client_id].name)
