angular.module('ttrack.clients.controllers.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.clients.directives'
]).controller(
  'clientAddCtrl',
  ['$scope', 'ClientCollection', '$state', 'apiErrorDialog',
   function ($scope, ClientCollection, $state, errorDlg) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.clientAddForm.$valid) {
         return;
       }
       ClientCollection.save(
         $scope.client,
         function (data) {
           $state.go("manage.clients.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return $scope.clientAddForm.$dirty && $scope.clientAddForm.$valid;
     };

   }
  ]);
