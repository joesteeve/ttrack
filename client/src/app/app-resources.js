angular.module('ttrack.resources', [
  'ngResource'
])
// ------------------ User related API ---------------------------
  .factory(
    "UserCollection",
    ['$resource', function($resource) {
      return $resource($("#api-url-users").text(), {},
                       {
                         query : {
                           method : 'GET',
                           isArray : false
                         }
                       });
    }])
  .factory(
    "User",
    ['$resource', function($resource) {
      return $resource($("#api-url-users").text() + "/:id.json", {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "UserProfile",
    ['$resource', function($resource) {
      return $resource($("#api-url-user-profile").text(), {},
                       {
                         'save' : {
                           method : 'PUT'
                         }
                       });
    }])
// -------------------- UI Session API ----------------------------
  .factory(
    "UIUserSession",
    ['$resource', function($resource) {
      return $resource($("#api-ui-session-url").text());
    }])
// ------------------ Roles related API ---------------------------
  .factory(
    "RoleCollection",
    ['$resource', function($resource) {
      return $resource($("#api-url-roles").text());
    }])
  .factory(
    "Role",
    ['$resource', function ($resource) {
      return $resource($("#api-url-roles").text() + "/:id.json", {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
      }])
// ------------------ Project related API ---------------------------
  .factory(
    "ProjectCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-projects").text());
    }])
  .factory(
    "Project", [ "$resource", function ($resource) {
      return $resource($("#api-url-projects").text() + "/:id.json", {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "ActivityCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-activities").text() + ":projectId");
    }])
  .factory(
    "Activity",
    ['$resource', function ($resource) {
      return $resource($("#api-url-activities").text() + ":projectId" +
                       "/:activityName.json", {},
                       {
                         save : {
                           method: 'PUT'
                         }
                       });
    }])
  .factory(
    "MemberCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-members").text() + ":projectId");
    }])
  .factory(
    "Member",
    ['$resource', function ($resource) {
      return $resource($("#api-url-members").text() + ":projectId" +
                       "/:userId.json");
    }])
// ------------------ Client related API ---------------------------
  .factory(
    "ClientCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-clients").text());
    }])
  .factory(
    "Client",
    ['$resource', function ($resource) {
      return $resource($("#api-url-clients").text() + "/:id.json", {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
// ------------------ Tag related API ---------------------------
  .factory(
    "TagCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-tags").text());
    }])
  .factory(
    "Tag",
    ['$resource', function ($resource) {
      return $resource($("#api-url-tags").text() + "/:name.json", {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
      }])
// ------------------ Timesheet related API ---------------------------
  .factory(
    "TimeSheetCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-timesheet").text());
    }])
  .factory(
    "TimeSheet",
    ['$resource', function ($resource) {
      return $resource($("#api-url-timesheet").text() + "/:timeEntryId.json",
                      {},
                      {
                        save : {
                          method : 'PUT'
                        }
                      });
    }])
  .factory(
    "TimeSheetActionSplit",
    ['$resource', function ($resource) {
      return $resource($("#api-url-timesheet-split").text());
    }])
  .factory(
    "UserTimeSheetCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-user-timesheet").text()+ ":id");
    }])
  .factory(
    "ProjectTimeSheetCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-project-timesheet").text()+ ":id");
    }])
  .factory(
    "ClientTimeSheetCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-client-timesheet").text()+ ":id");
    }])
  .factory(
    "TagTimeSheetCollection",
    ['$resource', function ($resource) {
      return $resource($("#api-url-tag-timesheet").text()+ ":tagName");
    }])
// ------------------ Report related API ---------------------------
  .factory(
    "UserReport",
    ['$resource', function ($resource) {
      return $resource($("#api-url-user-reports").text() + ":id");
    }])
  .factory(
    "ProjectReport",
    ['$resource', function ($resource) {
      return $resource($("#api-url-project-reports").text() + ":id");
    }])
  .factory(
    "ClientReport",
    ['$resource', function ($resource) {
      return $resource($("#api-url-client-reports").text() + ":id");
    }])
  .factory(
    "TagReport",
    ['$resource', function ($resource) {
      return $resource($("#api-url-tag-reports").text() + ":tagName");
    }]);
