angular.module('ttrack.tags.manage', [
  'ui.router',
  'ttrack.tags.controllers.list',
  'ttrack.tags.controllers.add',
  'ttrack.tags.controllers.show',
  'ttrack.tags.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('manage.tags', {
        url: '/tags',
        abstract: true,
        template: '<ui-view/>'
      })
      // Make this state as default by making the url as '' and its
      // parent as abstract
      .state('manage.tags.list', {
        url: '',
        controller: 'tagListCtrl',
        templateUrl: 'tags/tagList.tpl.html'
      })
      .state('manage.tags.add', {
        url: '/add',
        controller: 'tagAddCtrl',
        templateUrl: 'tags/tagAdd.tpl.html'
      })
      .state('manage.tags.show', {
        url: '/show/:tagName',
        controller: 'tagShowCtrl',
        templateUrl: 'tags/tagShow.tpl.html'
      })
      .state('manage.tags.edit', {
        url: '/edit/:tagName',
        controller: 'tagEditCtrl',
        templateUrl: 'tags/tagEdit.tpl.html'
      });

    }
  ]);
