# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component

from ttrack.interfaces import IApplicationWillBeCreatedEvent
from .models import UserContainer, User

@component.adapter(IApplicationWillBeCreatedEvent)
def initialize_db(event):
    event.app['users'] = UserContainer()

def register_zca_subscribers():
    component.provideHandler(initialize_db)

register_zca_subscribers()
