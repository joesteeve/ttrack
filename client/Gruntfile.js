module.exports = function ( grunt ) {

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-html2js');

  var taskConfig = {
    pkg: grunt.file.readJSON("package.json"),

    app_files: {
      jsFiles: ['src/app/**/*.js']
    },

    meta: {
      banner:
        '/**\n' +
        ' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
        ' * <%= pkg.homepage %>\n' +
        ' *\n' +
        ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
        ' * Licensed <%= pkg.licenses.type %> <<%= pkg.licenses.url %>>\n' +
        ' */\n'
    },

    jshint: {
      all: [
        'Gruntfile.js',
        'src/app/**/*.js'
      ],
      options: {
        curly: true,
        immed: true,
        newcap: true,
        noarg: true,
        sub: true,
        boss: true,
        eqnull: true
      }
    },

    less: {
      dev: {
        src: 'src/app/app.less',
        dest: 'dist-dev/styles/app.css'
      },
      release: {
        src: 'src/app/app.less',
        dest: 'dist-prod/styles/app.css',
        options: {
          cleancss: true,
          compress: true
        }
      }
    },

    html2js: {
      dev: {
        src: ['src/app/**/*.tpl.html'],
        dest: 'dist-dev/scripts/app-templates.js',
        options: {
          base: 'src/app',
          module: 'ttrack.app.templates'
        }
      }
    },

    watch: {
      less : {
        files: 'src/app/**/*.less',
        tasks: ['less:dev']
      }
    },

    clean: [
      'dist-dev/',
      'dist-prod/'
    ],

    useminPrepare: {
      html: ['dist-dev/*.pt'],
      options: {
        dest: 'dist-prod'
      }
    },

    concat: {
      options : {
        separator: ';'
      }
    },

    uglify: {
    },

    copy: {
      dev: {
        files: [
          {
            expand: true,
            cwd: 'src',
            src: ['app/**/*.{js,html}', '*.pt',
                  'vendor/**/*.{js,css,svg,ttf,woff,eot}'],
            dest: 'dist-dev/'
          }
        ]
      },

      release: {
        files: [
          {
            expand: true,
            cwd: 'dist-dev',
            src: ['*.pt'],
            dest: 'dist-prod/'
          },
          {
            expand: true,
            cwd: 'src/vendor/bootstrap/fonts',
            src: ['*.{svg,ttf,woff,eot}'],
            dest: 'dist-prod/fonts/'
          }
        ]
      }
    },

    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 20
      },
      release: {
        files: [{
          src: [
            'dist-prod/**/*.{png,gif,jpg,svg,ttf,woff,eot,css,js}'
          ]
        }]
      }
    },

    usemin: {
      html: ['dist-prod/*.pt'],
      css: ['dist-prod/styles/*.css'],
      options: {
        assetDirs: ['dist-prod', 'dist-prod/fonts/']
      }
    }
  };
  grunt.initConfig(taskConfig);

  grunt.registerTask('development', 'Builds for development in dist-dev/', [
    'clean',
    'jshint',
    'less:dev',
    'copy:dev',
    'html2js:dev'
  ]);

  grunt.registerTask('production', 'Builds for production in dist-prod/', [
    'development',
    'less:release',
    'useminPrepare',
    'concat',
    'uglify',
    'cssmin',
    'copy:release',
    'filerev',
    'usemin'
  ]);
};
