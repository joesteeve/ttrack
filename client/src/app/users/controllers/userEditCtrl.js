angular.module('ttrack.users.controllers.edit', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.users.directives'
]).controller(
  'userEditCtrl',
  ['$scope', 'User', '$stateParams', '$state', 'apiErrorDialog',
   'RoleCollection',
   function ($scope, User, $stateParams, $state, errorDlg, RoleCollection) {
     var userId = $stateParams.userId;
     User.get(
       {id: userId},
       function(data) {
         $scope.user = data.result;
         get_roles();
       },
       function(err_res) {
         console.log("Invalid userid : " + userId);
         $state.go("manage.users.list");
       });

     function get_roles() {
       RoleCollection.get(
         function (data) {
           $scope.roles = data.result;
           var role = {
             role_id: 'sg:user',
             name: '(Normal User)',
             permissions: []
           };
           $scope.roles.push(role);
           if ($scope.user.role_id) {
             for (var i in data.result) {
               role = data.result[i];
               if (role.role_id == $scope.user.role_id) {
                 $scope.selected_role = role;
                 break;
               }
             }
           } else {
             $scope.selected_role = role;
           }
         },
         function (err_res) {
           console.log("Error occurred while getting roles : " + err_res);
         });
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return ($scope.userEditForm.$dirty && $scope.userEditForm.$valid &&
               $scope.selected_role && $scope.selected_role.name);
     };

     $scope.save = function () {
       if (!$scope.userEditForm.$valid) {
         return;
       }
       if ($scope.selected_role.role_id != 'sg:user') {
         $scope.user.role_id = $scope.selected_role.role_id;
       } else {
         $scope.user.role_id = '';
       }
       User.save(
         {id: userId}, $scope.user,
         function (data) {
           $state.go("manage.users.show", {'userId': userId});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);
