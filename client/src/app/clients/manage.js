angular.module('ttrack.clients.manage', [
  'ui.router',
  'ttrack.clients.controllers.list',
  'ttrack.clients.controllers.add',
  'ttrack.clients.controllers.show',
  'ttrack.clients.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('manage.clients', {
        url: '/clients',
        abstract: true,
        template: '<ui-view/>'
      })
      // Make this state as default by making the url as '' and its
      // parent as abstract
      .state('manage.clients.list', {
        url: '',
        controller: 'clientListCtrl',
        templateUrl: 'clients/clientList.tpl.html'
      })
      .state('manage.clients.add', {
        url: '/add',
        controller: 'clientAddCtrl',
        templateUrl: 'clients/clientAdd.tpl.html'
      })
      .state('manage.clients.show', {
        url: '/show/:clientId',
        controller: 'clientShowCtrl',
        templateUrl: 'clients/clientShow.tpl.html'
      })
      .state('manage.clients.edit', {
        url: '/edit/:clientId',
        controller: 'clientEditCtrl',
        templateUrl: 'clients/clientEdit.tpl.html'
      });

    }
  ]);
