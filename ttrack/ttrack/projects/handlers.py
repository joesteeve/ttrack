# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component
from repoze.folder.interfaces import (
    IObjectWillBeRemovedEvent,
)

from pyramid.traversal import find_root

from ttrack.interfaces import IApplicationWillBeCreatedEvent
from ttrack.clients.interfaces import IClientWillBeRemovedEvent
from .models import ProjectContainer
from .interfaces import IProjectRoleWillBeRemovedEvent

@component.adapter(IApplicationWillBeCreatedEvent)
def initialize_db(event):
    event.app['projects'] = ProjectContainer()

@component.adapter(IClientWillBeRemovedEvent)
def handle_client_remove(event):
    client = event.client
    root = find_root(client)
    projects = root['projects']
    log.debug('projects: client-remove %s', client.client_id)
    for project in client.projects:
        projects.remove(project.project_id)

@component.adapter(IProjectRoleWillBeRemovedEvent)
def handle_project_role_remove(event):
    proj = event.project
    role = event.role
    # Map all the members in the current role to None, which means
    # that they are mapped to normal member role
    proj.members.find_and_replace_roles(self, role, None)

def register_zca_subscribers():
    component.provideHandler(initialize_db)
    component.provideHandler(handle_client_remove)
    component.provideHandler(handle_project_role_remove)

register_zca_subscribers()
