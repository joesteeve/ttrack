import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid',
    'pyramid_zodbconn',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'ZODB3==3.10.5',
    'waitress',
    'cornice',
    'colander>=1.0b1',
    'repoze.folder',
    'zope.lifecycleevent',
    'pyramid_zcml',
    'pyramid_chameleon',
    'repoze.evolution',
    ]

tests_requires = [req for req in requires]
tests_requires.extend([
    'mock',
])

setup(name='ttrack',
      version='0.1',
      description='ttrack',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='HiPro IT Solutions Private Limited',
      author_email='info@hipro.co.in',
      url='',
      keywords='web pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=tests_requires,
      test_suite="ttrack",
      entry_points="""
      [paste.app_factory]
      main = ttrack:main
      [console_scripts]
      tt_evolve = ttrack.evolution:main
      """
)
