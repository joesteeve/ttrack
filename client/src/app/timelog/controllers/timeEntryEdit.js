angular.module('ttrack.timelog.controllers.edit', [
  'ui.router',
  'ui.bootstrap',
  'ngTagsInput',
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.filters'
]).controller(
  "timeEntryEditCtrl",
  ['$scope', 'TimeSheet', 'UserProfile', 'Project', 'TagCollection',
   'TimeSheetActionSplit','$stateParams', '$state', 'ttrackUtils',
   'apiErrorDialog', '$q', '$timeout',
   function ($scope, TimeSheet, UserProfile, Project, TagCollection,
             TimeSheetActionSplit, $stateParams, $state, ttrackUtils,
             errorDlg, $q, $timeout) {
     var timeentry_id = $stateParams.timeEntryId;
     var end_date_modified = false;
     TimeSheet.get(
       {timeEntryId: timeentry_id},
       function (data) {
         $scope.timeentry = data.result;
         Project.get(
           {id: $scope.timeentry.project_id},
           function (data) {
             $scope.timeentry.project = data.result;
             $scope.activities = data.result.activities;
           },
           function (err_res) {
             console.log("Invalid project_id : " +
                         $scope.timeentry.project_id);
           });
       },
       function (err_res) {
         console.log("Invalid timeentry_id : " + timeentry_id);
         $state.go("timelog.list");
       });

     $scope.times = [];
     for (var h=0; h<24; h++) {
       for (var m=0; m<60; m++) {
         var st1 = "";
         if (h < 10) {
           st1 += "0";
         }
         st1 = st1 + h + ":";
         if (m < 10) {
           st1 += "0";
         }
         st1 = st1 + m;
         $scope.times.push(st1);
       }
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.getTimeClass = function (ngsDateModelCtrl, ngsTimeModelCtrl,
                                     ngeDateModelCtrl, ngeTimeModelCtrl) {
       if ((ngsTimeModelCtrl.$invalid && ngsTimeModelCtrl.$dirty) ||
           (ngsDateModelCtrl.$invalid && ngsDateModelCtrl.$dirty) ||
           (ngeDateModelCtrl.$invalid && ngeDateModelCtrl.$dirty) ||
           (ngeTimeModelCtrl.$invalid && ngeTimeModelCtrl.$dirty) ||
           ($scope.showDateInvalidError) || ($scope.showTimeInvalidError)) {
         return "has-error";
       }
       return "";
     };

     UserProfile.get (
       function (data) {
         $scope.projects = data.result.projects;
       },
       function (err_res) {
         console.log("Error occurred while getting projects : " + err_res);
       });

     TagCollection.get (
       function (data) {
         $scope.tags = data.result;
       },
       function (err_res) {
         console.log("Error occurred while gettings tags : " + err_res);
       });

     $scope.getTag = function (search_term) {
       var query = search_term.toLowerCase();
       var result = [];
       var deferred = $q.defer();
       for (var index in $scope.tags) {
         var tag_name = $scope.tags[index].name;
         var ltag_name = tag_name.toLowerCase();
         if (ltag_name.indexOf(query) > -1) {
           result.push(tag_name);
         }
       }
       $timeout(function () {
         deferred.resolve(result);
       }, 10);
       return deferred.promise;
     };

     $scope.newTagAdded = function (tag) {
       var new_tag_name = tag.text.toLowerCase();
       var found = false;
       for (var i=0; i < ($scope.timeentry.tags.length - 1); i++) {
         var tag_name = $scope.timeentry.tags[i].text.toLowerCase();
         if (tag_name == new_tag_name) {
           found = true;
           break;
         }
       }
       if (found) {
         $scope.timeentry.tags.pop();
       }
     };

     $scope.cbTagRemoved = function (tag) {
       $scope.timeEntryEditForm.$setDirty(true);
     };

     $scope.projectSelected = function (project) {
       $scope.activities = project.activities;
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.showOk = function(ngModelCtrl) {
       return ngModelCtrl.$dirty && ngModelCtrl.$valid;
     };

     $scope.getDateString = function (date_time) {
       if (date_time) {
         var dt = new Date(date_time);
         return dt.toDateString();
       }
       return "";
     };

     $scope.getDateTimeString = function (date, time) {
       var dt = new Date(date);
       return dt.toDateString() + " " + time;
     };

     $scope.startDateChanged = function () {
       if ($scope.timeentry.start_date) {
         if (!end_date_modified) {
           $scope.timeentry.end_date = $scope.timeentry.start_date;
         }
         $(ttrackUtils.jq("timeentry.edit.form.start_time")).focus();
       }
     };

     $scope.endDateChanged = function () {
       end_date_modified = true;
       if ($scope.timeentry.end_date) {
         $(ttrackUtils.jq("timeentry.edit.form.end_time")).focus();
       }
     };

     $scope.$watchCollection(
       '[timeentry.start_date, timeentry.end_date]',
       function (newValues) {
         if (newValues[0] && newValues[1]) {
           if (newValues[0] > newValues[1]) {
             $scope.showDateInvalidError = true;
             return;
           }
         }
         $scope.showDateInvalidError = false;
       });


     $scope.$watchCollection(
       '[timeentry.start_time, timeentry.end_time, timeentry.start_date,' +
         'timeentry.end_date]',
       function(newValues) {
         if (newValues[2] == newValues[3]) {
           if (newValues[1] <= newValues[0]) {
             $scope.showTimeInvalidError = true;
             return;
           }
         }
         if (newValues[2] && newValues[3]) {
           // Checking two date values for equality leads to
           // incorrect results so we convert it to string and
           // compare
           var sdt = new Date(newValues[2]);
           var edt = new Date(newValues[3]);
           if ((sdt.toDateString() == edt.toDateString()) &&
               (newValues[1] <= newValues[0])) {
             $scope.showTimeInvalidError = true;
             return;
           }
         }
         $scope.showTimeInvalidError = false;
       });

     $scope.showDateTimeError = function () {
       if ($scope.showDateInvalidError || $scope.showTimeInvalidError) {
         return true;
       }
       return false;
     };

     $scope.showWorkingHoursCheckbox = function () {
       if ($scope.timeentry &&
           ($scope.timeentry.end_date > $scope.timeentry.start_date)) {
         return true;
       }
       return false;
     };

     $scope.timesplit = {show: false, status: ''};

     $scope.$watchCollection(
       '[showDateInvalidError, showTimeInvalidError, timeentry.start_date,' +
         'timeentry.start_time, timeentry.end_date, timeentry.end_time]',
       function(newValues) {
         if (!newValues[0] && !newValues[1] && newValues[2] &&
             newValues[3] && newValues[4] && newValues[5]) {
           $scope.getTimeSplitInfo();
         } else {
           $scope.timesplit.show = false;
         }
       }
     );

     $scope.showTimeSplitInfo = function () {
       return $scope.timesplit.show;
     };

     $scope.getTimeSplitInfo = function () {
       var local_start_date = new Date($scope.timeentry.start_date);
       var local_start_date_str = local_start_date.getFullYear() +
             "-" + ttrackUtils.pad( local_start_date.getMonth() + 1 ) +
             "-" + ttrackUtils.pad( local_start_date.getDate());
       var local_end_date = new Date($scope.timeentry.end_date);
       var local_end_date_str = local_end_date.getFullYear() +
             "-" + ttrackUtils.pad( local_end_date.getMonth() + 1 ) +
             "-" + ttrackUtils.pad( local_end_date.getDate());
       var params= {
         'exclude' : timeentry_id,
         'start_date' : local_start_date_str,
         'end_date' : local_end_date_str,
         'start_time' : $scope.timeentry.start_time,
         'end_time' : $scope.timeentry.end_time
       };
       TimeSheetActionSplit.save(
         params,
         function (data) {
           $scope.timesplit.status = data.status;
           $scope.timesplit.show = true;
           $scope.timesplit.ignore_conflicts = false;
           if (data.status == 'good') {
             $scope.timesplit.text = "Time entry " +
               local_start_date.toDateString() + " " + params.start_time +
               " - " + local_end_date.toDateString() + " " + params.end_time +
               " is valid.";
             $scope.timesplit.splits = [];
           } else if (data.status == 'error') {
             $scope.timesplit.text = "Time entry " +
               local_start_date.toDateString() + " " + params.start_time +
               " - " + local_end_date.toDateString() + " " + params.end_time +
               " conflicts with the below";
             $scope.timesplit.splits = data.result;
           } else if (data.status == 'split') {
             $scope.timesplit.text = "Time entries exists between " +
               "the given time period " + local_start_date.toDateString() +
               " " + params.start_time + " - " +
               local_end_date.toDateString() + " " + params.end_time +
               ". However, there are some empty timeslots which can be " +
               "filled by splitting the current time entry " +
               "in order to avoid duplicates";
             $scope.timesplit.splits = [];
             for (var index in data.result) {
               var te = data.result[index];
               var di = {'start_date' : te.start_date,
                         'end_date' : te.end_date,
                         'start_time' : te.start_time,
                         'end_time' : te.end_time,
                         'isSelected' : true};
               $scope.timesplit.splits.push(di);
             }
           }
           else {
             console.log ("Invalid time-split action received : " + data);
           }
         },
         function (err_res) {
           errorDlg.show(err_res);
         });

     };

     $scope.canSave = function () {
       if ($scope.timeEntryEditForm.$dirty && $scope.timeEntryEditForm.$valid &&
           !$scope.showTimeInvalidError && !$scope.showDateInvalidError) {
         if ($scope.timesplit.status == 'split') {
           if ($scope.timesplit.ignore_conflicts) {
             return true;
           }
           for (var index in $scope.timesplit.splits) {
             var sp = $scope.timesplit.splits[index];
             if (sp.isSelected) {
               return true;
             }
           }
         } else if ($scope.timesplit.status == 'error') {
           if ($scope.timesplit.ignore_conflicts) {
             return true;
           }
         }else if ($scope.timesplit.status == 'good') {
           return true;
         }
       }
       return false;
     };

     function get_timeentry_tags() {
       var tags = [];
       for (var index in $scope.timeentry.tags) {
         var tag = $scope.timeentry.tags[index];
         tags.push(tag.text);
       }
       return tags;
     }

     $scope.save = function () {
       if (!$scope.timeEntryEditForm.$valid) {
         return;
       }
       // Do the save here..
       var ti;
       var te = {
         time_splits : [],
         project_id : $scope.timeentry.project.project_id,
         tags : get_timeentry_tags(),
         description : $scope.timeentry.description,
         ignore_conflicts : $scope.timesplit.ignore_conflicts,
         working_hours_only : $scope.timeentry.working_hours_only
       };
       if ($scope.timeentry.activity.name) {
         te.activity = $scope.timeentry.activity.name;
       } else {
         te.activity = $scope.timeentry.activity;
       }
       if (($scope.timesplit.status == 'good') ||
           ($scope.timesplit.status == 'error' &&
            $scope.timesplit.ignore_conflicts) ||
           ($scope.timesplit.status == 'split' &&
            $scope.timesplit.ignore_conflicts)) {
         var local_start_date = new Date($scope.timeentry.start_date);
         var local_start_date_str = local_start_date.getFullYear() +
               "-" + ttrackUtils.pad( local_start_date.getMonth() + 1 ) +
               "-" + ttrackUtils.pad( local_start_date.getDate());
         var local_end_date = new Date($scope.timeentry.end_date);
         var local_end_date_str = local_end_date.getFullYear() +
               "-" + ttrackUtils.pad( local_end_date.getMonth() + 1 ) +
               "-" + ttrackUtils.pad( local_end_date.getDate());
         ti = { start_date : local_start_date_str,
                end_date : local_end_date_str,
                start_time : $scope.timeentry.start_time,
                end_time : $scope.timeentry.end_time
              };
         te.time_splits.push(ti);
       } else if ($scope.timesplit.status == 'split' &&
                  !$scope.timesplit.ignore_conflicts) {
         for (var index in $scope.timesplit.splits) {
           var ts = $scope.timesplit.splits[index];
           if (ts.isSelected) {
             ti = { start_date : ts.start_date,
                    end_date : ts.end_date,
                    start_time : ts.start_time,
                    end_time : ts.end_time
                  };
             te.time_splits.push(ti);
           }
         }
       }
       else {
         console.log("Invalid timeslit status : " + $scope.timesplit.status);
         return;
       }
       TimeSheet.save(
         { timeEntryId : timeentry_id },
         te,
         function (data) {
           $state.go("timelog.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);
