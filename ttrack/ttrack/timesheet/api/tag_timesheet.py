# Copyright (C) 2013 HiPro IT Solutions Private Limited. All rights
# reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import colander
from pyramid.httpexceptions import HTTPBadRequest
from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from ttrack import users, projects, tags, clients
from .common import TimeSheetAPIBase

class TagCollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1, max=1000))
    start_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    end_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    userid = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    client_id = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    project_id = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)

@cornice_resource(
    name="api.tag_timesheet",
    path="/api/v1/tag_timesheet/{tag_name}",
    permission="timesheet.view",
    )
class TagTimesheetAPI(TimeSheetAPIBase):

    @cornice_view(schema=TagCollectionGetSchema,
                  validators=(tags.validators.url_tag_name_exists,
                              users.validators.qs_userid_exists,
                              clients.validators.qs_client_id_exists,
                              projects.validators.qs_project_id_exists,
                              '_validate_pagination_params_available',
                          ))
    def get(self):
        tag = self._url_get_tag()                 # mandatory
        user = self._qs_get_user()                # optional
        client = self._qs_get_client()            # optional
        project = self._qs_get_project()          # optional

        return self._get_filtered_data(
            self.timesheet.get_tag_filtered_timelog,
            tag, user, client, project)
