angular.module('ttrack.users.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'userListCtrl',
  ['$scope', 'UserCollection', 'User', 'confirmationDialogYesNo',
   'apiErrorDialog',
   function ($scope, UserCollection, User, confirmDlg, errorDlg) {
     function update () {
       UserCollection.get(function(data) {
         $scope.users = data.result;
       });
     }

     $scope.filterActive = true;

     $scope.deleteUser = function(userid) {
       var result = confirmDlg.show(
         "Delete user?",
         "All the timelog entries made by the user '" + userid + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         User.remove(
           {id: userid},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }]);
