angular.module('ttrack.timelog.controllers.show', [
  'ngSanitize',
  'ui.router',
  'ttrack.resources'
]).controller(
    "timeEntryShowCtrl",
    ['$scope', 'TimeSheet', '$stateParams', '$state',
     function ($scope, TimeSheet, $stateParams, $state) {
       var timeentry_id = $stateParams.timeEntryId;
       TimeSheet.get(
         {timeEntryId: timeentry_id},
         function (data) {
           $scope.timeentry = data.result;
           if ($scope.timeentry.description) {
             $scope.html_description = $scope.timeentry.description.replace(/\n/g, '<br />');
           }
         },
         function (err_res) {
           console.log("Invalid timeentry_id : " + timeentry_id);
           $state.go("timelog.list");
         });
       $scope.getDateTimeString = function (date, time) {
         var dt = new Date(date);
         return dt.toDateString() + " " + time;
       };

     }]);
