angular.module('ttrack.projects.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'projectListCtrl',
  ['$scope', 'ProjectCollection', 'Project',
   'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, ProjectCollection, Project, confirmDlg, errorDlg) {
     function update () {
       ProjectCollection.get(function (data) {
         $scope.projects = data.result;
       });
     }

     $scope.deleteProject = function(project_id) {
       var result = confirmDlg.show(
         "Delete project?",
         "All the timelog entries made for the project '" + project_id + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         Project.remove(
           {id: project_id},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }
  ]);
