# Copyright (C) 2013 HiPro IT Solutions Private Limited. All rights
# reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import colander
from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from ttrack import users, projects, tags, clients
from .common import TimeSheetAPIBase

class UserCollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1, max=1000))
    start_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    end_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    client_id = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    project_id = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    tag_name = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)


@cornice_resource(
    name="api.user_timesheet",
    path="/api/v1/user_timesheet/{userid}",
    permission="timesheet.view",
    )
class UserTimesheetAPI(TimeSheetAPIBase):

    @cornice_view(
        schema=UserCollectionGetSchema,
        validators=(
            users.validators.url_userid_exists,
            clients.validators.qs_client_id_exists,
            projects.validators.qs_project_id_exists,
            tags.validators.qs_tag_name_exists,
            '_validate_pagination_params_available',
        )
    )
    def get(self):
        user = self._url_get_user()            # mandatory
        client = self._qs_get_client()         # optional
        project = self._qs_get_project()       # optional
        tag = self._qs_get_tag()               # optional

        return self._get_filtered_data(
            self.timesheet.get_user_filtered_timelog,
            user, client, project, tag)
