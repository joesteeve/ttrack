angular.module('ttrack.clients.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'clientListCtrl',
  ['$scope', 'ClientCollection', 'Client', 'confirmationDialogYesNo',
   'apiErrorDialog',
   function ($scope, ClientCollection, Client, confirmDlg, errorDlg) {
     function update () {
       ClientCollection.get(function (data) {
         $scope.clients = data.result;
       });
     }

     $scope.deleteClient = function(client_id) {
       var result = confirmDlg.show(
         "Delete client?",
         "All projects under the client '" + client_id + "' and the related "+
           "timelog entries will be deleted. Do you want to continue?");
       result.then(function () {
         Client.remove(
           {id: client_id},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }
  ]);
