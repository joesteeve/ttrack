# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from persistent import Persistent
from BTrees.OOBTree import OOTreeSet
from zope import interface, event
from repoze import folder
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )

from ttrack import errors
from .interfaces import (
    IRole, IRoleAddedEvent, IRoleWillBeRemovedEvent, IRoleModifiedEvent)
from ttrack.auth import PERMISSIONS, AUTH_PERMISSIONS

@interface.implementer(IRoleAddedEvent)
class RoleAddedEvent(object):

    def __init__(self, role):
        self.role = role

@interface.implementer(IRoleWillBeRemovedEvent)
class RoleWillBeRemovedEvent(object):

    def __init__(self, role):
        self.role = role

@interface.implementer(IRoleModifiedEvent)
class RoleModifiedEvent(object):

    def __init__(self, role):
        self.role = role

class RoleContainer(folder.Folder):

    def add(self, role):
        super(RoleContainer, self).add(role.role_id, role, send_events=False)
        event.notify(RoleAddedEvent(role))

    def rename_role_id(self, old_id, new_id):
        """This method will be exclusively used by the evolution script to
        migrate role ids which contains capital case in them.
        """
        role = super(RoleContainer, self).get(old_id, None)
        assert role is not None, "Old role id (%s) does not exist" % (old_id,)
        assert new_id not in self, "New role id (%s) already exists" % (new_id,)
        role_id = new_id.strip().lower()
        role.role_id = role_id
        # Don't propogate the IObjectWillBeRemovedEvent and
        # IObjectAddedEvent
        super(RoleContainer, self).remove(old_id, send_events=False)
        super(RoleContainer, self).add(role_id, role, send_events=False)

    def is_id_unique(self, role_id):
        return role_id not in self

    def get_acl(self):
        acl = [
            (Allow, 'admin', ALL_PERMISSIONS),
            (Allow, Authenticated, AUTH_PERMISSIONS),
        ]
        for r in self.values():
            acl.extend(r.to_acl())
        return acl

    def get(self, role_id, *args):
        role_id = role_id.strip().lower()
        return super(RoleContainer, self).get(role_id, *args)

    def __contains__(self, role_id):
        role_id = role_id.strip().lower()
        return super(RoleContainer, self).__contains__(role_id)

    def remove(self, role_id):
        role_id = role_id.strip().lower()
        role = self.get(role_id)
        event.notify(RoleWillBeRemovedEvent(role))
        return super(RoleContainer, self).remove(role_id, send_events=False)

@interface.implementer(IRole)
class Role(Persistent):

    def __init__(self, role_id, name):
        super(Role, self).__init__()
        self.role_id = role_id.strip().lower()
        self.name = name.strip()
        self.permissions = OOTreeSet()

    def to_dict(self):
        return {
            'role_id' : self.role_id,
            'name' : self.name,
            'permissions' : [p for p in self.permissions],
        }

    def to_group(self):
        return (self.role_id,)

    def to_acl(self):
        acl = []
        for p in self.permissions:
            acl.append((Allow, self.role_id, PERMISSIONS[p]))
        return acl

    @classmethod
    def from_dict(klass, data):
        role = klass(role_id=data['role_id'], name=data['name'])
        if 'permissions' in data:
            for permission in data['permissions']:
                role.permissions.add(permission)
        return role

    def add_permission(self, permission):
        if permission not in PERMISSIONS:
            raise errors.BadParameter("Invalid permission='%s'" % (permission,))
        self.permissions.add(permission)

    def remove_permission(self, permission):
        self.permissions.remove(permission)

    def get_permissions(self):
        perm = set()
        for p in self.permissions:
            perm.update(PERMISSIONS[p])
        return tuple(perm)
