angular.module('ttrack.timelog', [
  'ui.router',
  'ttrack.timelog.controllers.list',
  'ttrack.timelog.controllers.add',
  'ttrack.timelog.controllers.show',
  'ttrack.timelog.controllers.edit'
]);

angular.module('ttrack.timelog').config(
  ['$stateProvider',
   function ($stateProvider) {
     $stateProvider
       .state('timelog', {
         parent: 'page',
         abstract: true,
         template: '<div data-ui-view></div>',
         url: '/timelog'
       })
       .state('timelog.list', {
         url: '',
         controller: 'timeEntryListCtrl',
         templateUrl: 'timelog/timeEntryList.tpl.html'
       })
       .state('timelog.show', {
         url: '/show/:timeEntryId',
         controller: 'timeEntryShowCtrl',
         templateUrl: 'timelog/timeEntryShow.tpl.html'
       })
       .state('timelog.add', {
         url: '/add',
         controller: 'timeEntryAddCtrl',
         templateUrl: 'timelog/timeEntryAdd.tpl.html'
       })
       .state('timelog.edit', {
         url: '/edit/:timeEntryId',
         controller: 'timeEntryEditCtrl',
         templateUrl: 'timelog/timeEntryEdit.tpl.html'
       });
   }]);
