# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from persistent import Persistent
from persistent.mapping import PersistentMapping

from ttrack import errors

from .timelog import (
    TimeLog,
)

from .utils import (
    get_or_create_timelog,
    cleanup_timeentry,
)


class TagTimeSheet(Persistent):
    """Per tag time-sheet"""

    def __init__(self):
        super(TagTimeSheet, self).__init__()
        self._all = TimeLog()
        self._users = PersistentMapping()
        self._projects = PersistentMapping()
        self._activities = PersistentMapping()

    def add(self, timeentry):
        self._all.add(timeentry)
        # add to tag.user timelog
        tl = get_or_create_timelog(self._users, timeentry.user)
        tl.add(timeentry)
        # add to tag.project timelog
        tl = get_or_create_timelog(self._projects, timeentry.project)
        tl.add(timeentry)
        # add to tag.activity timelog
        tl = get_or_create_timelog(self._activities, timeentry.activity)
        tl.add(timeentry)

    def remove(self, timeentry):
        self._all.remove(timeentry)
        cleanup_timeentry(self._users, timeentry.user, timeentry)
        cleanup_timeentry(self._projects, timeentry.project, timeentry)
        cleanup_timeentry(self._activities, timeentry.activity, timeentry)

    def get_all(self):
        return self._all

    def get_user(self, user):
        return self._users.get(user, TimeLog())

    def get_project(self, project):
        return self._projects.get(project, TimeLog())

    def get_activity(self, activity):
        return self._activities.get(activity, TimeLog())

    def get_statistics_for_time_key(self, time_key):
        u_stats = {}
        p_stats = {}
        c_stats = {}
        for user, timelog in self._users.items():
            u_stats[user.userid] = timelog.get_total_time_mins(key=time_key)
        for project, timelog in self._projects.items():
            a_stats = {}
            for activity in project.activities.values():
                if activity in self._activities:
                    # TimeLog for an activity will be added only when
                    # a timeentry containing that activity is
                    # added. But the ActivityTimeSheet in TimeSheet
                    # object will be created as and when the activity
                    # is created
                    a_tl = self._activities[activity]
                    a_stats[activity.name] = a_tl.get_total_time_mins(
                        key=time_key)
            p_stats[project.project_id] = {
                'total' : timelog.get_total_time_mins(key=time_key),
                'per-activity' : a_stats,
            }
            cid = project.client.client_id
            if cid not in c_stats:
                c_stats[cid] = 0
            c_stats[cid] += p_stats[project.project_id]['total']
        return {
            'users' : u_stats,
            'projects' : p_stats,
            'clients' : c_stats,
            'total' : self._all.get_total_time_mins(key=time_key)
        }

    def get_statistics_for_range(self, start_datetime, end_datetime):
        u_stats = {}
        p_stats = {}
        c_stats = {}
        total = 0

        for entry in self._all.get_ite_entries(start_datetime, end_datetime):
            mins = entry.effective_time_mins()
            # per-user time-spent
            if entry.user.userid not in u_stats:
                u_stats[entry.user.userid] = 0
            u_stats[entry.user.userid] += mins
            if entry.project.project_id not in p_stats:
                p_stats[entry.project.project_id] = {
                    'total' : 0,
                    'per-activity': {},
                }
            p_stats[entry.project.project_id]['total'] += mins
            # per activity time-spent
            a_st = p_stats[entry.project.project_id]['per-activity']
            if entry.activity.name not in a_st:
                a_st[entry.activity.name] = 0
            a_st[entry.activity.name] += mins
            # per client time-spent
            cid = entry.project.client.client_id
            if cid not in c_stats:
                c_stats[cid] = 0
            c_stats[cid] += mins
            # total time-spent
            total += mins

        return {
            'users' : u_stats,
            'projects' : p_stats,
            'clients' : c_stats,
            'total' : total,
        }
