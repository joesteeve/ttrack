angular.module('ttrack.tags.controllers.show', [
  'ttrack.resources'
]).controller(
  'tagShowCtrl',
  ['$scope', 'Tag', '$stateParams', '$state',
   function ($scope, Tag, $stateParams, $state) {
     var tag_name = $stateParams.tagName;
     Tag.get(
       {name: tag_name},
       function (data) {
         $scope.tag = data.result;
       },
       function (err_res) {
         console.log("Invalid tagname : " + tag_name);
         $state.go("manage.tags.list");
       });
   }
  ]);
