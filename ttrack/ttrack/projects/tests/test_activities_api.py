# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy
from repoze.folder import Folder

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.users.models import User, UserContainer, find_group

from ttrack.projects.models import (
    ProjectContainer,
    Project,
    Activity,
    Member,
    )

from ttrack.clients.models import (
    ClientContainer,
    Client,
    )


USER_DATA = (
    {"userid" : "admin",
     "real_name" : "Administrator",
     "email_id" : "admin@admin.org",
     "password" : "pass"},
    {"userid" : "john",
     "real_name" : "John Doe",
     "email_id" : "john@doe.org",
     "password" : "john"},
    {"userid" : "smith",
     "real_name" : "Jane Smith",
     "email_id" : "jane@smith.org",
     "password" : "jane"}
)

CLIENT_DATA = (
    {
        'client_id' : 'c1',
        'name': 'Client 1',
        'address': 'c1 Address',
        },
    {
        'client_id' : 'c2',
        'name' : 'Client 2',
        'address' : 'c2 Address',
        },
    )

PROJECT_DATA = (
    {
        'project_id' : 'p1',
        'client' : 'c1',
        'name': 'Project One',
        'activities': ['p1 Activity One', 'p1 activity Two',
                       'p1 activity three'],
        'userid' : 'admin',
        'members' : ['john', 'smith']
    },
    {
        'project_id' : 'p3',
        'client' : 'c2',
        'name' : 'Project Three',
        'activities': ['p3 c2 act one', 'p3 Act Two', 'p3 c2 Activity Three'],
        'members': ['smith']
    },
    )


class TestRoot(Folder):
    __acl__ = ((Allow, Authenticated, "projects.view"),
               (Allow, Authenticated, 'view'),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(udata, cdata, pdata):
    root = TestRoot()
    root['users'] = UserContainer()
    root['clients'] = ClientContainer()
    root['projects'] = ProjectContainer()

    for u in udata:
        user = User.from_dict(u)
        root['users'].add(user)

    for c in cdata:
        client = Client.from_dict(c)
        root['clients'].add(client)

    for p in pdata:
        pd = {}
        pd.update(p)
        pd['client'] = root['clients'].get(p['client'])
        project = Project.from_dict(pd)
        root['projects'].add(project)
        for activity in pd['activities']:
            project.add_activity(activity)
        for userid in pd['members']:
            mem = Member(root['users'].get(userid))
            mem.role = project.roles.get('member')
            project.add_member(mem)


    def root_factory(request):
        return root
    return root_factory

class ActivityAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/activities/p1'
        self.api_url_p3 = '/api/v1/activities/p3'
        self.api_url_not_exist = '/api/v1/activities/p2' # non existent
        self.p1_data = PROJECT_DATA[0]['activities']
        self.p3_data = PROJECT_DATA[1]['activities']

        self.clients_api_url = '/api/v1/clients'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all_activities_for_existent_project(self):
        resp = self.app.get(self.api_url_p1, status=HTTPOk.code)
        activities = resp.json['result']
        self.assertEqual(len(activities), len(self.p1_data))

    def test_get_all_activities_for_non_existent_project(self):
        resp = self.app.get(self.api_url_not_exist, status=HTTPNotFound.code)

    def test_add_new_for_existent_project(self):
        new_activity = { 'name' : 'New Activity'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPOk.code)
        act = resp.json['result']
        self.assertEqual(new_activity['name'], act['name'])

        resp = self.app.get(self.api_url_p1 + "/New Activity.json",
                            status=HTTPOk.code)
        act = resp.json['result']
        self.assertEqual(new_activity['name'], act['name'])

    def test_add_new_for_non_existent_project(self):
        new_activity = { 'name' : 'New Activity'}
        resp = self.app.post_json(self.api_url_not_exist, new_activity,
                                  status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_name(self):

        new_activity = { 'name' : 'p1 Activity One'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_name_case_insensitive(self):
        new_activity = { 'name' : 'P1 AcTivitY ONE'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_name_non_existent_project(self):
        new_activity = { 'name' : 'p1 Activity One'}
        resp = self.app.post_json(self.api_url_not_exist, new_activity,
                                  status=HTTPBadRequest.code)

    def test_add_fail_on_bad_schema(self):
        new_activity = {}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPBadRequest.code)

    def test_add_fail_on_bad_schema_non_existent_project(self):
        new_activity = {}
        resp = self.app.post_json(self.api_url_not_exist, new_activity,
                                  status=HTTPBadRequest.code)

    def test_get_existent_on_existent_project(self):
        resp = self.app.get(self.api_url_p1 + "/p1 Activity One.json",
                            status=HTTPOk.code)
        act = resp.json['result']
        self.assertEqual('p1 Activity One', act['name'])

    def test_get_case_insensitive_on_existent_project(self):
        resp = self.app.get(self.api_url_p1 + "/P1 ACTivity ONE.json",
                            status=HTTPOk.code)
        act = resp.json['result']
        self.assertEqual('p1 Activity One', act['name'])

    def test_get_on_non_existent_project(self):
        resp = self.app.get(self.api_url_not_exist + "/p1 Activity One.json",
                            status=HTTPNotFound.code)

    def test_get_non_existent(self):
        resp = self.app.get(self.api_url_p1 + "/p1 Activity Twenty.json",
                            status=HTTPNotFound.code)

    def test_put_change_name(self):
        act = { 'name' : 'Name Changed' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPOk.code)
        resp = self.app.get(self.api_url_p1 + "/Name Changed.json",
                            status=HTTPOk.code)
        upd_act = resp.json['result']
        self.assertEqual(act['name'], upd_act['name'])

    def test_put_change_name_case(self):
        act = { 'name' : 'P1 Activity ONE' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPOk.code)
        # As the change is only in the case we get by the old name
        # itself
        resp = self.app.get(self.api_url_p1 + "/p1 Activity One.json",
                            status=HTTPOk.code)
        upd_act = resp.json['result']
        self.assertEqual(act['name'], upd_act['name'])

    def test_put_non_exsistent(self):
        act = { 'name' : 'Name Changed' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity Twenty.json", act,
                          status=HTTPBadRequest.code)

    def test_put_duplicate_name(self):
        act = { 'name' : 'p1 activity Two'}
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPBadRequest.code)

    def test_put_duplicate_name_case_insensitive(self):
        act = { 'name' : 'P1 ACTiviTY TWO'}
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPBadRequest.code)

    def test_delete(self):
        self.app.delete(self.api_url_p1 + "/p1 Activity One.json",
                        status=HTTPOk.code)

    def test_delete_non_existent(self):
        self.app.delete(self.api_url_p1 + "/Not here.json",
                        status=HTTPBadRequest.code)


class ActivityAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/activities/p1'
        self.api_url_p3 = '/api/v1/activities/p3'
        self.p1_data = PROJECT_DATA[0]['activities']
        self.p3_data = PROJECT_DATA[1]['activities']
        self.config = testing.setUp()
        self.config.include("cornice")
        request = testing.DummyRequest()
        root_factory = build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA)
        request.root = root_factory(None)
        self.config.testing_securitypolicy(
            userid='john', groupids=find_group('john', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(root_factory)
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url_p1, status=HTTPOk.code)

    def test_deny_get_collection(self):
        self.app.get(self.api_url_p3, status=HTTPForbidden.code)

    def test_allow_get(self):
        self.app.get(self.api_url_p1 + '/p1 activity Two.json',
                     status=HTTPOk.code)

    def test_deny_get(self):
        self.app.get(self.api_url_p3 + '/p3 c2 act one.json',
                     status=HTTPForbidden.code)

    def test_deny_add(self):
        new_activity = { 'name' : 'New Activity'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPForbidden.code)

    def test_deny_update(self):
        act = { 'name' : 'Name Changed' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url_p1 + "/p1 Activity One.json",
                        status=HTTPForbidden.code)


class ActivityAPIAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/activities/p1'
        self.api_url_p3 = '/api/v1/activities/p3'
        self.p1_data = PROJECT_DATA[0]['activities']
        self.p3_data = PROJECT_DATA[1]['activities']
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url_p3, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url_p3 + '/p3 c2 Activity Three.json',
                     status=HTTPOk.code)

    def test_allow_add(self):
        new_activity = { 'name' : 'New Activity'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPOk.code)

    def test_allow_update(self):
        act = { 'name' : 'Name Changed' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url_p1 + "/p1 Activity One.json",
                        status=HTTPOk.code)


class ActivityAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = '/api/v1/activities/p1'
        self.api_url_p3 = '/api/v1/activities/p3'
        self.p1_data = PROJECT_DATA[0]['activities']
        self.p3_data = PROJECT_DATA[1]['activities']
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url_p3, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url_p3 + '/p3 c2 Activity Three.json',
                     status=HTTPForbidden.code)

    def test_deny_add(self):
        new_activity = { 'name' : 'New Activity'}
        resp = self.app.post_json(self.api_url_p1, new_activity,
                                  status=HTTPForbidden.code)

    def test_deny_update(self):
        act = { 'name' : 'Name Changed' }
        self.app.put_json(self.api_url_p1 + "/p1 Activity One.json", act,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url_p1 + "/p1 Activity One.json",
                        status=HTTPForbidden.code)
