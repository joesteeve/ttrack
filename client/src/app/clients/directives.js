angular.module('ttrack.clients.directives', [
  'ttrack.resources'
])

  .directive(
    'uniqueClientId',
    ['Client', function(Client) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Client.get(
                  { id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueClientId', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueClientId', true);
                  });
              }
            });
          });
        }
      };
    }]);
