# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import authenticated_userid

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

@cornice_resource(
    name="api.session",
    path="/api/v1/ui/session",
    permission="view"
)
class SessionAPI(object):

    def __init__(self, request):
        self.request = request
        self.users = request.root['users']
        self.current_user = self.users[authenticated_userid(request)]

    def get(self):
        user_info = self.current_user.to_dict()
        global_perm_list = self.current_user.get_permissions()
        project_perm = {}
        for p in self.current_user.projects:
            project_perm[p.project_id] = p.get_permissions()

        user_info['permissions'] = {
            'global': global_perm_list,
            'per-project': project_perm
        }
        return {
            'type': 'session',
            'result': user_info
        }
