# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.clients.models import (
    ClientContainer,
    Client
    )

CLIENT_DATA = (
    {
        'client_id' : 'c1',
        'name': 'Client One',
        'address': 'Unknown',
    },
    {
        'client_id' : 'c2',
        'name' : 'Client two',
        'address' : 'C2 address',
    },
    {
        'client_id' : 'c3',
        'name' : 'Client three',
        'address' : 'C3 address',
        'projects' : ['p1', 'p2'],
    },
    {
        'client_id' : 'four',
        'name' : 'Client Four',
        'address' : 'Client Four Address',
    }
)


class MockProject(object):
    def __init__(self, project_id):
        self.project_id = project_id

    def to_dict(self):
        return {}

class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "clients.view"),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(cdata):
    root = TestRoot()
    root['clients'] = ClientContainer()
    for c in cdata:
        client = {}
        client.update(c)
        client['projects'] = []
        if 'projects' in c:
            for p in c['projects']:
                client['projects'].append(MockProject(p))
        client = Client.from_dict(client)
        root['clients'].add(client)

    def root_factory(request):
        return root
    return root_factory

def client_in_list(data, client_list):
    for client in client_list:
        if data['client_id'] == client['client_id']:
            return True
    return False


class ClientAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/clients'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(build_root_factory(CLIENT_DATA))
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all_clients(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        clients = resp.json['result']
        self.assertEqual(len(clients), len(CLIENT_DATA))
        for d in CLIENT_DATA:
            self.assertTrue(client_in_list(d, clients))

    def test_get_existing_client(self):
        resp = self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)
        client = resp.json['result']
        self.assertEqual('c1', client['client_id'])

    def test_get_existing_client_case_insensitive_client_id(self):
        resp = self.app.get(self.api_url + '/FouR.json', status=HTTPOk.code)
        client = resp.json['result']
        self.assertEqual(client['client_id'], 'four')

    def test_get_project_list_in_client_object_json(self):
        resp = self.app.get(self.api_url + '/c3.json', status=HTTPOk.code)
        client = resp.json['result']
        self.assertTrue(len(client['projects']) is not 0)

    def test_get_non_existant_client(self):
        self.app.get(self.api_url + '/unknown.json', status=HTTPNotFound.code)

    def test_add_new(self):
        new_client = {
            'client_id' : 'new',
            'name' : 'New Client',
            }
        resp = self.app.post_json(self.api_url, new_client, status=HTTPOk.code)
        client = resp.json['result']
        self.assertEqual(new_client['client_id'], client['client_id'])

        resp = self.app.get(self.api_url + '/new.json', status=HTTPOk.code)
        client = resp.json['result']
        self.assertEqual(new_client['client_id'], client['client_id'])

    def test_add_fail_on_non_unique_client_id(self):
        new_client = {
            'client_id' : 'c1',
            'name' : 'New Client',
            }
        self.app.post_json(self.api_url, new_client, status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_case_insensitive_client_id(self):
        new_client = {
            'client_id' : 'FOUR',
            'name' : 'New Client',
            }
        self.app.post_json(self.api_url, new_client, status=HTTPBadRequest.code)

    def test_add_fail_on_bad_schema(self):
        new_client = {
            'client_id' : 'new',
            }
        self.app.post_json(self.api_url, new_client, status=HTTPBadRequest.code)

    def test_put_change_client_name(self):
        client = {
            'client_id' : 'c1',
            'name' : 'Updated name',
            }
        self.app.put_json(self.api_url + '/c1.json', client, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)
        upd_client = resp.json['result']
        self.assertEqual(client['name'], upd_client['name'])

    def test_put_change_client_address(self):
        client = {
            'client_id' : 'c1',
            'address' : 'Updated address',
            }
        self.app.put_json(self.api_url + '/c1.json', client, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)
        upd_client = resp.json['result']
        self.assertEqual(client['address'], upd_client['address'])

    def test_put_change_client_name_and_address(self):
        client = {
            'client_id' : 'c1',
            'name' : 'Updated name',
            'address' : 'Updated address',
            }
        self.app.put_json(self.api_url + '/c1.json', client, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)
        upd_client = resp.json['result']
        self.assertEqual(client['name'], upd_client['name'])
        self.assertEqual(client['address'], upd_client['address'])

    def test_put_fail_on_client_id_change(self):
        client = {
            'client_id' : 'new',
            }
        self.app.put_json(self.api_url + '/c1.json', client,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_client_id_1(self):
        data = {
            'client_id' : 'four',
            'name' : 'Fourth Client'
        }
        self.app.put_json(self.api_url + '/Four.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_client_id_2(self):
        data = {
            'client_id' : 'FouR',
            'name' : 'Fourth Client'
        }
        self.app.put_json(self.api_url + '/four.json', data,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_non_existant_client_id(self):
        client = {
            'client_id' : 'other',
            }
        self.app.put_json(self.api_url + '/other.json', client,
                          status=HTTPBadRequest.code)

    def test_delete(self):
        self.app.delete(self.api_url + '/c1.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/c1.json', status=HTTPNotFound.code)

    def test_delete_non_existant_user(self):
        self.app.delete(
            self.api_url + '/other.json', status=HTTPBadRequest.code)

    def test_delete_case_insensitive(self):
        self.app.delete(self.api_url + '/FoUR.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/four.json', status=HTTPNotFound.code)


class ClientAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/clients'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(CLIENT_DATA))
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)

    def test_deny_add(self):
        client = {
            'client_id' : 'new',
            'name' : 'New Client',
            }
        self.app.post_json(self.api_url, client, status=HTTPForbidden.code)

    def test_deny_update(self):
        client = {
            'client_id' : 'c1',
            'name' : 'New Client',
            }
        self.app.put_json(self.api_url + '/c1.json', client,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/c1.json', status=HTTPForbidden.code)


class ClientAPIAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/clients'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(CLIENT_DATA))
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/c1.json', status=HTTPOk.code)

    def test_allow_add(self):
        client = {
            'client_id' : 'new',
            'name' : 'New Client',
            }
        self.app.post_json(self.api_url, client, status=HTTPOk.code)

    def test_allow_update(self):
        client = {
            'client_id' : 'c1',
            'name' : 'New Client',
            }
        self.app.put_json(self.api_url + '/c1.json', client, status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url + '/c1.json', status=HTTPOk.code)


class ClientAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/clients'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(CLIENT_DATA))
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url + '/c1.json', status=HTTPForbidden.code)

    def test_deny_add(self):
        client = {
            'client_id' : 'new',
            'name' : 'New Client',
            }
        self.app.post_json(self.api_url, client, status=HTTPForbidden.code)

    def test_deny_update(self):
        client = {
            'client_id' : 'c1',
            'name' : 'New Client',
            }
        self.app.put_json(
            self.api_url + '/c1.json', client, status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/c1.json', status=HTTPForbidden.code)
