angular.module('ttrack.filters', [
])
  .filter(
    'matchProject',
    function () {
      return function (project_list, search_term) {
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in project_list) {
          var project_name = project_list[index].name.toLowerCase();
          var project_id = project_list[index].project_id.toLowerCase();
          if (project_name.indexOf(term) != -1 ||
              project_id.indexOf(term) != -1) {
            result.push(project_list[index]);
          }
        }
        return result;
      };
    })
  .filter(
    'matchTime',
    function () {
      return function (time_list, search_term) {
        var result = [];
        var time_split = search_term.split(":");
        var hour = time_split[0];
        var term = "";
        if (hour < 10 && hour.length == 1) {
          term += "0";
        }
        else if (hour > 23 || hour < 0) {
          return result;
        }

        term = term + hour + ":";
        if (time_split.length > 1) {
          var min = time_split[1];
          if (min > 59 || min < 0) {
            return result;
          }
          term += min;
        }

        var exp = new RegExp("^" + term);
        for (var index in time_list) {
          var time = time_list[index];
          if (time.match(exp)) {
            result.push(time);
          }
        }
        return result;
      };
    });
