# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component
import zope.component.testing as zc_testing
import pyramid.testing

import ttrack
from ttrack.models import create_app_root
from ttrack.users.models import UserContainer, User
from ttrack.clients.models import ClientContainer, Client
from ttrack.projects.models import ProjectContainer, Project, Member
from ttrack import users, clients, projects, tags, timesheet, roles
from ttrack.timesheet.models import UITimeEntryFactory
from ttrack.roles import Role

from ttrack import sample_data1 as s1

def build_root_with_s1():
    app_root = create_app_root()
    populate_base_data(app_root, s1.ROLES, s1.USERS, s1.CLIENTS, s1.PROJECTS)
    populate_timeentry_data(app_root, s1.TIMEENTRIES)
    return app_root

def build_root_with_s1_base():
    app_root = create_app_root()
    populate_base_data(app_root, s1.ROLES, s1.USERS, s1.CLIENTS, s1.PROJECTS)
    return app_root

def populate_base_data(root, rdata, udata, cdata, pdata):
    users = root['users']
    roles = root['roles']
    clients = root['clients']
    projects = root['projects']

    for d in rdata:
        r = Role.from_dict(d)
        roles.add(r)
    for d in udata:
        user = User.from_dict(d)
        user.role = roles.get(d.get('role_id', None), None)
        users.add(user)
    for d in cdata:
        clients.add(Client.from_dict(d))
    for _d in pdata:
        d = {}
        d.update(_d)
        d['client'] = clients.get(d['client'])
        project = Project.from_dict(d)
        projects.add(project)
        for activity in d['activities']:
            project.add_activity(activity)
        for userid in d['members']:
            mem = Member(users.get(userid))
            mem.role = project.roles.get('member')
            project.add_member(mem)
    return root

def populate_timeentry_data(root, tdata):
    projects = root['projects']
    users = root['users']
    te_factory = UITimeEntryFactory(root['tags'])
    timesheet = root['timesheet']
    for d in tdata:
        user = users.get(d['userid'])
        log.debug("userid : (%s)", d['userid'])
        project = projects.get(d['project_id'])
        te = te_factory.create(d['start_datetime'], d['end_datetime'],
                               user, project, d['activity'], d['tags'],
                               d.get('description', None),
                               d.get('working_hours_only', False))
        timesheet.add(te)

def setUp(root_factory=build_root_with_s1):
    zc_testing.setUp()
    registry = component.getGlobalSiteManager()
    config = pyramid.testing.setUp(registry=registry)
    config.setup_registry(root_factory=root_factory)
    config.include('pyramid_zcml')
    config.load_zcml('ttrack:configure.zcml')
    config.include("cornice")

    ttrack.register_zca_subscribers()
    users.register_zca_subscribers()
    clients.register_zca_subscribers()
    projects.register_zca_subscribers()
    tags.register_zca_subscribers()
    timesheet.register_zca_subscribers()
    roles.register_zca_subscribers()
    return config

def tearDown():
    pyramid.testing.tearDown()
    zc_testing.tearDown()
