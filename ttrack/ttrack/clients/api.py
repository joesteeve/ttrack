# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import (
    authenticated_userid,
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )
import colander

from .models import Client
from ttrack import errors
from . import validators

class AddClientSchema(colander.MappingSchema):
    client_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    address = colander.SchemaNode(
        colander.String(), location="body", type="str",
        missing=colander.drop)

class PutClientSchema(colander.MappingSchema):
    client_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str",
        missing=colander.drop)
    address = colander.SchemaNode(
        colander.String(), location="body", type="str",
        missing=colander.drop)


@cornice_resource(
    name="api.clients",
    collection_path="/api/v1/clients",
    path="/api/v1/clients/{client_id}.json",
    permission="clients.manage",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.clients = request.root['clients']

    # Helper methods
    def is_client_id_unique(self, request):
        client_id = request.json['client_id']
        # we should check only when there is a 'client_id'. missing
        # fields are handled by schema validation.
        if client_id and not self.clients.is_id_unique(client_id):
            self.request.errors.add('body', 'client_id', 'Client already exists')
            self.request.errors.status = HTTPBadRequest.code

    # Views
    @cornice_view(permission="clients.view")
    def collection_get(self):
        data = {
            "type" : 'client_list',
            "result" : None,
            }
        data['result'] = [client.to_dict()
                          for client in self.clients.values()]
        return data

    @cornice_view(schema=AddClientSchema,
                  validators=('is_client_id_unique', ))
    def collection_post(self):
        client = Client.from_dict(self.request.json)
        self.clients.add(client)
        data = {
            'type' : 'client',
            'result' : self.request.json
            }
        return data

    @cornice_view(permission="clients.view",
                  validators=(validators.url_client_id_exists,))
    def get(self):
        client_id = self.request.matchdict['client_id']
        client = self.clients.get(client_id)
        data = {
            'type' : "client",
            'result' : client.to_dict(),
            }
        return data

    @cornice_view(schema=PutClientSchema,
                  validators=(validators.strict_url_client_id_exists,))
    def put(self):
        client_id = self.request.matchdict['client_id']

        # enforce 'client_id' is read-only
        if client_id != self.request.json_body['client_id']:
            self.request.errors.add(
                'body', 'client_id', 'Client_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        client = self.clients.get(client_id)
        updated_client = self.request.json

        if "name" in updated_client:
            client.name = updated_client["name"]
        if "address" in updated_client:
            client.address = updated_client["address"]

        data = {
            'type' : 'client',
            'result' : client.to_dict(),
            }
        return data

    @cornice_view(validators=(validators.strict_url_client_id_exists,))
    def delete(self):
        route_client_id = self.request.matchdict['client_id']
        self.clients.remove(route_client_id)
