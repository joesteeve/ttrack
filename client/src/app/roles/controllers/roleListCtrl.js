angular.module('ttrack.roles.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'roleListCtrl',
  ['$scope', 'RoleCollection', 'Role', 'confirmationDialogYesNo',
   'apiErrorDialog',
   function ($scope, RoleCollection, Role, confirmDlg, errorDlg) {
     function update () {
       RoleCollection.get(function (data) {
         $scope.roles = data.result;
       });
     }

     $scope.deleteRole = function(role_id) {
       var result = confirmDlg.show(
         "Delete role?",
         "Users who are mapped under the role '" + role_id + "' will be " +
           "made as normal users. Do you want to continue?");
       result.then(function () {
         Role.remove(
           {id: role_id},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }]);
