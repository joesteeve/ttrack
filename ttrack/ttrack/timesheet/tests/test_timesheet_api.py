# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

import ttrack.testing
from .api_data import TE_DATA
from ttrack.users.models import find_group

class TimeSheetAPITests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_get_fail_on_bad_userid(self):
        self.app.get(self.api_url, {'userid' : 'user5'},
                     status=HTTPBadRequest.code)

    def test_get_fail_on_page_out_of_range(self):
        self.app.get(self.api_url, {'page' : 100},
                     status=HTTPBadRequest.code)

    def test_get_fail_on_invalid_page_number(self):
        self.app.get(self.api_url, {'page' : 10.2},
                     status=HTTPBadRequest.code)

    def test_get_fail_on_negative_page_number(self):
        self.app.get(self.api_url, {'page' : -1},
                     status=HTTPBadRequest.code)

    def test_get_fail_on_excessive_page_size(self):
        self.app.get(self.api_url, {'page_size' : 1500},
                     status=HTTPBadRequest.code)

    def test_get_fail_on_negative_page_size(self):
        self.app.get(self.api_url, {'page_size' : -10},
                     status=HTTPBadRequest.code)

    def test_get_entries_for_authenticated_user_default_page(self):
        resp = self.app.get(self.api_url)
        # Totally there are 4 entries out of which 2 are multi day
        # entries one spanning two days and other spanning three
        # days. So we have total 7 InternalTimeEntry objects in the
        # collection
        self.assertEqual(len(resp.json['result']), 7)

    def test_get_entries_for_userid_default_page(self):
        resp = self.app.get(self.api_url, {'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 6)

    def test_get_entries_for_authenticated_user_first_page(self):
        resp = self.app.get(self.api_url, {'page' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 7)

    def test_get_single_last_entry(self):
        resp = self.app.get(self.api_url, {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '00:00')
        self.assertEqual(res['date'], '2014-01-04')
        self.assertEqual(res['end_time'], '10:00')

    def test_get_pages_for_authenticated_userid(self):
        resp = self.app.get(self.api_url, {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '00:00')
        self.assertEqual(res['date'], '2014-01-04')
        self.assertEqual(res['end_time'], '10:00')

        resp = self.app.get(self.api_url, {'page' : 2, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-03')

        resp = self.app.get(self.api_url, {'page' : 3, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-03')

        resp = self.app.get(self.api_url, {'page' : 4, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-02')

        resp = self.app.get(self.api_url, {'page' : 5, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-02')

        resp = self.app.get(self.api_url, {'page' : 6, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-02')

        resp = self.app.get(self.api_url, {'page' : 7, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-01')

        self.app.get(self.api_url, {'page' : 8, 'page_size' : 1},
                     status=HTTPBadRequest.code)

    def test_get_pages_for_other_userid(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '08:30')
        self.assertEqual(res['date'], '2014-01-03')
        self.assertEqual(res['end_time'], '18:30')

        resp = self.app.get(self.api_url,
                            {'page' : 2, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '00:00')
        self.assertEqual(res['date'], '2014-01-03')
        self.assertEqual(res['end_time'], '10:00')

        resp = self.app.get(self.api_url,
                            {'page' : 3, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '12:00')
        self.assertEqual(res['date'], '2014-01-02')
        self.assertEqual(res['end_time'], '00:00')

        resp = self.app.get(self.api_url,
                            {'page' : 4, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '00:00')
        self.assertEqual(res['date'], '2014-01-02')
        self.assertEqual(res['end_time'], '11:30')

        resp = self.app.get(self.api_url,
                            {'page' : 5, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '10:30')
        self.assertEqual(res['date'], '2014-01-01')
        self.assertEqual(res['end_time'], '00:00')

        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1, 'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '09:00')
        self.assertEqual(res['date'], '2014-01-01')
        self.assertEqual(res['end_time'], '10:00')

        self.app.get(self.api_url,
                     {'page' : 7, 'page_size' : 1, 'userid': 'john'},
                     status=HTTPBadRequest.code)

    def test_post_fail_on_invalid_project_id(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '10:00',
                              'end_time': '10:30'},],
            'project_id' : 'p2',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_invalid_userid(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '10:00',
                              'end_time': '10:30'},],
            'project_id' : 'cfactory',
            'userid' : 'user5',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_invalid_time(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '10:00',
                              'end_time': '24:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)


    def test_post_fail_on_invalid_date(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-13-1',
                              'start_time' : '10:00',
                              'end_time': '14:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_incorrect_start_and_end_time(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '10:00',
                              'end_time': '04:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_incorrect_start_and_end_date(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-2',
                              'end_date' : '2014-1-1',
                              'start_time' : '10:00',
                              'end_time': '14:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_missing_time_splits(self):
        time_entries = {
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_empty_time_splits(self):
        time_entries = {
            'time_splits' : [],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_single_conflict(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '09:00',
                              'end_time': '10:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

    def test_post_fail_on_any_one_conflict(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '09:00',
                              'end_time': '10:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 7)

    def test_post_fail_on_any_one_split(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPBadRequest.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 7)

    def test_post_all_good_default_entry(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_post_ignore_conflict(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '09:00',
                              'end_time': '10:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'ignore_conflicts': True,
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_post_ignore_conflict_on_split(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'ignore_conflicts': True,
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_post_ignore_conflict_multi_day(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-2',
                              'start_time' : '08:00',
                              'end_time': '09:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'ignore_conflicts': True,
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_post_new_activity(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '08:00')
        self.assertEqual(res['end_time'], '09:00')
        self.assertEqual(res['activity'], time_entries['activity'])

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_post_new_single_tag(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'tags' : ["tag1",],
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '08:00')
        self.assertEqual(res['end_time'], '09:00')
        self.assertEqual(res['tags'][0], "tag1")

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        print(resp.json['result'])
        self.assertEqual(len(resp.json['result']), 8)

    def test_post_multiple_new_tags(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'tags' : ["tag1", "tag2", "tag3"],
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['start_time'], '08:00')
        self.assertEqual(res['end_time'], '09:00')
        self.assertEqual(res['tags'][0], "tag1")
        self.assertEqual(res['tags'][1], "tag2")
        self.assertEqual(res['tags'][2], "tag3")

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_post_multi_day_working_hours_only(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'working_hours_only' : True,
            'ignore_conflicts' : True,
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['working_hours_only'], True)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_post_multi_day_all_hours_only(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'ignore_conflicts' : True,
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['working_hours_only'], False)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_post_all_fields(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
            'tags' : ["tag1",],
            'description' : 'Worked on a new activity',
            'working_hours_only' : True,
            'ignore_conflicts' : True,
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['working_hours_only'], True)
        self.assertEqual(res['activity'], time_entries['activity'])
        self.assertEqual(res['tags'][0], "tag1")
        self.assertEqual(res['description'], time_entries['description'])

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_post_all_fields_for_other_user(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'userid' : 'john',
            'activity' : 'new activity',
            'tags' : ["tag1",],
            'description' : 'Worked on a new activity',
            'working_hours_only' : True,
            'ignore_conflicts' : True,
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['working_hours_only'], True)
        self.assertEqual(res['activity'], time_entries['activity'])
        self.assertEqual(res['tags'][0], "tag1")
        self.assertEqual(res['description'], time_entries['description'])

        resp = self.app.get(self.api_url, {'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_get_for_authenticated_userid(self):
        resp = self.app.get(self.api_url, {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        resp = self.app.get(self.api_url + '/%s.json' % (timeentry_id,),
                            status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['timeentry_id'], timeentry_id)


    def test_get_for_other_userid(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        resp = self.app.get(self.api_url + '/%s.json' % (timeentry_id,),
                            {'userid' : 'john'}, status=HTTPOk.code)
        res = resp.json['result']
        self.assertEqual(res['timeentry_id'], timeentry_id)

    def test_get_fail_for_invalid_userid(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        self.app.get(self.api_url + '/%s.json' % (timeentry_id,),
                     {'userid' : 'user5'}, status=HTTPBadRequest.code)

    def test_get_fail_for_invalid_timeentry(self):
        self.app.get(self.api_url + '/invalid.json',
                     status=HTTPNotFound.code)

    def test_put_fail_for_invalid_timeentry(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/invalid.json', time_entries,
                          status=HTTPNotFound.code)

    def test_put_fail_for_invalid_userid(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'userid' : 'user5',
            'activity' : 'a1',
        }

        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPBadRequest.code)

    def test_put_fail_on_single_conflict(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-3',
                              'end_date' : '2014-1-3',
                              'start_time' : '08:30',
                              'end_time': '18:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPBadRequest.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 7)

    def test_put_fail_on_any_one_conflict(self):
        resp = self.app.get(self.api_url,
                            {'page' : 5, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '09:00',
                              'end_time': '10:00'},],
            'project_id' : 'cfactory',
            'userid' : 'john',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPBadRequest.code)

        resp = self.app.get(self.api_url, {'userid': 'john'},status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 6)

    def test_put_fail_on_split(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-3',
                              'end_date' : '2014-1-3',
                              'start_time' : '08:30',
                              'end_time': '22:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPBadRequest.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 7)

    def test_put_single_good_entry(self):
        resp = self.app.get(self.api_url,
                            {'page' : 2, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']
        log.debug(res)

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-4',
                              'start_time' : '10:30',
                              'end_time': '22:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url,
                            {'page': 1, 'page_size': 1}, status=HTTPOk.code)
        res = resp.json['result'][0]
        self.assertEqual(res['date'], '2014-01-04')
        self.assertEqual(res['start_time'], '10:30')
        self.assertEqual(res['end_time'], '22:30')

    def test_put_multiple_good_entries(self):
        resp = self.app.get(self.api_url,
                            {'page' : 2, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']
        log.debug(res)

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-4',
                              'start_time' : '10:30',
                              'end_time': '11:30'},
                             {'start_date' : '2014-1-4',
                              'end_date' : '2014-1-4',
                              'start_time' : '11:30',
                              'end_time': '12:30'},
                             {'start_date' : '2014-1-4',
                              'end_date' : '2014-1-4',
                              'start_time' : '13:30',
                              'end_time': '14:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPOk.code)
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_put_ignore_single_conflict(self):
        resp = self.app.get(self.api_url,
                            {'page' : 1, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-3',
                              'end_date' : '2014-1-3',
                              'start_time' : '08:30',
                              'end_time': '18:30'},],
            'project_id' : 'cfactory',
            'activity' : 'a1',
            'ignore_conflicts': True,
        }
        res = self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                                time_entries, status=HTTPOk.code)

        # We have reduced multi-day entry which spans to three days
        # into one day entry so our total count will reduce by 2
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 5)

    def test_put_ignore_conflict_on_any_one_conflict(self):
        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-2',
                              'start_time' : '10:00',
                              'end_time': '11:30'},],
            'project_id' : 'cfactory',
            'userid' : 'john',
            'activity' : 'a1',
            'ignore_conflicts' : True,
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, {'userid': 'john'},status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

    def test_put_ignore_conflict_on_split(self):
        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '09:00',
                              'end_time': '11:30'},],
            'project_id' : 'cfactory',
            'userid' : 'john',
            'activity' : 'a1',
            'ignore_conflicts' : True,
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, {'userid': 'john'},status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_put_ignore_conflicts_update_all_fields(self):
        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '07:00',
                              'end_time': '08:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-1',
                              'start_time' : '08:00',
                              'end_time': '09:00'},
                             {'start_date' : '2014-1-1',
                              'end_date' : '2014-1-2',
                              'start_time' : '10:00',
                              'end_time': '11:30'},],
            'project_id' : 'cfactory',
            'userid' : 'john',
            'activity' : 'new activity',
            'description' : "Work on new activity",
            'working_hours_only' : True,
            'ignore_conflicts' : True,
        }
        self.app.put_json(self.api_url + '/%s.json' % timeentry_id,
                          time_entries, status=HTTPOk.code)

        resp = self.app.get(self.api_url, {'userid': 'john'},status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 9)

        resp = self.app.get(self.api_url,
                            {'page' : 9, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        self.assertEqual(res['activity'], time_entries['activity'])
        self.assertEqual(res['description'], time_entries['description'])
        self.assertEqual(res['working_hours_only'], True)

    def test_delete_invalid_timeentry(self):
        self.app.delete(self.api_url + "/invalid.json",
                        status=HTTPNotFound.code)

    def test_delete_self_timeentry(self):
        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']
        log.debug(res)

        self.app.delete(self.api_url + "/%s.json" % timeentry_id,
                        status=HTTPOk.code)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 5)

    def test_delete_other_user_timeentry(self):
        resp = self.app.get(self.api_url,
                            {'page' : 6, 'page_size' : 1, 'userid': 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)
        res = resp.json['result'][0]
        timeentry_id = res['timeentry_id']

        self.app.delete(self.api_url + "/%s.json" % timeentry_id,
                        status=HTTPOk.code)

        resp = self.app.get(self.api_url, {'userid' : 'john'},
                            status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 5)


class TimeSheetAPINonAdminPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet'
        self.config = ttrack.testing.setUp(self.root_factory)
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

        request = pyramid.testing.DummyRequest()
        request.root = self.root_factory(None)
        self.config.testing_securitypolicy(
            userid='john', groupids=find_group('john', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_get_fail_on_other_userid(self):
        self.app.get(self.api_url, {'userid' : 'admin'},
                     status=HTTPForbidden.code)

    def test_allow_get_for_self(self):
        self.app.get(self.api_url, status=HTTPOk.code)


    def test_allow_add_for_self(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_deny_add_for_non_member(self):
        time_entries = {
            'time_splits' : [{'start_date': '2013-12-1',
                              'end_date': '2013-12-3',
                              'start_time' : '09:00',
                              'end_time' : '18:00'},],
            'project_id' : 'coovum_cleanup',
            'activity' : 'Survey of Water flow path',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPForbidden.code)

    def test_deny_add_for_other(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-5',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
            'userid' : 'admin',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPForbidden.code)


class TimeSheetAPIAdminPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_allow_get_for_other_userid(self):
        self.app.get(self.api_url, {'userid' : 'john'},
                     status=HTTPOk.code)

    def test_allow_get_for_self(self):
        self.app.get(self.api_url, status=HTTPOk.code)


    def test_allow_add_for_self(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-5',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
        }
        resp = self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 1)

        resp = self.app.get(self.api_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 8)

    def test_allow_add_for_other(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
            'userid' : 'john',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPOk.code)


class TimeSheetAPIUnAuthenticatedPermissionsTests(unittest.TestCase):
    def root_factory(self, request):
        return self.root

    def setUp(self):
        self.api_url = '/api/v1/timesheet'
        self.config = ttrack.testing.setUp(self.root_factory)
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.timesheet.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))
        # build a root
        self.root = ttrack.testing.build_root_with_s1_base()
        ttrack.testing.populate_timeentry_data(self.root, TE_DATA)

    def tearDown(self):
        ttrack.testing.tearDown()

    def test_deny_get(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_add(self):
        time_entries = {
            'time_splits' : [{'start_date' : '2014-1-4',
                              'end_date' : '2014-1-5',
                              'start_time' : '08:00',
                              'end_time': '09:00'},],
            'project_id' : 'cfactory',
            'activity' : 'new activity',
            'userid' : 'john',
        }
        self.app.post_json(
            self.api_url, time_entries, status=HTTPForbidden.code)
