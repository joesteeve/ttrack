angular.module('ttrack.clients.controllers.edit', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'clientEditCtrl',
  ['$scope', 'Client', '$stateParams', '$state', 'apiErrorDialog',
   function ($scope, Client, $stateParams, $state, errorDlg) {
     var client_id = $stateParams.clientId;
     Client.get(
       {id: client_id},
       function(data) {
         $scope.client = data.result;
       },
       function(err_res) {
         console.log("Invalid client_id : " + client_id);
         $state.go("manage.clients.list");
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.clientEditForm.$dirty && $scope.clientEditForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.clientEditForm.$valid) {
         return;
       }
       Client.save(
         {id: client_id}, $scope.client,
         function (data) {
           $state.go("manage.clients.show", {clientId: client_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }
  ]);
