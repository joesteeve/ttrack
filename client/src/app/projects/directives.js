angular.module('ttrack.projects.directives', [
  'ttrack.resources'
])

  .directive(
    'uniqueProjectId',
    ['Project', function(Project) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Project.get(
                  { id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueProjectId', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueProjectId', true);
                  });
              }
            });
          });
        }
      };
    }
    ]
  )
  .directive(
    'uniqueActivityName',
    ['Activity', function(Activity) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Activity.get(
                  { projectId: scope.project_id,
                    activityName: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueActivityName', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueActivityName', true);
                  });
              }
            });
          });
        }
      };
    }])
  .directive(
    'uniqueActivityNameExclude',
    ['Activity', '$parse', function(Activity, $parse) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function(event) {
            scope.$apply(function () {
              var viewValue = element.val();
              var modelGetter = $parse(attrs.uniqueActivityNameExclude);
              var activity_name = modelGetter(scope);
              if (viewValue && activity_name) {
                if (viewValue.toLowerCase() != activity_name.toLowerCase()) {
                  // Here both viewValue and the exclude activity_name
                  // is available. we try to check if a activity is
                  // unique only if the given activity_name is
                  // different from the exclude activity_name
                  Activity.get(
                    {
                      projectId: scope.project_id,
                      activityName: viewValue
                    },
                    function (data) {
                      ngModelCtrl.$setValidity('uniqueActivityNameExclude',
                                               false);
                    },
                    function (err_res) {
                      ngModelCtrl.$setValidity(
                        'uniqueActivityNameExclude', true);
                    });
                }
                else {
                  ngModelCtrl.$setValidity('uniqueActivityNameExclude', true);
                }
              }
            });
          });
        }
      };
    }]);
