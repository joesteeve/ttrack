# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from .timelog import TimeLog

# Utility function to get or (if not available) create a TimeLog
# object from the given mapping for the given key
def get_or_create_timelog(mapping, key):
    tl = mapping.get(key, None)
    if tl is None:
        tl = TimeLog()
        mapping[key] = tl
    return tl

# Utility function to remove a timeentry object from a TimeLog. If the
# TimeLog is empty, it is also removed.
def cleanup_timeentry(mapping, key, entry):
    tl = mapping[key]
    tl.remove(entry)
    if tl.empty():
        del(mapping[key])
