angular.module('ttrack.roles.controllers.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.roles.directives'
]).controller(
  'roleAddCtrl',
  ['$scope', 'RoleCollection', '$state', 'apiErrorDialog',
   function ($scope, RoleCollection, $state, errorDlg) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.cancel = function () {
       $state.go("manage.roles.list");
     };

     $scope.permissions = [
       { name : 'Manage users', isSelected : false},
       { name : 'Manage clients', isSelected : false},
       { name : 'Manage projects', isSelected : false},
       { name : 'Manage tags', isSelected : false},
       { name : 'Manage roles', isSelected : false},
       { name : 'Manage all timelogs in all projects', isSelected : false}
     ];

     var p_changed = false;
     $scope.$watch($scope.permissions, function (newValue) {
       p_changed = true;
     });

     $scope.save = function () {
       if (!$scope.roleAddForm.$valid) {
         return;
       }
       $scope.role.permissions = [];
       for (var index in $scope.permissions) {
         var perm = $scope.permissions[index];
         if (perm.isSelected) {
           $scope.role.permissions.push(perm.name);
         }
       }
       RoleCollection.save(
         $scope.role,
         function (data) {
           $state.go("manage.roles.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       if ($scope.roleAddForm.$dirty && $scope.roleAddForm.$valid && p_changed) {
         for (var index in $scope.permissions) {
           var perm = $scope.permissions[index];
           if (perm.isSelected) {
             return true;
           }
         }
       }
       return false;
     };

   }]);
