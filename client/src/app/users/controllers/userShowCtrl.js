angular.module('ttrack.users.controllers.show', [
  'ui.router',
  'ttrack.resources'
]).controller(
    'userShowCtrl',
    ['$scope', 'User', '$stateParams', '$state',
     function ($scope, User, $stateParams, $state) {
       var userId = $stateParams.userId;
       User.get(
         {id: userId},
         function(data) {
           $scope.user = data.result;
         },
         function(err_res) {
           console.log("Invalid userid : " + userId);
           $state.go("manage.users.list");
         });
     }]);
