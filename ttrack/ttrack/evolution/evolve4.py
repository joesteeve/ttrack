# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

def evolve(root):
    """This evolve converts all the project IDs to lower case

    This will try to resolve conflicts while trying to rename an
    upper-case project_id to a lower-case. If a lower case of a
    project id conflicts with an existing project id, number is
    suffixed to get a unique project_id as there is no point in
    deleting a project ID as this will cause all the timeentries
    affected
    """
    log.info("Evolving to step 4: convert all project IDs to lowercase")
    projects = root['projects']

    to_change = []
    for k in projects.keys():
        if k.islower() is False:
            to_change.append(k)

    for project_id in to_change:
        if project_id in projects:
            log.info("Lower case converstion of project ID (%s) conflicts",
                     project_id)
            log.info("Trying to resolve conflict of project ID (%s)", project_id)
            i = 1
            new_id = project_id
            lcase_id = project_id.strip().lower()
            while new_id in projects:
                new_id = "%s%d" % (lcase_id, i)
                i = i + 1
            log.info("Project ID (%s) resolved to '%s'", project_id, new_id)
        else:
            new_id = project_id.strip().lower()
        projects.rename_project_id(project_id, new_id)

    if to_change:
        log.info("Migrated %d record(s) to 'step 4' standard", len(to_change))
    else:
        log.info("All records comply to 'step 4' standard")
    log.info("ZODB successfully evolved to step 4")
