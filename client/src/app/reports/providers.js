angular.module('ttrack.reports.providers', [
  'ttrack.utils'
])

  .service('rptColorCodeTable', function () {
    var colors = ["#F08080", "#20B2AA", "#BA55D3", "#9370DB", "#3CB371",
                  "#00FFFF", "#0000FF", "#FF00FF", "#808080", "#008000",
                  "#B22222", "#9932CC", "#483D8B", "#FF69B4", "#4B0082",
                  "#C71585", "#808000", "#6B8E23", "#FFA500", "#FF4500",
                  "#DA70D6", "#AFEEEE", "#DB7093", "#CD853F", "#DDA0DD",
                  "#FF0000", "#BC8F8F", "#4169E1", "#8B4513", "#FA8072",
                  "#F4A460", "#2E8B57", "#6A5ACD", "#008080", "#FF6347",
                  "#EE82EE", "#9ACD32", "#708090", "#5F9EA0", "#D2691E"];
    var index = -1;

    this.getColor = function () {
      // This method gives new color code for every call
      var color = '';
      index += 1;
      if (index > 39 || index < 0) {
        index = 0;
      }
      return colors[index];
    };

    this.getStyle = function () {
      // This method gives the CSS style for background-color for the
      // current color code
      if (index >= 0 && index <= 39) {
        return { 'background-color' : colors[index] };
      } else {
        return { 'background-color' : "#FFFFFF" };
      }
    };
  })

  .factory(
    'rptUtils',
    ['ttrackUtils', function (ttrackUtils) {

    // Various date calculations are based on the assumption that the week
    // starts on Monday. This is will ensure consistency with ISO8601 week
    // number calculation and angular ui's datepicker widget. So, to
    // define a week duration: a current week starts on Monday and ends on
    // next Sunday

    function getISO8601WeekInfo(date) {
      // Code snippet from bootstrap angular ui datepicker.js
      var checkDate = new Date(date);
      // Thursday
      checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
      var time = checkDate.getTime();
      checkDate.setMonth(0); // Compare with Jan 1
      checkDate.setDate(1);
      return {
        week : Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1,
        year : checkDate.getFullYear()
      };
    }

    function getMonday(date) {
      // Return the date for the current week's Monday, if the given date
      // falls on Monday return the same
      var dt = new Date(date);
      dt.setDate(dt.getDate() + 1 - (dt.getDay() || 7));
      return dt;
    }

    function getNextSunday(date) {
      // Return the next sunday date from the given date, if the given date
      // falls on Sunday return the same
      var dt = new Date(date);
      dt.setDate(dt.getDate() - (dt.getDay() || 7) + 7);
      return dt;
    }

    function getWeekTimeKeyString(date) {
      var ret = getISO8601WeekInfo(date);
      return "week-" + ret.year + "-" + ttrackUtils.pad(ret.week);
    }

    function getPreviousWeekTimeKeyString(date) {
      var dt = new Date(date);
      dt.setDate(dt.getDate() - 7);
      return getWeekTimeKeyString(dt);
    }

    function getDateString(date) {
      var dt = new Date(date);
      return dt.getFullYear() + "-" + ttrackUtils.pad(dt.getMonth() + 1) + "-" +
        ttrackUtils.pad(dt.getDate());
    }

    function getCurrentWeekMondayDateString(date) {
      // Get the date string for the current week's Monday, if the given
      // date falls on Monday return the date string for the same
      var dt = getMonday(date);
      return getDateString(dt);
    }

    function getNextSundayDateString(date) {
      // Get the date string for the current week's next Sunday, if the
      // given date falls on Sunday return the date string for the same
      var dt = getNextSunday(date);
      return getDateString(dt);
    }

    function getPreviousWeekMondayDateString(date) {
      // Get the date string for the monday which falls on previous week
      var dt = getMonday(date);
      dt.setDate(dt.getDate() - 7);
      return getDateString(dt);
    }

    function getPreviousWeekNextSundayDateString(date) {
      var dt = getNextSunday(date);
      dt.setDate(dt.getDate() - 7);
      return getDateString(dt);
    }

    function getDaysInMonth(year, month) {
      // Courtesy: http://stackoverflow.com/a/1184359
      // Here month is based on normal human notation eg: 1 for Jan, 12
      // for Dec etc. instead of Javascript notation of month

      // As the Javascript represents month from 0 we construct the date
      // object with day as zero and month set to next month so it gives
      // the last date of the previous month. Eg: if the month passed is 2
      // denoting Feb, in terms of Javascript this denotes March so we
      // construct the date for March with day as zero which gives last
      // day of Feb
      return new Date(year, month, 0).getDate();
    }

    function getMonthStartDateString(date) {
      var dt = new Date(date);
      return  dt.getFullYear() + "-" + ttrackUtils.pad(dt.getMonth() + 1) +
        "-01";
    }

    function getPreviousMonthStartDateString(date) {
      var dt = new Date(date);
      if (dt.getMonth() > 0) {
        return dt.getFullYear() + "-" + ttrackUtils.pad(dt.getMonth()) + "-01";
      }
      return (dt.getFullYear() - 1) + "-12-01";
    }

    function getPreviousMonthEndDateString(date) {
      var dt = new Date(date);
      if (dt.getMonth() > 0) {
        return dt.getFullYear() + "-" + ttrackUtils.pad(dt.getMonth()) + "-" +
          ttrackUtils.pad(getDaysInMonth(dt.getFullYear(), dt.getMonth()));
      }
      return (dt.getFullYear() - 1) + "-12-31";
    }

    function getPreviousMonthTimeKeyString(date) {
      var dt = new Date(getPreviousMonthStartDateString(date));
      return "month-" + dt.getFullYear() + "-" +
        ttrackUtils.pad(dt.getMonth() + 1);
    }

    function getSelectRangeOptions() {
      var dt = new Date();
      return [
        {
          label : 'All',
          start_date : null,
          end_date : null,
          time_key : null
        },
        {
          label : 'Current Week',
          start_date : getCurrentWeekMondayDateString(dt),
          end_date : getNextSundayDateString(dt),
          time_key : getWeekTimeKeyString(dt)
        },
        {
          label : 'Previous Week',
          start_date : getPreviousWeekMondayDateString(dt),
          end_date : getPreviousWeekNextSundayDateString(dt),
          time_key : getPreviousWeekTimeKeyString(dt)
        },
        {
          label : 'Current Month',
          start_date : getMonthStartDateString(dt),
          end_date : getDateString(dt),
          time_key : "month-" + dt.getFullYear() + "-" +
            ttrackUtils.pad(dt.getMonth() + 1)
        },
        {
          label : 'Previous Month',
          start_date : getPreviousMonthStartDateString(dt),
          end_date : getPreviousMonthEndDateString(dt),
          time_key : getPreviousMonthTimeKeyString(dt)
        },
        {
          label : 'Current Year',
          start_date : dt.getFullYear() + "-01-01",
          end_date : getDateString(dt),
          time_key : "year-" + dt.getFullYear()
        },
        {
          label : 'Previous Year',
          start_date : (dt.getFullYear() - 1) + "-01-01",
          end_date : (dt.getFullYear() - 1) + "-12-31",
          time_key : "year-" + (dt.getFullYear() - 1)
        }
      ];
    }

    return {
      'getISO8601WeekInfo': getISO8601WeekInfo,
      'getMonday': getMonday,
      'getNextSunday': getNextSunday,
      'getWeekTimeKeyString': getWeekTimeKeyString,
      'getPreviousWeekTimeKeyString': getPreviousWeekTimeKeyString,
      'getDateString': getDateString,
      'getCurrentWeekMondayDateString': getCurrentWeekMondayDateString,
      'getNextSundayDateString': getNextSundayDateString,
      'getPreviousWeekMondayDateString': getPreviousWeekMondayDateString,
      'getPreviousWeekNextSundayDateString': getPreviousWeekNextSundayDateString,
      'getDaysInMonth': getDaysInMonth,
      'getMonthStartDateString': getMonthStartDateString,
      'getPreviousMonthStartDateString': getPreviousMonthStartDateString,
      'getPreviousMonthEndDateString': getPreviousMonthEndDateString,
      'getPreviousMonthTimeKeyString': getPreviousMonthTimeKeyString,
      'getSelectRangeOptions': getSelectRangeOptions
    };

  }]);
