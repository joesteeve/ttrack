# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component

from pyramid.traversal import find_root

from ttrack.interfaces import IApplicationWillBeCreatedEvent

from .interfaces import (
    IRoleAddedEvent,
    IRoleModifiedEvent,
    IRoleWillBeRemovedEvent
)
from .models import RoleContainer, Role

def _set_acl(context):
    root = find_root(context)
    roles = root['roles']
    root.set_acl(roles.get_acl())

@component.adapter(IApplicationWillBeCreatedEvent)
def initialize_db(event):
    roles = RoleContainer()
    event.app['roles'] = roles
    # Set the initial ACL
    event.app.set_acl(roles.get_acl())

@component.adapter(IRoleModifiedEvent)
def handle_role_modification(event):
    role = event.role
    log.debug('Role Modified : %s', role.role_id)
    _set_acl(role)

@component.adapter(IRoleAddedEvent)
def handle_role_add(event):
    role = event.role
    log.debug("Role added : %s", role.role_id)
    _set_acl(role)

@component.adapter(IRoleWillBeRemovedEvent)
def handle_role_remove(event):
    role = event.role
    log.debug("Role removed : %s", role.role_id)
    root = find_root(role)
    root['users'].find_and_replace_roles(role, None)

def register_zca_subscribers():
    component.provideHandler(initialize_db)
    component.provideHandler(handle_role_modification)
    component.provideHandler(handle_role_add)
    component.provideHandler(handle_role_remove)

register_zca_subscribers()
