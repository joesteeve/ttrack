angular.module('ttrack.users.controllers.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.users.directives'
]).controller(
  'userAddCtrl',
  ['$scope', 'UserCollection', 'RoleCollection', '$state', 'apiErrorDialog',
   function ($scope, UserCollection, RoleCollection, $state, errorDlg) {

     RoleCollection.get(
       function (data) {
         $scope.roles = data.result;
         var role = {
           role_id: 'sg:user',
           name: '(Normal User)',
           permissions: []
           };
         $scope.roles.push(role);
         $scope.selected_role = role;
       },
       function (err_res) {
         console.log("Error occurred while getting roles : " + err_res);
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.userAddForm.$valid) {
         return;
       }
       if ($scope.selected_role.role_id != 'sg:user') {
         // Role is selected
         $scope.user.role_id = $scope.selected_role.role_id;
       }
       UserCollection.save(
         $scope.user,
         function (data) {
           $state.go('manage.users.list');
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return ($scope.userAddForm.$dirty && $scope.userAddForm.$valid &&
               $scope.selected_role && $scope.selected_role.name);
     };

   }]);
