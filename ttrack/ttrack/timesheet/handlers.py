# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component

from pyramid.traversal import find_root

from ttrack.users.interfaces import IUserAddedEvent, IUserWillBeRemovedEvent
from ttrack.clients.interfaces import (
    IClientAddedEvent, IClientWillBeRemovedEvent)
from ttrack.projects.interfaces import (
    IProjectAddedEvent,
    IProjectWillBeRemovedEvent,
    IActivityAddedEvent,
    IActivityWillBeRemovedEvent
)
from ttrack.tags.interfaces import ITagAddedEvent, ITagWillBeRemovedEvent

from .models import TimeSheet


def _get_timesheet(context):
    root = find_root(context)
    if 'timesheet' not in root:
        root['timesheet'] = TimeSheet()
    return root['timesheet']


# handle user add/remove

@component.adapter(IUserAddedEvent)
def handle_user_add(event):
    user = event.user
    timesheet = _get_timesheet(user)
    timesheet.add_user(user)
    log.debug('timesheet: user-add %s', user.userid)

@component.adapter(IUserWillBeRemovedEvent)
def handle_user_remove(event):
    user = event.user
    timesheet = _get_timesheet(user)
    timesheet.remove_user(user)
    log.debug('timesheet: user-remove %s', user.userid)


# handle project add/remove

@component.adapter(IProjectAddedEvent)
def handle_project_add(event):
    project = event.project
    timesheet = _get_timesheet(project)
    timesheet.add_project(project)
    log.debug('timesheet: project-add %s', project.project_id)

@component.adapter(IProjectWillBeRemovedEvent)
def handle_project_remove(event):
    project = event.project
    timesheet = _get_timesheet(project)
    timesheet.remove_project(project)
    log.debug('timesheet: project-remove %s', project.project_id)


# handle activity add/remove

@component.adapter(IActivityAddedEvent)
def handle_activity_add(event):
    activity = event.activity
    timesheet = _get_timesheet(activity)
    timesheet.add_activity(activity)
    log.debug('timesheet: activity-add %s', activity.name)

@component.adapter(IActivityWillBeRemovedEvent)
def handle_activity_remove(event):
    activity = event.activity
    timesheet = _get_timesheet(activity)
    timesheet.remove_activity(activity)
    log.debug('timesheet: activity-remove %s', activity.name)


# handle tag add/remove

@component.adapter(ITagAddedEvent)
def handle_tag_add(event):
    tag = event.tag
    timesheet = _get_timesheet(tag)
    timesheet.add_tag(tag)
    log.debug('timesheet: tag-add %s', tag.name)

@component.adapter(ITagWillBeRemovedEvent)
def handle_tag_remove(event):
    tag = event.tag
    timesheet = _get_timesheet(tag)
    timesheet.remove_tag(tag)
    log.debug('timesheet: tag-remove %s', tag.name)


# handle client add/remove

@component.adapter(IClientAddedEvent)
def handle_client_add(event):
    client = event.client
    timesheet = _get_timesheet(client)
    timesheet.add_client(client)
    log.debug('timesheet: client-add %s', client.client_id)

@component.adapter(IClientWillBeRemovedEvent)
def handle_client_remove(event):
    client = event.client
    timesheet = _get_timesheet(client)
    timesheet.remove_client(client)
    log.debug('timesheet: client-remove %s', client.client_id)

def register_zca_subscribers():
    component.provideHandler(handle_user_add)
    component.provideHandler(handle_user_remove)
    component.provideHandler(handle_project_add)
    component.provideHandler(handle_project_remove)
    component.provideHandler(handle_activity_add)
    component.provideHandler(handle_activity_remove)
    component.provideHandler(handle_tag_add)
    component.provideHandler(handle_tag_remove)
    component.provideHandler(handle_client_add)
    component.provideHandler(handle_client_remove)

register_zca_subscribers()
