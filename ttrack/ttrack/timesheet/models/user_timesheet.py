# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from persistent import Persistent
from persistent.mapping import PersistentMapping

from ttrack import errors

from .timelog import (
    TimeLog,
)

from .utils import (
    get_or_create_timelog,
    cleanup_timeentry,
)


class UserTimeSheet(Persistent):
    """Per user time-sheet

    This time-sheet object maintains the following relationships:

    a) intersection(U, P)
    b) intersection(U, A)
    c) intersection(U, T)

    Where U => User
          P => Project
          A => Activity
          T => Tag
    """

    def __init__(self):
        super(UserTimeSheet, self).__init__()
        self._all = TimeLog()
        self._projects = PersistentMapping()
        self._activities = PersistentMapping()
        self._tags = PersistentMapping()

    def add(self, timeentry):
        self._all.add(timeentry)
        # add to user.project timelog
        tl = get_or_create_timelog(self._projects, timeentry.project)
        tl.add(timeentry)
        # add to user.activity timelog
        tl = get_or_create_timelog(self._activities, timeentry.activity)
        tl.add(timeentry)
        # add to user.tag timelog
        for tag in timeentry.tags:
            tl = get_or_create_timelog(self._tags, tag)
            tl.add(timeentry)

    def remove(self, timeentry):
        self._all.remove(timeentry)
        cleanup_timeentry(self._projects, timeentry.project, timeentry)
        cleanup_timeentry(self._activities, timeentry.activity, timeentry)
        for tag in timeentry.tags:
            cleanup_timeentry(self._tags, tag, timeentry)

    def remove_tag(self, tag):
        if tag in self._tags:
            del(self._tags[tag])

    def get_all(self):
        return self._all

    def get_project(self, project):
        return self._projects.get(project, TimeLog())

    def get_activity(self, activity):
        return self._activities.get(activity, TimeLog())

    def get_tag(self, tag):
        return self._tags.get(tag, TimeLog())

    def get_statistics_for_time_key(self, time_key):
        p_stats = {}
        c_stats = {}
        t_stats = {}
        for project, p_tl in self._projects.items():
            pid = project.project_id
            a_stats = {}
            for activity in project.activities.values():
                if activity in self._activities:
                    # TimeLog for an activity will be added only when
                    # a timeentry containing that activity is
                    # added. But the ActivityTimeSheet in TimeSheet
                    # object will be created as and when the activity
                    # is created
                    a_tl = self._activities[activity]
                    a_stats[activity.name] = a_tl.get_total_time_mins(
                        key=time_key)
            p_stats[pid] = {
                'total' : p_tl.get_total_time_mins(key=time_key),
                'per-activity' : a_stats,
            }
            cid = project.client.client_id
            if cid not in c_stats:
                c_stats[cid] = 0
            c_stats[cid] += p_tl.get_total_time_mins(key=time_key)
        for tag, t_tl in self._tags.items():
            t_stats[tag.name] = t_tl.get_total_time_mins(key=time_key)

        return {
            'projects' : p_stats,
            'tags' : t_stats,
            'clients' : c_stats,
            'total' : self._all.get_total_time_mins(key=time_key)
        }

    def get_statistics_for_range(self, start_datetime, end_datetime):
        p_stats = {}
        t_stats = {}
        c_stats = {}
        total = 0

        for entry in self._all.get_ite_entries(start_datetime, end_datetime):
            mins = entry.effective_time_mins()
            # per project time-spent
            p_st = p_stats.get(entry.project.project_id, None)
            if p_st is None:
                p_stats[entry.project.project_id] = {
                    'total' : 0,
                    'per-activity' : {},
                }
                p_st = p_stats[entry.project.project_id]
            p_st['total'] += mins
            # per activity time-spent
            a_st = p_st['per-activity']
            if entry.activity.name not in a_st:
                a_st[entry.activity.name] = 0
            a_st[entry.activity.name] += mins
            # per tag time-spent
            for tag in entry.tags:
                if tag.name not in t_stats:
                    t_stats[tag.name] = 0
                t_stats[tag.name] += mins
            # per client time-spent
            cid = entry.project.client.client_id
            if cid not in c_stats:
                c_stats[cid] = 0
            c_stats[cid] += mins
            # total time-spent
            total += mins

        return {
            'projects' : p_stats,
            'tags' : t_stats,
            'clients' : c_stats,
            'total' : total,
        }
