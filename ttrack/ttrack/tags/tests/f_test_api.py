# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.tags import (
    TagContainer,
    Tag,
)


TAG_DATA = (
    { "name" : "Tag1"},
    { "name" : "tag2"},
    { "name" : "tag 3"},
)


class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "tags.view"),
               (Allow, Authenticated, "tags.add"),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(cdata):
    root = TestRoot()
    root['tags'] = TagContainer()
    for t in cdata:
        tag = Tag.from_dict(t)
        root['tags'].add(tag)

    def root_factory(request):
        return root
    return root_factory

def tag_in_list(data, tag_list):
    for tag in tag_list:
        if data['name'] == tag['name']:
            return True
    return False


class TagAPITests(unittest.TestCase):
    def setUp(self):
        self.api_url = '/api/v1/tags'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_root_factory(build_root_factory(TAG_DATA))
        self.config.scan("ttrack.tags.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        tags = resp.json['result']
        self.assertEqual(len(TAG_DATA), len(tags))
        for d in TAG_DATA:
            self.assertTrue(tag_in_list(d, tags))

    def test_get(self):
        resp = self.app.get(self.api_url + '/Tag1.json', status=HTTPOk.code)
        tag = resp.json['result']
        self.assertEqual('Tag1', tag['name'])

    def test_get_fail_on_non_existant(self):
        self.app.get(self.api_url + '/unknown.json', status=HTTPNotFound.code)

    def test_get_name_is_case_insensitive(self):
        resp = self.app.get(self.api_url + '/tAG1.json', status=HTTPOk.code)
        tag = resp.json['result']
        self.assertEqual('Tag1', tag['name'])

    def test_add(self):
        new_tag = {
            'name' : 'New Tag',
            }
        resp = self.app.post_json(self.api_url, new_tag, status=HTTPOk.code)
        tag = resp.json['result']
        self.assertEqual(new_tag['name'], tag['name'])

        resp = self.app.get(self.api_url + '/New Tag.json', status=HTTPOk.code)
        tag = resp.json['result']
        self.assertEqual(new_tag['name'], tag['name'])

    def test_add_fail_on_non_unique_tag_name(self):
        new_tag = {
            'name' : 'Tag1',
            }
        self.app.post_json(self.api_url, new_tag, status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_tag_name_case_insensitive(self):
        new_tag = {
            'name' : 'tAG1',
            }
        self.app.post_json(self.api_url, new_tag, status=HTTPBadRequest.code)

    def test_add_fail_on_bad_schema(self):
        new_tag = {
            }
        self.app.post_json(self.api_url, new_tag, status=HTTPBadRequest.code)

    def test_put_change_name(self):
        tag = {
            'name' : 'tag changed Name',
        }
        self.app.put_json(self.api_url + '/Tag1.json', tag, status=HTTPOk.code)
        resp = self.app.get(
            self.api_url + '/tag changed Name.json', status=HTTPOk.code)
        upd_tag = resp.json['result']
        self.assertEqual(tag['name'], upd_tag['name'])

    def test_put_change_name_case(self):
        tag = {
            'name' : 'tAG1',
        }
        self.app.put_json(self.api_url + '/Tag1.json', tag, status=HTTPOk.code)
        resp = self.app.get(
            self.api_url + '/tAG1.json', status=HTTPOk.code)
        upd_tag = resp.json['result']
        self.assertEqual(tag['name'], upd_tag['name'])

    def test_put_non_existant(self):
        tag = {
            'name' : 'tAG1',
        }
        self.app.put_json(
            self.api_url + '/unknown.json', tag, status=HTTPNotFound.code)

    def test_put_duplicate_name(self):
        tag = {
            'name' : 'Tag1',
        }
        self.app.put_json(
            self.api_url + '/tag2.json', tag, status=HTTPBadRequest.code)

    def test_delete(self):
        self.app.delete(self.api_url + '/Tag1.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/Tag1.json', status=HTTPNotFound.code)

    def test_delete_non_existant_user(self):
        self.app.delete(
            self.api_url + '/other.json', status=HTTPNotFound.code)


class TagAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/tags'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(TAG_DATA))
        self.config.scan("ttrack.tags.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/Tag1.json', status=HTTPOk.code)

    def test_allow_add(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.post_json(self.api_url, tag, status=HTTPOk.code)

    def test_deny_update(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.put_json(self.api_url + '/Tag1.json', tag,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/Tag1.json', status=HTTPForbidden.code)


class TagAPIAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/tags'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(TAG_DATA))
        self.config.scan("ttrack.tags.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/Tag1.json', status=HTTPOk.code)

    def test_allow_add(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.post_json(self.api_url, tag, status=HTTPOk.code)

    def test_allow_update(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.put_json(self.api_url + '/Tag1.json', tag, status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url + '/Tag1.json', status=HTTPOk.code)


class TagAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/tags'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(build_root_factory(TAG_DATA))
        self.config.scan("ttrack.tags.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url + '/Tag1.json', status=HTTPForbidden.code)

    def test_deny_add(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.post_json(self.api_url, tag, status=HTTPForbidden.code)

    def test_deny_update(self):
        tag = {
            'name' : 'New Tag',
            }
        self.app.put_json(
            self.api_url + '/Tag1.json', tag, status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/Tag1.json', status=HTTPForbidden.code)

    def test_deny_delete_without_exposing_unknown(self):
        self.app.delete(
            self.api_url + '/Unknown.json', status=HTTPForbidden.code)
