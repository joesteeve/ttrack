angular.module('ttrack.dashboard', [
  'ui.router',
  'ttrack.dashboard.home'

])
  .config(
    ['$stateProvider', '$urlRouterProvider',
     function ($stateProvider, $urlRouterProvider) {
       $stateProvider
         .state('home', {
           parent: 'page',
           url: '/home',
           templateUrl: 'dash/home.tpl.html',
           controller: 'dashHomeCtrl'
         });
       $urlRouterProvider.otherwise("/home");
     }]);
