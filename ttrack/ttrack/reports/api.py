# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import (
    authenticated_userid,
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from cornice import Service

import colander

from ttrack import (
    errors,
    projects,
    users,
    clients,
    tags,
)

from .generators import (
    generate_project_report,
    generate_user_report,
    generate_client_report,
    generate_tag_report,
    )


class ReportGetSchema(colander.MappingSchema):
    time_key = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    start_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    end_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)


@cornice_resource(
    name="api.project_reports",
    path="/api/v1/reports/projects/{project_id}",
    permission="reports.view",
    )
class ProjectReportsAPI(object):
    def __init__(self, request):
        self.request = request
        self.projects = request.root['projects']
        self.timesheet = request.root['timesheet']

    @cornice_view(schema=ReportGetSchema,
                  validators=(projects.validators.url_project_id_exists,))
    def get(self):
        project_id = self.request.matchdict['project_id']
        project = self.projects.get(project_id)

        start_date = self.request.validated.get('start_date', None)
        end_date = self.request.validated.get('end_date', None)
        if (start_date and end_date) and (start_date > end_date):
            self.request.errors.add(
                "querystring", "start_date",
                "start-date (%s) cannot be greater than end-date (%s)" %
                (start_date, end_date))
            self.request.errors.status = HTTPBadRequest.code
            return

        time_key = self.request.validated.get('time_key', None)
        project_timesheet = self.timesheet.get_project_timesheet(project)
        report = generate_project_report(
            project_timesheet, time_key, start_date, end_date)
        return {
            'type' : 'project-reports',
            'result' : report,
        }


@cornice_resource(
    name="api.user_reports",
    path="/api/v1/reports/users/{userid}",
    permission="reports.view",
    )
class UserReportsAPI(object):
    def __init__(self, request):
        self.request = request
        self.users = request.root['users']
        self.timesheet = request.root['timesheet']

    @cornice_view(schema=ReportGetSchema,
                  validators=(users.validators.url_userid_exists,))
    def get(self):
        userid = self.request.matchdict['userid']
        user = self.users.get(userid)

        start_date = self.request.validated.get('start_date', None)
        end_date = self.request.validated.get('end_date', None)
        if (start_date and end_date) and (start_date > end_date):
            self.request.errors.add(
                "querystring", "start_date",
                "start-date (%s) cannot be greater than end-date (%s)" %
                (start_date, end_date))
            self.request.errors.status = HTTPBadRequest.code
            return

        time_key = self.request.validated.get('time_key', None)
        user_timesheet = self.timesheet.get_user_timesheet(user)
        report = generate_user_report(
            user_timesheet, time_key, start_date, end_date)
        return {
            'type' : 'user-reports',
            'result' : report,
        }


@cornice_resource(
    name="api.client_reports",
    path="/api/v1/reports/clients/{client_id}",
    permission="reports.view",
    )
class ClientReportsAPI(object):
    def __init__(self, request):
        self.request = request
        self.clients = request.root['clients']
        self.timesheet = request.root['timesheet']

    @cornice_view(schema=ReportGetSchema,
                  validators=(clients.validators.url_client_id_exists,))
    def get(self):
        client_id = self.request.matchdict['client_id']
        client = self.clients.get(client_id)

        start_date = self.request.validated.get('start_date', None)
        end_date = self.request.validated.get('end_date', None)
        if (start_date and end_date) and (start_date > end_date):
            self.request.errors.add(
                "querystring", "start_date",
                "start-date (%s) cannot be greater than end-date (%s)" %
                (start_date, end_date))
            self.request.errors.status = HTTPBadRequest.code
            return

        time_key = self.request.validated.get('time_key', None)
        report = generate_client_report(
            client, self.timesheet, time_key, start_date, end_date)
        return {
            'type' : 'client-reports',
            'result' : report,
        }

@cornice_resource(
    name="api.tag_reports",
    path="/api/v1/reports/tags/{tag_name}",
    permission="reports.view",
)
class TagReportsAPI(object):

    def __init__(self, request):
        self.request = request
        self.tags = request.root['tags']
        self.timesheet = request.root['timesheet']


    @cornice_view(schema=ReportGetSchema,
                  validators=(tags.validators.url_tag_name_exists,))
    def get(self):
        tag_name = self.request.matchdict['tag_name']
        tag = self.tags.get(tag_name)

        start_date = self.request.validated.get('start_date', None)
        end_date = self.request.validated.get('end_date', None)
        if (start_date and end_date) and (start_date > end_date):
            self.request.errors.add(
                "querystring", "start_date",
                "start-date (%s) cannot be greater than end-date (%s)" %
                (start_date, end_date))
            self.request.errors.status = HTTPBadRequest.code
            return

        time_key = self.request.validated.get('time_key', None)
        report = generate_tag_report(
            tag, self.timesheet, time_key, start_date, end_date)
        return {
            'type' : 'tag-reports',
            'result' : report,
        }
