# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope.interface import Interface, Attribute

class IProject(Interface):
    pass

class IProjectAddedEvent(Interface):
    project = Attribute("The Project object")

class IProjectWillBeRemovedEvent(Interface):
    project = Attribute("The Project object")

class IActivity(Interface):
    pass

class IActivityAddedEvent(Interface):
    project = Attribute("The Project object")
    activity = Attribute("The Activity object")

class IActivityWillBeRemovedEvent(Interface):
    project = Attribute("The Project object")
    activity = Attribute("The Activity object")

class IProjectRole(Interface):
    pass

class IProjectRoleWillBeRemovedEvent(Interface):
    """This event is exclusively fired in Role remove action before the
    project-Role is removed and the IProjectRolesModifiedEvent is
    fired
    """
    project = Attribute("The Project object")
    role = Attribute("The Project-Role object")
