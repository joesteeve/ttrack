# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime
import uuid

from persistent import Persistent
from persistent.list import PersistentList

from ttrack import errors

FIRST_UUID = '0' * 32
LAST_UUID = 'f' * 32


def compute_timeentry_id(start_datetime, UUID=None):
    if UUID is None:
        UUID = uuid.uuid4().hex
    return "%s-%s" % (start_datetime.isoformat(), UUID)

def _to_mins(delta):
    mins = int(delta.total_seconds() / 60)
    return mins


class UITimeEntryFactory(object):
    def __init__(self, tags):
        self.tags = tags

    def create(self, start_datetime, end_datetime,
               user, project, activity_name, tag_name_list,
               description=None, working_hours_only=False):
        # get the activity object
        activity = project.get_activity(activity_name, None)
        if activity is None:
            activity = project.add_activity(activity_name)

        # get the list of tag objects
        tag_list = []
        for tag_name in tag_name_list:
            tag = self.tags.get(tag_name, None)
            if tag is None:
                tag = self.tags.add_by_name(tag_name)
            tag_list.append(tag)

        te = UITimeEntry(start_datetime, end_datetime)
        te.user = user
        te.activity = activity
        te.project = project
        for tag in tag_list:
            te.add_tag(tag)
        te.description = description
        te.calculate_working_hours_only = working_hours_only
        return te


class UITimeEntry(Persistent):

    def __init__(self, start_datetime, end_datetime):
        """
        start_datetime: Starting datetime as datetime.datetime object (local)
        end_datetime: Ending datetime as datetime.datetime object (local)
        """
        super(UITimeEntry, self).__init__()
        self.ite_list = PersistentList()
        self.set_time(start_datetime, end_datetime)

        # Properties which can be accessed only by getters and setters
        self.__calculate_working_hours_only = False
        self.__description = None
        self.__tags = PersistentList()
        self.__project = None
        self.__activity = None
        self.__user = None

    def set_time(self, start_datetime, end_datetime):
        if start_datetime >= end_datetime:
            raise errors.BadParameter(
                "start_datetime='%s', should be less than end_datetime='%s'" %
                (start_datetime, end_datetime))

        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.ute_id = compute_timeentry_id(self.start_datetime)

        s_date = start_datetime.date()
        e_date = end_datetime.date()
        if s_date != e_date:
            td = e_date - s_date
            for day in range(td.days + 1):
                date = s_date + datetime.timedelta(days=day)
                if day > 0:
                    start_time = datetime.time(0, 0)
                else:
                    start_time = start_datetime.time()

                if day < td.days:
                    end_time = datetime.time(23, 59, 59)
                else:
                    end_time = end_datetime.time()
                if end_time > datetime.time(0, 0, 0):
                    # If the given start_datetime is 2014-01-01T10:00
                    # and end_datetime is 2014-01-02T00:00 we skip the
                    # ite creation for the second day as the
                    # start_time and end_time for 2014-01-02 will be
                    # the same
                    self.construct_internal_time_entry(date, start_time, end_time)
        else:
            self.construct_internal_time_entry(s_date, start_datetime.time(),
                                               end_datetime.time())

    def construct_internal_time_entry(self, date, start_time, end_time):
        ite = InternalTimeEntry(date, start_time, end_time)
        ite.ute = self
        self.ite_list.append(ite)

    def add_tag(self, tag):
        if tag in self.__tags:
            raise errors.AlreadyExists("Tag='%s' already exists" % tag.name)
        for ite in self.ite_list:
            ite.add_tag(tag)
        self.__tags.append(tag)

    def unlink_tag(self, tag):
        if tag not in self.__tags:
            raise errors.DoesNotExist("Tag='%s' does not exist" % tag.name)
        for ite in self.ite_list:
            ite.unlink_tag(tag)
        self.__tags.remove(tag)

    # Getter properties
    @property
    def description(self):
        return self.__description

    @property
    def project(self):
        return self.__project

    @property
    def activity(self):
        return self.__activity

    @property
    def user(self):
        return self.__user

    @property
    def tags(self):
        return self.__tags

    @property
    def calculate_working_hours_only(self):
        return self.__calculate_working_hours_only

    # Setter properties
    @description.setter
    def description(self, value):
        for ite in self.ite_list:
            ite.description = value
        self.__description = value

    @project.setter
    def project(self, value):
        for ite in self.ite_list:
            ite.project = value
        self.__project = value

    @activity.setter
    def activity(self, value):
        for ite in self.ite_list:
            ite.activity = value
        self.__activity = value

    @user.setter
    def user(self, value):
        for ite in self.ite_list:
            ite.user = value
        self.__user = value

    @calculate_working_hours_only.setter
    def calculate_working_hours_only(self, value):
        for ite in self.ite_list:
            ite.calculate_working_hours_only = value
        self.__calculate_working_hours_only = value

    @classmethod
    def clone(klass, obj):
        new_entry = UITimeEntry(obj.start_datetime, obj.end_datetime)
        new_entry.project = obj.project
        new_entry.user = obj.user
        new_entry.activity = obj.activity
        for tag in obj.tags:
            new_entry.add_tag(tag)
        return new_entry

    @classmethod
    def from_dict(klass, data):
        dt = datetime.datetime
        s_dtime = dt.combine(dt.strptime(data['start_date'], '%Y-%m-%d').date(),
                             dt.strptime(data['start_time'], '%H:%M').time())
        if 'end_date' in data:
            e_dtime = dt.combine(dt.strptime(data['end_date'],
                                             '%Y-%m-%d').date(),
                                 dt.strptime(data['end_time'], '%H:%M').time())
        else:
            e_dtime = dt.combine(s_dtime.date(),
                                 dt.strptime(data['end_time'], '%H:%M').time())

        te = klass(s_dtime, e_dtime)
        return te

    def to_dict(self):
        project_id = None
        project_name = None
        client_id = None
        client_name = None
        if self.project:
            project_id = self.project.project_id
            project_name = self.project.name
            if self.project.client:
                client_id = self.project.client.client_id
                client_name = self.project.client.name
        activity = None
        if self.activity:
            activity = self.activity.name
        te = {
            'timeentry_id' : self.ute_id,
            'start_date' : self.start_datetime.strftime('%Y-%m-%d'),
            'start_time' : self.start_datetime.strftime("%H:%M"),
            'end_date' : self.end_datetime.strftime('%Y-%m-%d'),
            'end_time' : self.end_datetime.strftime("%H:%M"),
            'description' : self.description,
            'userid' : self.user.userid,
            'user_name' : self.user.real_name,
            'project_id': project_id,
            'project_name': project_name,
            'client_id' : client_id,
            'client_name' : client_name,
            'working_hours_only': self.calculate_working_hours_only,
            'activity' : activity,
            'tags' : [],
        }
        if self.tags:
            for tag in self.tags:
                te['tags'].append(tag.name)
        return te

    def __repr__(self):
        return self.ute_id


class InternalTimeEntry(Persistent):

    def __init__(self, date, start_time, end_time):
        """
        start_time : Starting time as datetime.time object (local)

        end_time : Ending time as datetime.time object (local)
        """
        super(InternalTimeEntry, self).__init__()
        self._effective_time_delta = None
        self.set_time(date, start_time, end_time)
        self.calculate_working_hours_only = False
        self.description = None
        self.ute = None
        self.tags = PersistentList()
        self.project = None
        self.activity = None
        self.user = None


    def set_time(self, date, start_time, end_time):
        """Sets the time and updates the timelog_id

        The timelog_id should be unique. It is generated using
        'start-timestamp' and a uuid
        """
        if start_time >= end_time:
            raise errors.BadParameter(
                "start_time=%s, should be less than end_time=%s" % \
                (start_time, end_time))
        # convert the datetime.time object to datetime.datetime
        self.start_time = datetime.datetime.combine(date, start_time)
        self.end_time = datetime.datetime.combine(date, end_time)
        self.ite_id = compute_timeentry_id(self.start_time)

        self._effective_time_delta = self.end_time - self.start_time

        # When the end-of-day is generated, it is generated as
        # 23:59:59. This causes a loss of '1 second' in the delta
        # computation. The following is a fix for that. Check issue#35
        if end_time > datetime.time(23, 59):
            self._effective_time_delta += datetime.timedelta(seconds=1)

    def add_tag(self, tag):
        if tag in self.tags:
            raise errors.AlreadyExists("Tag='%s' already exists" % tag.name)
        self.tags.append(tag)

    def unlink_tag(self, tag):
        if tag not in self.tags:
            raise errors.DoesNotExist("Tag='%s' does not exist" % tag.name)
        self.tags.remove(tag)

    def effective_time(self):
        return self._effective_time_delta

    def effective_time_mins(self):
        return _to_mins(self._effective_time_delta)

    @classmethod
    def clone(klass, obj):
        new_entry = TimeEntry(
            obj.start_time.date(), obj.start_time.time(), obj.end_time.time(),
            obj.description)
        new_entry.project = obj.project
        new_entry.user = obj.user
        new_entry.activity = obj.activity
        for tag in obj.tags:
            new_entry.tags.append(tag)

        return new_entry

    @classmethod
    def from_dict(klass, data):
        dt = datetime.datetime
        te = klass(dt.strptime(data['date'], '%Y-%m-%d').date(),
                   dt.strptime(data['start_time'], '%H:%M').time(),
                   dt.strptime(data['end_time'], '%H:%M').time())
        return te

    def to_dict(self):
        project_id = None
        project_name = None
        client_id = None
        client_name = None
        if self.project:
            project_id = self.project.project_id
            project_name = self.project.name
            if self.project.client:
                client_id = self.project.client.client_id
                client_name = self.project.client.name
        activity = None
        if self.activity:
            activity = self.activity.name
        if self.end_time.time() > datetime.time(23, 59):
            end_time = '00:00'
        else:
            end_time = self.end_time.strftime("%H:%M")
        te = {
            'timeentry_id' : self.ute.ute_id,
            'date' : self.start_time.strftime('%Y-%m-%d'),
            'start_time' : self.start_time.strftime("%H:%M"),
            'end_time' : end_time,
            'description' : self.description,
            'userid' : self.user.userid,
            'user_name' : self.user.real_name,
            'project_id': project_id,
            'project_name': project_name,
            'client_id' : client_id,
            'client_name' : client_name,
            'working_hours_only': self.calculate_working_hours_only,
            'activity' : activity,
            'tags' : [],
            'duration': self.effective_time_mins(),
        }
        if self.tags:
            for tag in self.tags:
                te['tags'].append(tag.name)
        return te
