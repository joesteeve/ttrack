# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

def body_role_id_exists(request):
    roles = request.root['roles']
    role_id = request.json.get("role_id", None)
    if role_id and role_id not in roles:
        request.errors.add(
            'body', 'role_id', 'Role=%s does not exist' % role_id)
        request.errors.status = HTTPBadRequest.code

def qs_role_id_exists(request):
    roles = request.root['roles']
    role_id = request.params.get("role_id", None)
    if role_id and role_id not in roles:
        request.errors.add('querystring', 'role_id',
                           'Role=%s does not exist' % role_id)
        request.errors.status = HTTPBadRequest.code

def url_role_id_exists(request):
    roles = request.root['roles']
    role_id = request.matchdict["role_id"]
    if role_id not in roles:
        request.errors.add(
            'url', 'role_id', 'Role=%s does not exist' % role_id)
        request.errors.status = HTTPNotFound.code
